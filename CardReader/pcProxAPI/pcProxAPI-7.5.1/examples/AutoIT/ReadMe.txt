Synopsis :
----------

This is a AutoIt example to show how rfideas's SDK can be used with AutoIt.
This application can be used to list all rfidea's devices, can be used to know
the current version etc.

Prerequisites\Dependencies:
--------------

1. AutoIt 
2. pcProxAPI.dll  while running on windows

NOTE: 
1. To know, how you can install AutoIt on your platform please read their official document.

How To Compile :
--------------

1. Command to compile on 32 bit platform :
"C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2exe.exe" /In readercomm.au3 /out readercomm.exe /console

2. Command to compile on 64 bit platform :
"C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2exe_x64.exe" /In readercomm.au3 /out readercomm.exe /console

NOTE:
The path of Aut2exe.exe and Aut2exe_x64.exe depends on the path of the directory where you have installed AutoIT.

How To Run :
-----------

Running the example is very easy, to see what are the options available use help.

    readercomm.exe --help

All supported options are below :

--enumerate        list all the connected rfidea's readers 
--sdk-version      give the sdk version 
--getid            give raw data of card which being read
--getESN           read the ESN from the reader
--help             print help menu 


Frequently Asked Questions(FAQ) :
--------------------------------

1. What to do when I get "Failed to load dll <name of lib> does not exists, 
   RFI_DLL_HOME environment variable can used to set DLL home" error?

  In default setup you should not get this error, but you can use RFI_DLL_HOME environment
  variable to point to home of libs where they can be found.
 
2.  Why do I get "No Reader Connected.." even though reader is connected physically?

It happens because you might have connected a reader other than USB as this sample supports USB readers only.  

3. Why do I get "No Id Found, Please put card on the reader and make sure it must be configured with the card placed on it"
even though a correct card is placed on the reader?

It might happens because the connected reader does not support GetActiveID32 Api.