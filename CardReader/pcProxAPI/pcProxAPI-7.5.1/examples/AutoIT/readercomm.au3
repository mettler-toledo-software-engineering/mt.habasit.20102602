#cs
RF IDeas, Inc. Proprietary and Confidential
Copyright © 2016 RF IDeas, Inc. as an unpublished work.
All Rights Reserved

Author : IntimeTec Visionsoft Private Limited.
Creation Date : 15 Feb,2017
This example merely a proof of concept that rfideas's
SDK(which is a C/C++ library) can be used in autoIT
#ce

#include <StaticConstants.au3>

Global $lastData[32+1]
Global $path

if(@AutoItX64) Then
   $path = "..\..\lib\64"
Else
   $path = "..\..\lib\32"
EndIf

; Path set by the user takes priority
$dll_home = EnvGet("RFI_DLL_HOME")
if($dll_home) Then
   $path = $dll_home
   EndIf

$pcProxAPI = $path&"\"&"pcProxAPI.dll"
Const $PRXDEVTYP_USB = 0

$dll = DllOpen($pcProxAPI)
if($dll == -1) Then
   ConsoleWrite(@CRLF&"Failed to load dll"&$pcProxAPI)
   ConsoleWrite(@CRLF&"RFI_DLL_HOME environment variable can used to set DLL home")
   Exit
EndIf

#cs
Will return True in case success false otherwise.
Will open connection to all rfidea's readers/devices in once
Individual device can be accessed by using setActiveDev first
then call other functions.
#ce
Func RF_usbConnect()
	Local $ret = DllCall($pcProxAPI, "short", "usbConnect")
	if Not @error Then Return $ret[0]
	return 0
 EndFunc

; Will return the luid of active device
Func RF_getLUID()
   $rets = DllCall($pcProxAPI, "Int", "GetLUID")
   if Not @error Then Return $rets[0]
	return 0
 EndFunc

;will return the partnumber of active reader
;will return 0 in case of failure
 Func RF_getPartNumberString()
   $returnPartNumber = DllCall($pcProxAPI, "STR", "getPartNumberString")
   if Not @error Then Return $returnPartNumber[0]
   return 0
 EndFunc

#cs
Will return the SDK version in following format
<Major>.<Minor>.<Dev>.
In case of error will return 0
#ce
Func RF_GetLibVersion()
	Local $ret = DllCall($pcProxAPI, "short", "GetLibVersion", "short*", 0, "short*", 0, "short*", 0)
	If(Not @error) Then Return $ret[1] & "." & $ret[2] & "." & $ret[3]
	return 0
 EndFunc

#cs
It will close the handle to all rfidea's devices, should be
called during clean up.
#ce
 Func RF_Disconnect()
	DllCall($pcProxAPI, "short", "USBDisconnect")
EndFunc


;This func wil print the raw data of the card placed on the reader.
 Func RF_getActiveID32()
	Local $tSTRUCT1 = DllStructCreate("BYTE v[32]")
	  Local $ret = DllCall($pcProxAPI, "short", "GetActiveID32", "ptr",DllStructGetPtr($tSTRUCT1),"short", 32)
	  Local $bits = $ret[0]
	  if($bits == 0) Then
		 ConsoleWrite(@CRLF&"No id found, Please put card on the reader and make sure it must be configured with the card placed on it."&@CRLF)
		 Exit
	  EndIf

	  Local $bytes = Int(($bits + 7) / 8) ; round up to whole bytes
	  if($bytes < 8) then
		 $bytes = 8
	  EndIf
	  $cardData = ""
	  For $i = $bytes to 1 step -1
		 $str = DllStructGetData($tSTRUCT1, 1,$i)
		$cardData = $cardData & Hex($str, 2) & " "
	 Next
	 ConsoleWrite(@CRLF&$bits&" Bits : "&$cardData&@CRLF)
 EndFunc

#cs
Will Return the total number of
connected rfidea's readers.
#ce
Func RF_GetDevCnt()
	Local $ret = DllCall($pcProxAPI, "short", "GetDevCnt")
	If Not @Error then Return $ret[0]
	return 0
EndFunc

;This func is used to read the ESN from the connected reader
Func RF_getEsn()
   Local $ESN = DllCall($pcProxAPI, "STR", "getESN")
   If Not @Error then Return $ESN[0]
EndFunc

;This func is used to set the active device. It will retun 0 in case of failure
Func RF_SetActDev($dev)
	Local $ret = DllCall($pcProxAPI, "short", "SetActDev", "short", $dev)
	If Not @Error then Return $ret[0]
	EndFunc

; This func is used to get vid pid of active reader
Func RF_GetVidPidVendorName()
   Local $ret = DllCall($pcProxAPI, "STR", "GetVidPidVendorName")
	If Not @Error then Return $ret[0]
	EndFunc

#cs
Helper of  usbconnect, Will restrict
the device search to a particular category
for example.
Device Type could be one of following
0 : To search only USB devices
1 :  To Search Serial RS-232 only.
-1 : Both USB and serial devices.
This function will return true in case of
success false otherwise.
#ce
Func RF_SetDevTypeSrch($prod)
   Local $ret = DllCall($pcProxAPI, "short", "SetDevTypeSrch","short",$prod)
   If Not @Error then Return $ret[0]
	EndFunc


;This func is used to print help text
Func PrintHelpText()
   $programName = @ScriptName
   $str = StringTrimRight($programName, 4)
   ConsoleWrite(@CRLF&"usage:"&@TAB&$str&@TAB&"[options]"&@CRLF&@CRLF)
ConsoleWrite("--enumerate"&@TAB&@TAB&"list all the connected rfidea's readers"&@CRLF)
 ConsoleWrite("--sdk-version"&@TAB&@TAB&"give the sdk version"&@CRLF)
ConsoleWrite("--getid"&@TAB&@TAB&@TAB&"give raw data of card which being read"&@CRLF)
ConsoleWrite("--getESN"&@TAB&@TAB&"read ESN from the reader"&@CRLF)
ConsoleWrite("--help"&@TAB&@TAB&@TAB&"print help menu"&@CRLF)
EndFunc

If($cmdLine[0] == "0") Then
   ConsoleWrite("Must Pass an argument"& @CRLF)
   PrintHelpText()
   Exit
   EndIf


   $usrSelect = $cmdLine[1]
if not ($usrSelect == "--enumerate" Or $usrSelect == "--sdk-version" Or $usrSelect == "--getid" Or $usrSelect == "--getESN" Or $usrSelect == "--help")Then
   ConsoleWrite(@CRLF&"Invalid Argument"&@CRLF)
   PrintHelpText()
Exit
EndIf

if($usrSelect == "--help") Then
   PrintHelpText()
   Exit
   EndIf


if($usrSelect == "--enumerate") Then
   RF_SetDevTypeSrch($PRXDEVTYP_USB)
   If RF_usbConnect() Then
	   $deviceNumber = RF_GetDevCnt()
	  ConsoleWrite(@CRLF&"PartNumber"&@TAB&"Vid:Pid"&@TAB&@TAB&@TAB&"LUID"&@CRLF&@CRLF)
	  For $i = 0 to $deviceNumber-1
	  $partNumber = ""
	  $luid = ""
	  $vidpid =  ""
	  RF_SetActDev($i)
	  $partNumber = RF_getPartNumberString()
	  if($partNumber == 0) Then
		 ConsoleWrite(@CRLF&"Unable to get partnumber"&@CRLF)
		 EndIf
	  $luid = RF_getLUID()
	  $vidpid = RF_GetVidPidVendorName()
	   if($vidpid == 0) Then
		 ConsoleWrite(@CRLF&"Unable to get vidpid"&@CRLF)
	  EndIf
	  if($luid == Null)Then
		 ConsoleWrite(@CRLF&"Unable to get luid"&@CRLF)
		 EndIf

	  ConsoleWrite($partNumber&@TAB&$vidpid&@TAB&$luid&@CRLF)
   Next
   RF_Disconnect()
	  Else
	  ConsoleWrite(@CRLF&"Reader Not Connected" & @CRLF)
   EndIf
EndIf

if($usrSelect == "--sdk-version") Then
   $libVersion = RF_GetLibVersion()
   ConsoleWrite(@TAB&"SDK Version :"&@TAB)
   ConsoleWrite($libVersion & @CRLF)
   if($libVersion == Null) Then
	  ConsoleWrite(@TAB&"Unable to get the sdk version"&@CRLF)
	  Exit
   EndIf
EndIf

if($usrSelect == "--getid") Then
   RF_SetDevTypeSrch($PRXDEVTYP_USB)
   If RF_usbConnect() Then
	  Sleep(250)
	  RF_getActiveID32()
	  RF_Disconnect()
   Else
	  ConsoleWrite(@CRLF&"Reader Not Connected"&@CRLF)
   EndIf
EndIf

if($usrSelect == "--getESN") Then
   RF_SetDevTypeSrch($PRXDEVTYP_USB)
   If RF_usbConnect() Then
	  $ESN = RF_getEsn()
	  If $ESN == "" Then
		 ConsoleWrite(@CRLF&"Reader does not support ESN functionality"&@CRLF)
	  Else
		 ConsoleWrite(@CRLF&"ESN: "&$ESN&@CRLF)
   EndIf
	  RF_Disconnect()
   Else
	  ConsoleWrite(@CRLF&"Reader Not Connected"&@CRLF)
   EndIf
EndIf

#cs
Copyright © RF IDeas, Inc. Proprietary and confidential.
EOF
#ce