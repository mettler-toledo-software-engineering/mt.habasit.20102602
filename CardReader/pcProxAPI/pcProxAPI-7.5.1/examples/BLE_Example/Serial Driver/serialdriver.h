#ifndef SERIALDRIVER_H
#define SERIALDRIVER_H

#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cmd_def.h"

extern HANDLE serial_handle;

int WriteToSerialPort(unsigned char *data, size_t len);
void output(uint8 len1, uint8* data1, uint16 len2, uint8* data2);
int read_response(unsigned char *pBuff, int *bytesActuallyRead);
int read_message();
int setPortSettings(char *portName);
int DiscoverFor(int timeInterval);
void ParseResponseData(unsigned char *buf, int bufferLength);

#endif