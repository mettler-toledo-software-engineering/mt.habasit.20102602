#include "serialdriver.h"

HANDLE serial_handle = NULL;
int WriteToSerialPort(unsigned char *data, size_t len)
{
	int result = 1;
	int offset = 0;
	int byteToBeWritten = len;
	int byteActuallyWritten = 0;
	BOOL rc = FALSE;
	/*
	When we request to write large number of bytes then it might be possible that
	all the bytes are not written in one go. To handle that condition and to write
	all the bytes that are requested by user, we have used this loop.
	*/
	do
	{
		PurgeComm(serial_handle, PURGE_RXCLEAR | PURGE_RXABORT);
		PurgeComm(serial_handle, PURGE_TXCLEAR | PURGE_TXABORT);
		rc = WriteFile(serial_handle,
			data + offset,
			byteToBeWritten,
			(LPDWORD)&byteActuallyWritten,
			NULL
			);
		if (rc == FALSE)
		{
			result = 0;
			break;
		}
		byteToBeWritten -= byteActuallyWritten;
		offset += byteToBeWritten;
	} while (byteToBeWritten != 0);
	return result;
}


void output(uint8 len1, uint8* data1, uint16 len2, uint8* data2)
{
	if (!WriteToSerialPort(data1, len1))
	{
		int error = (int)GetLastError();
		if (error == 2){	//Device is not connected or it is unplugged.
			printf("Device is not connected.\n");
		}
		else{
			printf("Writing to device failed.\nError Code: %d\n", error);
		}
		exit(-1);
	}

	if (!WriteToSerialPort(data2, len2))
	{
		int error = (int)GetLastError();
		if (error == 2){	//Device is not connected or it is unplugged.
			printf("Device is not connected.\n");
		}
		else{
			printf("Writing to device failed.\nError Code: %d\n", error);
		}
		exit(-1);
	}
}

int read_response(unsigned char *pBuff, int *bytesActuallyRead)
{
	unsigned char RXBuffer[2048] = { 0 };
	char readByte = 0;
	int i = 0;
	int result = 0;
	int expectedLength = 0;
	DWORD rread = 0;
	int retry = 100;
	while (retry > 0)
	{
		/* This is a state machine designed for reading the data from the port. It work as follows :
		* Get the complete response or event whatever it received in the buffer.
		* Calculates the total length of data received.
		* If the received data is response then it will fill in the read buffer else it will
		* skip the received data.
		*/
		while (ReadFile(serial_handle, &readByte, 1, &rread, NULL) && rread)
		{
			if ((i == 0) && (readByte == 0x80 || readByte == 0x00)) // Scan Response event 
			{
				RXBuffer[i++] = readByte;

			}
			else if (i == 1)
			{
				RXBuffer[i++] = readByte; // Length of the payload received as response
				expectedLength = 4 + readByte + (RXBuffer[0] & 0x07); // 4 - four bytes of header.
			}
			else if (i > 1)
			{
				RXBuffer[i++] = readByte;
			}

			if (i == expectedLength) // All the data is read
			{
				if (RXBuffer[0] == 0x00)
				{
					memcpy(pBuff, RXBuffer, expectedLength);
					*bytesActuallyRead = expectedLength;
					result = 1;
					retry = 0; // No need to retry we got the response.
					break;
				}
				expectedLength = 0;
				i = 0;
			}
		}
		retry--;
	}
	return result;
}

//Return 1 on success, 0 on failure
int read_message()
{
	const struct ble_msg *apimsg;
	struct ble_header apihdr;
	unsigned char data[256];//enough for BLE
	int byteActuallyRead = 0;

	read_response(data, &byteActuallyRead);

	apihdr.type_hilen = data[0];
	apihdr.lolen = data[1];
	apihdr.cls = data[2];
	apihdr.command = data[3];

	apimsg = ble_get_msg_hdr(apihdr);
	if (!apimsg)
	{
		printf("ERROR: Message not found:%d:%d\n", (int)apihdr.cls, (int)apihdr.command);
		return 0;
	}
	apimsg->handler(&data[4]);

	return 1;
}

//Return 1 on success, 0 on failure
int setPortSettings(char *portName)
{
	char mode_str[128] = "baud=57600 data=8 parity=n stop=1 dtr=on rts=on";	//57600, "8N1"

	char comport[80];
	_snprintf(comport, sizeof(comport)-1, "\\\\.\\%s", portName);

	serial_handle = CreateFile(comport,
		GENERIC_READ | GENERIC_WRITE,
		0,                          /* no share  */
		NULL,                       /* no security */
		OPEN_EXISTING,
		0,                          /* no threads */
		NULL);                      /* no templates */

	if (serial_handle == INVALID_HANDLE_VALUE)
	{
		printf("unable to open comport\n");
		return 0;
	}

	DCB port_settings;
	memset(&port_settings, 0, sizeof(port_settings));  /* clear the new struct  */
	port_settings.DCBlength = sizeof(port_settings);

	if (!BuildCommDCBA(mode_str, &port_settings))
	{
		printf("unable to set comport dcb settings\n");
		CloseHandle(serial_handle);
		return 0;
	}

	if (!SetCommState(serial_handle, &port_settings))
	{
		printf("unable to set comport cfg settings\n");
		CloseHandle(serial_handle);
		return 0;
	}

	COMMTIMEOUTS Cptimeouts;

	Cptimeouts.ReadIntervalTimeout = MAXDWORD;
	Cptimeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
	Cptimeouts.ReadTotalTimeoutConstant = 1;
	Cptimeouts.WriteTotalTimeoutMultiplier = 0;
	Cptimeouts.WriteTotalTimeoutConstant = 0;

	if (!SetCommTimeouts(serial_handle, &Cptimeouts))
	{
		printf("unable to set comport time-out settings\n");
		CloseHandle(serial_handle);
		return 0;
	}

	EscapeCommFunction(serial_handle, SETDTR);	//enableDTR

	return 1;
}

//Return 1 on success, 0 on failure
int DiscoverFor(int timeInterval)
{
	int scanTimeCount = timeInterval * 10; // 1 sec = 10 iteration of loop.
	int maxRetry = scanTimeCount;
	unsigned char RXBuffer[2048] = { 0 }; // Buffer to store the bytes read from the port.
	int expectedLength = 0;
	int i = 0;
	int bytesToRead = 1;
	unsigned char readByte = 0;
	DWORD rread = 0;

	while (maxRetry > 0)
	{
		/* This is a state machine designed for reading the scan response events. It work as follows :
		* Get the complete response or event whatever it received in the buffer.
		* Calculates the total length of data received.
		* If the received data is event then it will send that event for parsing and if it is a response
		* then it will skip that response.
		*/
		while (ReadFile(serial_handle, &readByte, bytesToRead, &rread, NULL) && rread)
		{

			if ((i == 0) && (readByte == 0x80 || readByte == 0x00)) // Scan Response event 
			{
				RXBuffer[i++] = readByte;

			}
			else if (i == 1)
			{
				RXBuffer[i++] = readByte; // Length of the payload received as response
				expectedLength = 4 + readByte + (RXBuffer[0] & 0x07); // 4 - four bytes of header.

			}
			else if (i > 1)
			{
				RXBuffer[i++] = readByte;
			}

			if (i == expectedLength) // All the data is read
			{
				if (RXBuffer[0] == 0x80)
				{
					ParseResponseData(RXBuffer, i);
				}
				expectedLength = 0;
				i = 0;
				break;
			}
		}
		maxRetry--;
		Sleep(100);
	}
	return 1;
}

void ParseResponseData(unsigned char *buf, int bufferLength)
{
	const struct ble_msg *apimsg;
	struct ble_header apihdr;

	apihdr.type_hilen = buf[0];
	apihdr.lolen = buf[1];
	apihdr.cls = buf[2];
	apihdr.command = buf[3];

	apimsg = ble_get_msg_hdr(apihdr);
	if (!apimsg)
	{
		printf("ERROR: Message not found:%d:%d\n", (int)apihdr.cls, (int)apihdr.command);
		return;
	}
	apimsg->handler(&buf[4]);

	return;
}
