// RF IDeas, Inc. Proprietary and Confidential
// Copyright © 2016 RF IDeas, Inc. as an unpublished work.
// All Rights Reserved

#include "serialdriver.h"
#include "pcProxAPI.h"

enum {
	BLE_READER = 0,
	ALL_BLE_DEVICES = 1
};

int checkSerialResponse();
extern int lastCommandSuccess;
extern int packetRecieved;
extern int displayScanResponse;
int devCnt = 0;

BOOL WINAPI consoleHandler(DWORD signal) {

	printf("\n\nStopping scan...Please wait for sometime\n\n");
	if (signal == CTRL_C_EVENT)
	{
		lastCommandSuccess = 0;
		ble_cmd_gap_end_procedure();	//Disable scan

		if (!checkSerialResponse()){
			printf("Disable Scan");
		}
		CloseHandle(serial_handle);
		USBDisconnect();
		exit(0);
	}

	return TRUE;
}

int checkSerialResponse()
{
	int result = 1;
	if (!read_message()){
		printf("\nCannot read from comport...");
		result = 0;
	}

	else if (!lastCommandSuccess){
		printf("\nAPI Failed...");
		result = 0;
	}
	return result;
}

void helpText()
{
	printf("Correct format of passing argument is :\n");
	printf("scanner.exe scan <port name>\t\t Display information of nearby BLE_Reader\n");
	printf("scanner.exe scan <port name> raw\t Display information of nearby BLE devices\n");
}

int main(int argc, char* argv[])
{
	if (!(argc == 3 || argc == 4))
	{
		printf("Wrong number of arguments passed.\n");
		helpText();
		return 0;
	}

	if (strcmp(argv[1], "scan") != 0)
	{
		printf("Invalid Argument Passed\n");
		helpText();
		return 0;
	}

	if (argc == 4)
	{
		if (strcmp(argv[3], "raw") != 0)
		{
			printf("Invalid Argument Passed\n");
			helpText();
			return 0;
		}
	}

	char *portName = argv[2];

	if (usbConnect() == 0)
	{
		printf("\n No reader connected\n");
		return 0;
	}


	int isBLEReader = 0;
	int devCnt = GetDevCnt();
	for (int index = 0; index < devCnt; index++)
	{
		SetActDev(index);
		if (IsBTLEPresent()) {
			SetBTLEConfiguration(BTLEON_RADIOON);
			isBLEReader = 1;
		}
	}

	if (!isBLEReader)
	{
		printf("This is not a BLE_Reader\n");
		USBDisconnect();
		return 0;
	}

	if (!setPortSettings(portName)){
		USBDisconnect();
		return 0;
	}

	/*
	**BLE module requires 600ms to bootload, so a delay may be required
	**before sending serial commands to the BLE module. But this is a case
	**only when reader is plugged for the first time. After that this sleep
	**is not required.
	*/
	Sleep(600);

	bglib_output = output;	//Callback for writing on comport

	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)consoleHandler, TRUE)) {
		printf("\nERROR: Could not set control handler");
		return 1;
	}
	printf("\nPRESS CTRL + C TO STOP SCANNING\n");

	lastCommandSuccess = 0;
	//If scanning is disabled already, then disable scan api will fail.
	//Therefore we will not check whether this api gets failed or not.
	ble_cmd_gap_end_procedure();	//Disable scan

	if (!read_message()){
		printf("\nCannot read from comport...");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	lastCommandSuccess = 0;
	ble_cmd_gap_set_scan_parameters(0xC8, 0xC8, 1);	//Set scan parameters, 1 = Active scanning

	if (!checkSerialResponse()){
		printf("Set scan parameters");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	lastCommandSuccess = 0;
	ble_cmd_gap_discover(gap_discover_generic);	//Enable scan or discover

	if (!checkSerialResponse()){
		printf("Set scan parameters");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	if (argc == 3){
		displayScanResponse = BLE_READER;
	}
	else if (argc == 4){
		displayScanResponse = ALL_BLE_DEVICES;
	}

	int ret = DiscoverFor(30);

	lastCommandSuccess = 0;
	ble_cmd_gap_end_procedure();	//Disable scan

	if (!checkSerialResponse()){
		printf("Disable Scan");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	if (packetRecieved == 0)
	{
		if (argc == 3){
			printf("\nNO BLE_READER FOUND NEARBY\n");
		}
		else if (argc == 4){
			printf("\nNO BLE DEVICE FOUND NEARBY\n");
		}
	}

	CloseHandle(serial_handle);
	USBDisconnect();
	return 0;
}

// Copyright © RF IDeas, Inc. Proprietary and confidential.
// EOF
