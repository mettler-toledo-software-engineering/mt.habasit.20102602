# Bluetooth Low Energy #


## Synopsis:- ##

It's a relatively small project which demonstrates how to use Bluetooth hardware of BLE_Reader. This Bluetooth hardware ( Bluegiga's BLE113 chip) works on a `command`, `response` and `event` based protocol. This application is a very basic demonstration on how to use their SDK.

## Application Overview:- ##

The application uses `Bluegiga APIs` to communicate through Bluetooth hardware of BLE_Reader. 
This application consists of two relatively small command line applications
called `beacon.exe` and `scanner.exe`. As you can guess with theirs names
that `beacon.exe` will turn an BLE_Reader into a beacon which
will advertise it's part-number, firmware details and device name. At the same
time, `scanner.exe` will scan the nearby BLE_Reader and will show their
information in the tabular form.

## Prerequisite:- ##

1. VC++ compiler tools
2. MSBuild

**Note**: If you have already installed *Visual Studio* then, the above-mentioned tools are automatically installed.

## How to Build:- ##

- Open a command prompt and switch to the directory from where you've opened this *readme*.
- Make sure that you have `MSBuild` available in your PATH environment variable.
- Add an environment variable VS_TOOLSET and assign appropriate value to it based on the Visual Studio installed in your system. Refer the below information to know what will be the value of VS_TOOLSET.

  - For Visual Studio 2019: VS_TOOLSET=v142
  - For Visual Studio 2017: VS_TOOLSET=v141
  - For Visual Studio 2015: VS_TOOLSET=v140
  - For Visual Studio 2013: VS_TOOLSET=v120

- Execute the command below to build both beacon and scanner:-

 `MSBuild BLE_Example.sln /t:Rebuild /p:Configuration=Release /p:Platform=win32`

Congratulations! You're ready to use both applications.

  **Note**: *It'll generate the binaries in release folder.*

## How to Use:- ##

Set the path of pcProxAPI.dll before running the application.
Command to set the path:

`set path=%path%;<pcProxAPI.dll path>`

- **Starting Advertisement:**

    `beacon.exe COM<port-number> <device-name>`

	 **NOTE:**

     1. *It will keep advertising until we press CTRL+C*.

	 2. *If you want to advertise with multiple readers then you have to connect the readers to different machines.*

- **Start Scanning:**

    `scanner.exe scan COM<port-number>`

     **NOTE:** *It will scan the air for nearby BLE_Readers for approx 30
    seconds and will terminate automatically.*

	`scanner.exe scan COM<port-number> raw`

	 **NOTE:** *It will scan the air for all the nearby BLE devices for approx 30
   second and will terminate automatically.*

	 **To stop scanning in between Press CTRL+C.**

## Warning:- ##

- Don't unplug the reader abruptly while advertising or scanning. Always stop gracefully otherwise it could damage hardware.
