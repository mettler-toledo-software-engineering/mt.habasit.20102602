// RF IDeas, Inc. Proprietary and Confidential
// Copyright © 2016 RF IDeas, Inc. as an unpublished work.
// All Rights Reserved

#include "serialdriver.h"
#include "pcProxAPI.h"
#include <sstream>
extern int lastCommandSuccess;

int devCnt = 0;

int checkSerialResponse()
{
	int result = 1;
	if (!read_message()){
		printf("\nCannot read from comport...");
		result = 0;
	}

	else if (!lastCommandSuccess){
		printf("\nAPI Failed...");
		result = 0;
	}
	return result;
}

BOOL WINAPI consoleHandler(DWORD signal) {

	printf("stopping advertisement\n");
	if (signal == CTRL_C_EVENT)
	{
		lastCommandSuccess = 0;
		ble_cmd_gap_set_mode(gap_non_discoverable, gap_non_connectable);	//Disable Advertisement

		if (!checkSerialResponse()){
			printf("Disable Advertisement");
		}
		CloseHandle(serial_handle);
		USBDisconnect();
		exit(0);
	}
	return TRUE;
}

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		printf("Wrong number of arguments passed.\n");
		printf("beacon.exe <Comport> <DeviceName>\n");
		return 0;
	}

	char *deviceName = argv[2];

	int lenOfDeviceName = strlen(deviceName);
	if (lenOfDeviceName > 6)
	{
		printf("Device Name length is more than 6 characters\n");
		return 0;
	}

	SetDevTypeSrch(PRXDEVTYP_USB);
	if (usbConnect() == 0)
	{
		printf("\nNo reader connected\n");
		return 0;
	}

	int isBLEReader = 0;
	int devCnt = GetDevCnt();
	for (int index = 0; index < devCnt; index++)
	{
		SetActDev(index);
		if (IsBTLEPresent()) {
			SetBTLEConfiguration(BTLEON_RADIOON);
			isBLEReader = 1;
		}
	}

	if (!isBLEReader)
	{
		printf("This is not a BLE_Reader\n");
		USBDisconnect();
		return 0;
	}

	char *portName = argv[1];
	if (!setPortSettings(portName)){
		USBDisconnect();
		return 0;
	}

	/*
	**BLE module requires 600ms to bootload, so a delay may be required
	**before sending serial commands to the BLE module. But this is a case
	**only when reader is plugged for the first time. After that this sleep
	**is not required.
	*/
	Sleep(600);

	bglib_output = output;	//Callback for writing on comport

	lastCommandSuccess = 0;
	ble_cmd_gap_set_mode(gap_non_discoverable, gap_non_connectable);	//Disable Advertisement

	if (!checkSerialResponse()){
		printf("Disable Advertisement");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	lastCommandSuccess = 0;
	ble_cmd_gap_set_adv_parameters(0x200, 0x200, 0x07);	//Set Adv Parameters

	if (!checkSerialResponse()){
		printf("Set Adv Parameters");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

//Set Adv Data --------->
	char beaconArray[256] = { 0 };
	char beaconPacket[256] = { 0 };
	char firmwareVersion[256] = { 0 };

	const char *partNumber = getPartNumberString();

		DWORD firmware = GetFirmwareVersion(0, 0);
		int major = (firmware >> 24) & 255; // major
		int minor = (firmware >> 16) & 255; // minor
		int tiny = (firmware >> 8) & 255;  // tiny
		int variant = firmware & 255;       // variant
		sprintf(firmwareVersion, "%d.%d.%d.%d", major, minor, tiny, variant);

	// Creating a complete data that will be broadcast.
	strcpy(beaconArray, partNumber);
	strcat(beaconArray, firmwareVersion);
	strcat(beaconArray, deviceName);
	int beaconLength = strlen(beaconArray);
	beaconPacket[0] = 0x02; // ad field length - 2 bytes
	beaconPacket[1] = 0x01; // ad field type - 0x01 (Flags)
	beaconPacket[2] = 0x06; // general discoverable, BR/EDR not supported
	beaconPacket[3] = beaconLength + 1; // ad field length 
	beaconPacket[4] = 0x09; // ad field type - 0x09 (complete local name)
	memcpy(&beaconPacket[5], beaconArray, beaconLength);
	int beaconPacketLength = beaconLength + 5;

	lastCommandSuccess = 0;
	ble_cmd_gap_set_adv_data(0x00, beaconPacketLength, beaconPacket);		//Set Adv Data

	if (!checkSerialResponse()){
		printf("Set Adv Data");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}
//<-----------Set Adv Data

	lastCommandSuccess = 0;
	ble_cmd_gap_set_mode(gap_user_data, gap_undirected_connectable);	//Enable Advertisement

	if (!checkSerialResponse()){
		printf("Enable Advertisement");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	lastCommandSuccess = 0;
	ble_cmd_system_address_get();	//Mac Address

	if (!checkSerialResponse()){
		printf("Mac Address");
		CloseHandle(serial_handle);
		USBDisconnect();
		return 0;
	}

	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)consoleHandler, TRUE)) {
		printf("\nERROR: Could not set control handler");
		return 1;
	}
	while (1)
	{
		Sleep(10); // To keep main thread alive.
	}

	return 0;
}

// Copyright © RF IDeas, Inc. Proprietary and confidential.
// EOF