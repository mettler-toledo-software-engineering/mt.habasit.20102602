﻿' RF IDeas, Inc. Proprietary and Confidential
' Copyright © 2016 RF IDeas, Inc. as an unpublished work.
' All Rights Reserved

'Author : IntimeTec Visionsoft Private Limited.
'Creation Date : 18 Feb,2017
'This example merely a proof of concept that rfideas's
'SDK(which is a C/C++ library) can be used in VB.NET.

Imports System.Runtime.InteropServices
Imports System.Threading

Module Module1

    Sub printHelpText()
        Dim processName As String = Process.GetCurrentProcess().ProcessName
        Dim helpText As String = [String].Format(vbNewLine & "Usages: {0:s} [Options]" & vbNewLine & vbNewLine, processName)
        helpText += "--enumerate" & vbTab & "list all the connected rfidea's readers" & vbNewLine
        helpText += "--sdk-version" & vbTab & "give the sdk version" & vbNewLine
        helpText += "--help" & vbTab & "        print this help" & vbNewLine
        helpText += "--getid" & vbTab & "        give raw data of card which being read" & vbNewLine
        helpText += "--getESN" & vbTab & "read ESN from the reader" & vbNewLine
        Console.WriteLine(helpText)
    End Sub

    Sub Main(args As String())
        Dim toalNbOfArgs As Integer = args.Length
        If toalNbOfArgs = 0 Then
            printHelpText()
        Else
            ' extend path variable to find DLL a proper place
            Dim existingPathValue As String = Environment.GetEnvironmentVariable("PATH")
            If Environment.Is64BitProcess = True Then
                Environment.SetEnvironmentVariable("PATH", existingPathValue + ";" + "..\..\..\..\lib\64")
            Else
                Environment.SetEnvironmentVariable("PATH", existingPathValue + ";" + "..\..\..\..\lib\32")
            End If
            Dim firstArg As String = args.GetValue(0)
            If firstArg = "--enumerate" Then
                ListAllRFIDevices()
            ElseIf firstArg = "--sdk-version" Then
                HandleSdkVersionReq()
            ElseIf firstArg = "--getid" Then
                HandleGetIdReq()
            ElseIf firstArg = "--getESN" Then
                HandleGetESNReq()
            ElseIf firstArg = "--help" Then
                printHelpText()
            Else
                Console.WriteLine(vbNewLine & "Invalid Argument..." & vbNewLine)
                printHelpText()
            End If
        End If
        ' to halt the screen only in debug mode
#If DEBUG Then
        Console.WriteLine("Press Enter to exit")
        Console.ReadLine()
#End If
    End Sub

    Private Sub ListAllRFIDevices()
        ' search only USB devices
        PcProxAPIWrapper.SetDevTypeSrch(PcProxAPIWrapper.PRXDEVTYP_USB)

        If PcProxAPIWrapper.usbConnect() Then
            Dim nbOfActiveDevices As Short = PcProxAPIWrapper.GetDevCnt()
            ' Underscore is a continuous character in VB will not appear in actual 
            ' output.
            Dim list_header As String = [String].Format(vbNewLine & _
                                            "{0:s}" & vbTab & vbTab _
                                            & "{1:s}" & vbTab & vbTab & vbTab & vbTab _
                                            & "{2:s}" & vbNewLine, _
                                            "PartNumber", "Vid:Pid", "LUID")
            Console.WriteLine(list_header)
            For i As Short = 0 To nbOfActiveDevices - 1
                Dim rc As Short = PcProxAPIWrapper.SetActDev(i)
                If rc Then
                    Dim parNumber As String = Marshal.PtrToStringAnsi(PcProxAPIWrapper.getPartNumberString())
                    Dim vid_pid_product_name As String = Marshal.PtrToStringAnsi(PcProxAPIWrapper.GetVidPidVendorName())
                    Dim luid As Integer = PcProxAPIWrapper.GetLUID()
                    Dim reader_record As String = [String].Format(vbNewLine & "{0:s}" & _
                                                                  vbTab & vbTab & _
                                                                  "{1:s}" & vbTab & vbTab & _
                                                                  "{2:d}", _
                                                                  parNumber, vid_pid_product_name, _
                                                                  luid)
                    Console.WriteLine(reader_record)
                Else
                    Console.WriteLine("Failed to set Active device..")
                End If
            Next
            Console.WriteLine()
            PcProxAPIWrapper.USBDisconnect()
        Else
            Console.WriteLine(vbNewLine & "Reader Not Connected" & vbNewLine)
        End If

    End Sub

    Private Sub HandleGetIdReq()
        If PcProxAPIWrapper.usbConnect() Then
            Dim buff_size As Short = 32
            Dim data_arr(buff_size) As Byte
            Dim data As IntPtr = Marshal.AllocHGlobal(32 * Marshal.SizeOf(GetType(Integer)))
            ' sleep is required to get active id 
            Thread.Sleep(250)
            Dim nbBits As Short = PcProxAPIWrapper.GetActiveID32(data, buff_size)
            If nbBits = 0 Then
                Console.WriteLine(vbNewLine & "No Id Found, Please put card on the reader" _
                                  & "and make sure it must be configured with the card placed on it" _
                                  & vbNewLine)
            Else
                Dim Bytes As Integer = (nbBits + 7) / 8
                If Bytes < 8 Then
                    Bytes = 8 ' Minimum 8 byte data will be given
                End If
                Marshal.Copy(data, data_arr, 0, buff_size)
                Dim formatedCardData As String = ""
                For i As Integer = 0 To Bytes - 1
                    Dim hex_byte As String = String.Format("{0:X2} ", data_arr.GetValue(i))
                    formatedCardData = hex_byte + formatedCardData
                Next
                Dim final_output As String = [String].Format(vbNewLine & "{0:d} Bit : {1:s}" _
                                                             & vbNewLine, nbBits, formatedCardData)
                Console.WriteLine(final_output)
            End If
        Else
            Console.WriteLine(vbNewLine & "Reader Not Connected" & vbNewLine)
        End If
    End Sub

    Private Sub HandleSdkVersionReq()
        Dim major As Short, minor As Short, dev As Short
        PcProxAPIWrapper.GetLibVersion(major, minor, dev)
        Dim sdkVersion As String = [String].Format("sdk version : {0:d}.{1:d}.{2:d}", major, minor, dev)
        Console.WriteLine() ' new line
        Console.WriteLine(sdkVersion)

    End Sub

    Private Sub HandleGetESNReq()
        ' search only USB devices
        PcProxAPIWrapper.SetDevTypeSrch(PcProxAPIWrapper.PRXDEVTYP_USB)

        If PcProxAPIWrapper.usbConnect() Then
            
            Dim ESN As String = Marshal.PtrToStringAnsi(PcProxAPIWrapper.getESN())
            If ESN Is Nothing Then
                Console.WriteLine(vbNewLine & "Reader does not support ESN functionality")
            Else
                Console.WriteLine(vbNewLine & "ESN: " & ESN)
            End If
            PcProxAPIWrapper.USBDisconnect()
        Else
            Console.WriteLine(vbNewLine & "Reader Not Connected")
        End If

    End Sub

End Module

' Copyright © RF IDeas, Inc. Proprietary and confidential.
' EOF