﻿' RF IDeas, Inc. Proprietary and Confidential
' Copyright © 2016 RF IDeas, Inc. as an unpublished work.
' All Rights Reserved

Imports System.Runtime.InteropServices
Public Class PcProxAPIWrapper
    Public Const PRXDEVTYP_ALL = -1   ' All types of interfaces USB, Serial, etc...
    Public Const PRXDEVTYP_USB = 0   ' USB only
    Public Const PRXDEVTYP_SER = 1   ' Serial (Virtual COM & RS-232) Only
    Public Const PRXDEVTYP_TCP = 2   ' TCP/IP

    ' Wrappers for pcprox DLL
    <DllImport("pcProxAPI.dll")> Public Shared Function GetLUID() As Integer
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function GetVidPidVendorName() As IntPtr
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function getPartNumberString() As IntPtr
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function GetDevCnt() As Short
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function usbConnect() As UShort
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function USBDisconnect() As UShort
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function SetDevTypeSrch(ByVal iSrchType As Short) As UShort
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function SetActDev(ByVal iSrchType As Short) As UShort
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function GetLibVersion(ByRef Major As Short, ByRef Minor As Short, ByRef dev As Short) As UShort
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function GetActiveID32(ByVal Major As IntPtr, ByVal Size As Short) As Short
    End Function
    <DllImport("pcProxAPI.dll")> Public Shared Function getESN() As IntPtr
    End Function

End Class

' Copyright © RF IDeas, Inc. Proprietary and confidential.
' EOF