Synopsis :
----------

This is a C# example to show how rfideas's SDK can be used with C#.
This application can be used to list all rfidea's devices, can be used to know
the current version etc.

Prerequisites\Dependencies :
--------------

1. Visual Studio
2. pcProxAPI.dll  while running on windows
3. Make sure that you have 'MSBuild' available in your PATH environment variable.


How to Build and Run:
--------------------

1. Build solution by using following command.

   "Msbuild" readercomm.sln /p:Configuration=Release /t:Rebuild
  
    Note: location of msbuild program may depend where you have installed it. 
 
2. Switch to bin\Release directory by using following command.
   
   cd bin\Release

3. Run following command to see what are the options available 

   readercomm --help
 

 All supported arguments :
 -------------------------

 Usages: readercomm [Options]

 --enumerate     list all the connected rfidea's readers
 --sdk-version   give the SDK version
 --help          print this help
 --getid         give raw data of card which being read
 --getESN        read the ESN from the reader



Frequently Asked Questions(FAQ) :
--------------------------------

1. What to do when I get Unhandled Exception: System.BadImageFormatException: 
An attempt was made to load a program with an incorrect format. (Exception from HRESULT: 0x8007000B) error?

  In default setup you should not get this error, but this happens when 64 bit compiled application
  tries to access 32 bit DLL or vice versa, please configure PATH variable to point to appropriate
  DLL. 

2. Why do I get "No Reader Connected.." even though reader is connected physically?

It happens because you might have connected a reader other than USB as this sample supports USB readers only.  

3. Why do I get "No Id Found, Please put card on the reader and make sure it must be configured with the card placed on it"
even though a correct card is placed on the reader?

It might happens because the connected reader does not support GetActiveID32 Api.

4. Why do I get "Unhandled Exception: System.DllNotFoundException: Unable to load DLL 'pcProxAPI.dll': 
The specified module could not be found. (Exception from HRESULT: 0x8007007E)" error?

  In default setup you should not get this error, but this happens when there is no 
  DLL available on PATH. Please configure PATH variable. 