﻿// RF IDeas, Inc. Proprietary and Confidential
// Copyright © 2016 RF IDeas, Inc. as an unpublished work.
// All Rights Reserved

/*
Author : IntimeTec Visionsoft Private Limited.
Creation Date : 17 Feb,2017
This example merely a proof of concept that rfideas's
SDK(which is a C/C++ library) can be used in C#.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
namespace readercomm
{
    class Program
    {
        public const short PRXDEVTYP_USB = 0;
        static void Main(string[] args)
        {

            pcproxlib.setLibPath();
            if(args.Length == 0)
            {
                Console.WriteLine("\n Must pass an argument");
                helpText();
                return;
            }
            
            if(!(args[0]=="--enumerate" || args[0]=="--sdk-version" || args[0]=="--getid"|| args[0] == "--getESN" || args[0]=="--help" ) )
            {
                Console.WriteLine("\n Invalid argument\n");
                helpText();
                return;
            }

            if(args[0] == "--help")
            {
                helpText();
                return;
            }

            if (args[0] == "--enumerate")
            {
                listAllConnectedDevice();
            }


            if(args[0] == "--sdk-version")
            {
                getLibVersion();
            }

            if(args[0] == "--getid")
            {
                getActiveID();
            }

            if (args[0] == "--getESN")
            {
                getEsn();
            }
        }

        //function for listing all the connected device
            public static void listAllConnectedDevice()
            {
                pcproxlib.SetDevTypeSrch(PRXDEVTYP_USB);
                int rc = pcproxlib.usbConnect();
                if (rc == 1)
                {
                    int deviceCount = pcproxlib.GetDevCnt();
                    Console.WriteLine("\nPartNumber\t\tVid/Pid\t\t\t\tLUID\n");
                    for (short i = 0; i < deviceCount; i++)
                    {
                        pcproxlib.SetActDev(i);
                        String partNumber = getPartNumber();
                        if (partNumber == null)
                            Console.WriteLine("\n Unable to get partnumber\n");
                        String vidpid = getVidPid();
                        if (vidpid == null)
                            Console.WriteLine("\n Unable to get vid pid\n");
                        int luid = pcproxlib.GetLUID();
                        Console.WriteLine(partNumber + "\t\t" + vidpid + "\t\t" + luid + "\n");
                    }
                }
                else
                {
                    Console.WriteLine("\n Reader Not Connected\n");
                }

            }

        /*wrapper function for getPartnumberString.
         * will return the partnumber 
         * In case of failure it will return Null
         */ 
         public static String getPartNumber()
        {
            IntPtr ret = pcproxlib.getPartNumberString();
            string partNumber = Marshal.PtrToStringAnsi(ret);
            return partNumber;
        }

         /*wrapper function for getVidPid.
          * will return the vidpid 
          * In case of failure it will return Null
          */ 
        public static String getVidPid()
        {
            IntPtr retvidpid = pcproxlib.GetVidPidVendorName();
            string vidpid = Marshal.PtrToStringAnsi(retvidpid);
            return vidpid;
        }
         
        /* wrapper function for getSdkVersion
         * will print  the SDK version in following format
         * <Major>.<Minor>.<Dev>.
         */
        public static void getLibVersion()
        {
            short maj = 0;
            short min = 0;
            short ver = 0;//min, ver;
            pcproxlib.GetLibVersion(ref maj, ref min, ref ver);
            Console.WriteLine("\nSDK-Version :\t" + maj + "." + min + "." + ver + "\n");
        }

        /*wrapper function for getRawData
         * will print the rawdata of card in following format
         * <Bits>,<RawData>
         */
        public static void getActiveID()
        {
            pcproxlib.SetDevTypeSrch(PRXDEVTYP_USB);
            int rc = pcproxlib.usbConnect();
            Thread.Sleep(2500);
            if (rc == 1)
            {
                IntPtr result1 = Marshal.AllocHGlobal(32 * sizeof(int));
                byte[] arr = new byte[32];
                int nBits = pcproxlib.GetActiveID32(result1, 32);
                if(nBits == 0)
                {
                    Console.WriteLine("\nNo Id Found, Please put card on the reader and make sure it must be configured with the card placed on it");
                    return;
                }
                int Bytes = (nBits + 7) / 8;
                if (Bytes < 8)
                {
                    Bytes = 8;
                }
                Marshal.Copy(result1, arr, 0, 32);
                String cardData = "";

                for (int i = 0; i < Bytes; i++)
                {
                    String data = String.Format("{0:X2} ", arr[i]);
                    cardData = data + cardData;
                }
                Console.WriteLine("\n" + nBits+"Bits" + ": " + cardData);

                Marshal.FreeHGlobal(result1);
            }
            else
            {
                Console.WriteLine("\n Reader Not Connected");
            }
        }

        public static void getEsn()
        {
            pcproxlib.SetDevTypeSrch(PRXDEVTYP_USB);
            int rc = pcproxlib.usbConnect();
            if (rc == 1)
            {
                    string ESN = Marshal.PtrToStringAnsi(pcproxlib.getESN());
                    if (ESN != null)
                    {
                        Console.WriteLine("\nESN: " + ESN);
                    }
                    else
                    {
                        Console.WriteLine("\nReader does not support ESN functionality");
                    }
            }
            else
            {
                Console.WriteLine("\n Reader Not Connected\n");
            }
        }

        //Function for printing help text
        public static void helpText()
        {
            String programName = Process.GetCurrentProcess().ProcessName;
            Console.WriteLine("\nusage:"+"\t"+programName+"\t[options]\n\n ");
            Console.WriteLine("--enumerate\t\tlist all the connected rfidea's readers \n" +
                "--sdk-version\t\tgive the sdk version \n" +
                "--getid\t\t\tgive raw data of card which being read\n"+
                "--getESN\t\tread the ESN from the reader\n" +
                "--help\t\t\tprint help menu");
        }

        }
    }

// Copyright © RF IDeas, Inc. Proprietary and confidential.
// EOF
