﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Infrastructure;
using MT.Habasit.Logic.Connections;
using MT.Habasit.Logic;
using MT.Habasit.Logic.BarcodeReader;
using log4net;
using MT.Singularity.Logging;
using MT.Habasit.Logic.USB;
using MT.Habasit.Config;
using System.IO;
using System.ComponentModel;
using MT.Habasit.Logic.LegicKeyReader;

namespace MT.Habasit.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private Configuration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);

                //  customerComponent.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
                CompositionContainer.AddInstance(await LegicKeyReader.CreateAsync());
                CompositionContainer.AddInstance<IComponents>(customerComponent);

                if (Directory.Exists(Globals.SQLIteX86Driver))
                {
                    if (!File.Exists(Path.Combine(Globals.SQLIteX86Driver, "SQLite.Interop.dll")))
                    {
                        byte[] clistAsByteArray = Properties.Resources.SQLite_Interop;
                        File.WriteAllBytes(Path.Combine(Globals.SQLIteX86Driver, "SQLite.Interop.dll"), clistAsByteArray);
                    }
                }

                var configuration = await customerComponent.GetConfigurationAsync();

                SetupDevices();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
        }

        private void RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;

                ConfigureBarcodeScanner(_configuration.BarcodeScannerPort);
                _configuration.PropertyChanged += ConfigurationPropertyChanged;
            }
        }

        private void ConfigureBarcodeScanner(object barcodeScannerPort)
        {
            DisposeBarcodeScanner();
            InitBarcodeScanner();
        }

        private void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //todo change des eod zeichens berücksichtigen
            if (e.PropertyName == "BarcodeScannerPort")
            {
                ConfigureBarcodeScanner(_configuration.BarcodeScannerPort);
            }
        }

        private void SetupDevices()
        {
            try
            {
                var connection = new UsbDeviceConnection(Globals.BarcodeReaderName).GetDeviceConnection();
                if (connection != null)
                {
                    var barcodereader = new UsbBarcodeReader(connection, EndOfDataCharacter.CR);
                    CompositionContainer.AddInstance<IBarcodeReader>(barcodereader);
                }

            }
            catch (Exception ex)
            {
                _log.Error("Setup BarcodeReader Failed", ex);
            }


            var usbHandler = new UsbHandler(Globals.IND930USBDrive);
            CompositionContainer.AddInstance<IUsbHandler>(usbHandler);
        }

        private void DisposeBarcodeScanner()
        {
            IBarcodeReader barcodereader;
            var success = CompositionContainer.TryResolve(out barcodereader);

            if (success)
            {
                var usbdevice = new USBDevice(Globals.BarcodeReaderName);
                if (usbdevice.GetPortName() == "")
                {
                    barcodereader.Dispose();
                }
            }
        }

        private void InitBarcodeScanner()
        {
            var connection = new UsbDeviceConnection(Globals.BarcodeReaderName).GetDeviceConnection();
            IBarcodeReader barcodereader;
            var success = CompositionContainer.TryResolve(out barcodereader);
            if (success == true)
            {
                if (barcodereader.Initialized == false)
                {
                    barcodereader.InitBarcodeReader(connection);
                }
            }
            else
            {
                try
                {
                    barcodereader = new UsbBarcodeReader(connection, EndOfDataCharacter.CR);
                    CompositionContainer.AddInstance(barcodereader);
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Setup BarcodeReader Failed", ex);
                }
            }

        }
    }
}
