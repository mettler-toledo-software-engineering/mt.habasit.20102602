﻿using System;

using System.Collections.Generic;
using System.Text;
using Dapper.Contrib.Extensions;
using MT.Habasit.Logic;

namespace MT.Habasit.Models
{
    [Table("TareModels")]
    public class TareModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime LastChanged { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public double Value { get; set; }
        public string Unit { get; set; }
        [Computed]
        public string ValueAsString
        {
            get { return $"{Value} {Unit}"; }
        }

        public TareModel()
        {
            SetCurrentUnit();
        }

        public TareModel(string description, string value)
        {
            Description = description;
            SplitValueAndUnit(value);
        }

        public TareModel(string description, double value, string unit)
        {
            Description = description;
            Value = value;
            Unit = unit;
        }

        public override string ToString()
        {
            return $"{Description} ({Value} {Unit})";
        }        

        private void SplitValueAndUnit(string valueToSplit)
        {
            string[] content = valueToSplit.Split(' ');

            Value = double.Parse(content[0]);
            Unit = content[1];
        }

        private void SetCurrentUnit()
        {
            Unit = Globals.GetCurrentScaleUnit();
        }
    }
}
