﻿namespace MT.Habasit.Models
{
    public class ExportModel
    {
        public string Index { get; set; }
        public int Number { get; set; }
        public string ACode { get; set; }
        public string UserKey { get; set; }
        public double NetWeight { get; set; }
        public double GrossWeight { get; set; }
        public double TareWeight { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

        public ExportModel()
        {
        }
    }
}
