﻿using Dapper.Contrib.Extensions;
using System;

namespace MT.Habasit.Models
{
    [Table("UserModels")]
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime LastChanged { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Key { get; set; }
        public string UID { get; set; }
        [Computed]
        public string Name
        {
            get { return $"{Firstname} {Lastname}"; }
        }

        public UserModel()
        {
        }

        public UserModel(string key, string firstname, string lastname)
        {
            Key = key;
            Firstname = firstname;
            Lastname = lastname;
        }

        public override string ToString()
        {
            return $"{Firstname} {Lastname}, {Key}";
        }
    }
}
