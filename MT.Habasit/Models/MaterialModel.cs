﻿using System;
using Dapper.Contrib.Extensions;

namespace MT.Habasit.Models
{
    [Table("MaterialModels")]
    public class MaterialModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime LastChanged { get; set; }
        public string Type { get; set; }
        public string System { get; set; }
        public string Code { get; set; }

        public MaterialModel()
        {
        }

        public MaterialModel(string name)
        {
            Type = name;
        }

        public MaterialModel(string name, string code)
        {
            Type = name;
            Code = code;
        }

        public override string ToString()
        {
            return $"{Type} {System}, {Code}";
        }
    }
}
