﻿using System;
using Dapper.Contrib.Extensions;

namespace MT.Habasit.Models
{
    [Table("TransactionModels")]
    public class TransactionModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime LastChanged { get; set; }

        public int UserId { get; set; }
        [Computed]
        public UserModel User { get; private set; }
        public int MaterialId { get; set; }
        [Computed]
        public MaterialModel Material { get; private set; }
        [Computed]
        public TareModel Tare { get; set; }
        public double TareWeight { get; set; }
        public double NetWeight { get; set; }
        public double GrossWeight { get; set; }
        public string Unit { get; set; }
        public bool IsExported { get; set; }

        public TransactionModel()
        {
        }

        public TransactionModel(UserModel user)
        {
            SetUser(user);
            SetMaterial(new MaterialModel());
            Tare = new TareModel();
        }

        public void SetMaterial(MaterialModel material)
        {
            MaterialId = material.Id;
            Material = material;
        }

        public void SetUser(UserModel user)
        {
            UserId = user.Id;
            User = user;
        }
    }
}
