﻿using System.Collections.Generic;

namespace DataManager.Models
{
    public interface IApiProductConfirmationResponse
    {
        List<ET_MESSAGE> ET_Messages { get; set; }
        string EV_HANDLING_UNIT { get; set; }
        string EV_IDOC_NO { get; set; }
    }
}