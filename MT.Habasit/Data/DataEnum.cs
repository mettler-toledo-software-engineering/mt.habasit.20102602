﻿namespace MT.Habasit.Data
{
    public enum SqlStatementTypes
    {
        SelectFromWhere,
        SelectBetween,
        DeleteFromWhere
    }

    public enum SqlResult
    {
        Failed
    }
}
