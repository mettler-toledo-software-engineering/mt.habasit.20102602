﻿//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.IO;
//using System.Linq;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;
//using System.Xml.Linq;
//using System.Xml.Serialization;
//using DataManager.Models;
//using MT.Singularity.Logging;
//using Newtonsoft.Json;

//namespace MT.Habasit.Data.API
//{
//    public class ApiHelper : IApiHelper
//    {
//        private HttpClient _apiClient;

//        public ApiHelper()
//        {
//            InitializeClient();
//        }

//        public HttpClient ApiClient
//        {
//            get
//            {
//                return _apiClient;
//            }
//        }

//        private void InitializeClient()
//        {
//            // referenz zu system.configuration in den references erstellen 
//            string api = ConfigurationManager.AppSettings["api"];
//            _apiClient = new HttpClient();
//            _apiClient.BaseAddress = new Uri(api);
//            _apiClient.DefaultRequestHeaders.Accept.Clear();
//            _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//        }

//        public async Task<IAuthenticatedUser> Authenticate(string username, string password)
//        {
//            var data = new FormUrlEncodedContent(new[]
//            {
//                new KeyValuePair<string, string>("grant_type","client_credentials"),
//                new KeyValuePair<string, string>("client_id", username),
//                new KeyValuePair<string, string>("client_secret",password),

//            });

//            // base url wird in app.config bereitgestellt. /token ist der api endpoint welchen wir von hand
//            //für authentifizierung erstellt haben (swagger config)
//            Log4NetManager.ApplicationLogger.Info("Requesting baerer token from api");
//            using (HttpResponseMessage response = await _apiClient.PostAsync("/v1/OAuthService/GenerateToken", data))
//            {
//                if (response.IsSuccessStatusCode)
//                {
//                    var resultstream = await response.Content.ReadAsStringAsync();
//                    var user = JsonConvert.DeserializeObject<AuthenticatedUser>(resultstream);

//                    Log4NetManager.ApplicationLogger.Info($"Got baerer token from api: {user.Access_Token}");
//                    return user;
//                }
//                else
//                {
//                    Log4NetManager.ApplicationLogger.Error($"Got NO bearer Token, reason: {response.ReasonPhrase}");
//                    throw new Exception($"{response.ReasonPhrase}");
//                }
//            }
//        }

//        public async Task<IApiProductConfirmationResponse> SendProductionData(string token)
//        {
//            _apiClient.DefaultRequestHeaders.Clear();
//            _apiClient.DefaultRequestHeaders.Accept.Clear();
//            _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
//            _apiClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
//            var data = CreateTestData();

//            var httpcontent = new StringContent(data.ToString(), Encoding.UTF8, "text/xml");
//            Log4NetManager.ApplicationLogger.Info("sending prodconf data to api....");
//            using (HttpResponseMessage response = await _apiClient.PostAsync("/v0/prodconf_AER120/send", httpcontent))
//            {
//                if (response.IsSuccessStatusCode)
//                {
//                    Log4NetManager.ApplicationLogger.Info("prod conf data received by api, waiting for response data");
//                    var resultstream = await response.Content.ReadAsStreamAsync();

//                    XDocument xml = XDocument.Load(resultstream);

//                    var responsemessages = new ApiProductConfirmationResponse();
//                    responsemessages.EV_IDOC_NO = xml.Descendants("EV_IDOC_NO").First().Value;
//                    responsemessages.EV_HANDLING_UNIT = xml.Descendants("EV_HANDLING_UNIT").First().Value;

//                    var items = xml.Descendants(XName.Get("item"));
//                    foreach (XElement xElement in items)
//                    {
//                        var message = new ET_MESSAGE();
//                        StringReader reader = new StringReader(xElement.ToString());
//                        XmlSerializer xmlSerializer2 = new XmlSerializer(typeof(ET_MESSAGE));
//                        message = (ET_MESSAGE)xmlSerializer2.Deserialize(reader);
//                        responsemessages.ET_Messages.Add(message);
//                    }
//                    Log4NetManager.ApplicationLogger.Info($"response messages received, reason: {response.ReasonPhrase}");
//                    return responsemessages;
//                }
//                else
//                {
//                    Log4NetManager.ApplicationLogger.Error($"Sending prod conf data failed!, reason: {response.ReasonPhrase}");
//                    throw new Exception(response.ReasonPhrase);
//                }
//            }
//        }

//        private ApiProdConfirmationData CreateTestData()
//        {
//            ApiProdConfirmationData data = new ApiProdConfirmationData
//            {
//                WERKS = "ES01",
//                LGNUM = "ES1",
//                TEAMID = "VALL70S3",
//                MATNR = "101408140101",
//                VERID = 0101,
//                POBJID = 23521,
//                MENGE = 36,
//                MEINH = "PC",
//                PRINTER = "LOCL"
//            };

//            return data;
//        }

//    }
//}
