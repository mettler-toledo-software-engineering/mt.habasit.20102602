﻿using System;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MT.Habasit.Data.Database
{
    public interface IDataBaseConnection<T>
    {
        Task<int> SaveDataAsync(T dataEntry);
        Task<bool> UpdateDataAsync(T dataEntry);
        Task<bool> DeleteDataAsync(T dataEntry);
        Task<List<T>> GetAllDataEntriesAsync();
        Task<T> GetEntryByIdAsync(int id);
        Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue);
        Task<bool> DeleteDataExported();
        Task<List<T>> GetDataBetweenTwoDatesAsync(DateTime start, DateTime end);
    }
}
