﻿using MT.Singularity.Platform;
using MT.Singularity.Platform.Devices.Scale;
using System.Configuration;
using System.IO;
using System.Reflection;
using MT.Singularity;

namespace MT.Habasit.Logic
{
    public static class Globals
    {
        public static int MaxMaterialsPerPage = 12;
        public static int MaxTaresPerPage = 12;

        private static IScale _scale;
        private static readonly SingularityEnvironment Environment = new SingularityEnvironment("mt.habasit");

        public static string SQLIteX86Driver = Path.Combine(Environment.BinaryDirectory, "x86");

        public static string BarcodeReaderName = "Cino FuzzyScan";
        public static int MessageShowInterval = 2000;
        public static int MessageShowIntervalShort = 500;
        public static string ProjectNumber = "P20102602";

        public static readonly string IND930USBDrive = "D:\\";
        public static string UsbBackupBaseDirectory = Path.Combine(IND930USBDrive, "Backup");
        public static string SystemUpdateSourceDirectory = Path.Combine(IND930USBDrive, "systemupdate");
        
        public static string GetDataDirectory()
        {
            return Environment.DataDirectory;
        }

        public static string ConnectionString()
        {
            //um den Configuration Manager nutzen zu können muss die Referenz System.Configuration hinzugefügt werden
            //add reference --> assemblies-- > system.configuration
            return ConfigurationManager.ConnectionStrings["HabasitSqlite"].ConnectionString;
        }

        public static string GetVersion()
        {
            return "V " + Assembly.GetExecutingAssembly().GetName().Version.Major +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Minor +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Build +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Revision;
        }

        public static void SetCurrentScale(IScale scale)
        {
            _scale = scale;
        }

        public static IScale GetCurrentScale()
        {
            return _scale;
        }

        public static string GetCurrentScaleUnit()
        {
            return _scale.CurrentDisplayUnit.ToAbbreviation();
        }
    }
}
