﻿using MT.Habasit.View;
using MT.Singularity.Presentation.Controls;

using Timer = System.Timers.Timer;

namespace MT.Habasit.Logic
{
    public static class Message
    {
        private static InfoBox _info;
        private static Timer _timer = null;

        public static void ShowErrorMessage(Visual parent, string title, string message)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK);
            _info.Show(parent);
        }

        public static void ShowInfoMessage(Visual parent, string title, string message)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK);
            _info.Show(parent);
        }

        public static void ShowInfoMessage(Visual parent, string title, string message, int seconds)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Information);
            _info.Show(parent);
            
            KeepBoxOpen(seconds);
        }

        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Elapsed -= Timer_Elapsed;
            _info.Close();
        }

        public static void ShowErrorMessage(Visual parent, string title, string message, int seconds)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Error);
            _info.Show(parent);

            KeepBoxOpen(seconds);
        }

        private static void KeepBoxOpen(int seconds)
        {
            _timer = new Timer(seconds);
            _timer.Elapsed += Timer_Elapsed;
            _timer.AutoReset = false;
            _timer.Start();
        }
    }
}
