﻿using MT.Habasit.Data.Database;
using MT.Habasit.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransactionModel = MT.Habasit.Models.TransactionModel;

namespace MT.Habasit.Logic
{
    public static class DatabaseExtension
    {
        private static IDataBaseConnection<TransactionModel> _transactionModelBaseConnection = new SqliteGenericDataBaseAccess<TransactionModel>();
        private static IDataBaseConnection<MaterialModel> _materialModelBaseConnection = new SqliteGenericDataBaseAccess<MaterialModel>();
        private static IDataBaseConnection<UserModel> _userModelBaseConnection = new SqliteGenericDataBaseAccess<UserModel>();
        private static IDataBaseConnection<TareModel> _tareModelBaseConnection = new SqliteGenericDataBaseAccess<TareModel>();

        private static ProcessFlow _currentProcess = ProcessFlow.Current;

        private static DateTime CurrentDateTime()
        {
            return DateTime.Now;
        }

        public static async Task LoadAllEntries()
        {
            await GetAllUsersFromDBAsync();
            await GetAllMaterialsFromDBAsync();
            await GetAllTaresFromDBAsync();
            await GetAllTransactionsFromDBAsync();
        }

        #region User

        public static async Task GetAllUsersFromDBAsync()
        {
            List<UserModel> _userModels = await _userModelBaseConnection.GetAllDataEntriesAsync();

            _currentProcess.UserList = _userModels;
        }

        private static bool IsUsedUser(int id)
        {
            TransactionModel transaction = _currentProcess.TransactionList.Find(model => model.UserId == id);

            return transaction != null;
        }

        public static async Task<bool> DeleteUserFromDBAsync(UserModel user)
        {
            if (!IsUsedUser(user.Id))
            {
                bool result = await _userModelBaseConnection.DeleteDataAsync(user);
                await GetAllUsersFromDBAsync();

                return result;
            }

            return false;
        }

        public static async Task<int> SaveUserInDBAsync(UserModel user)
        {
            user.TimeStamp = CurrentDateTime();
            user.LastChanged = CurrentDateTime();

            int result = await _userModelBaseConnection.SaveDataAsync(user);
            await GetAllUsersFromDBAsync();

            return result;
        }

        public static async Task<bool> UpdateUserInDBAsync(UserModel user)
        {
            user.LastChanged = CurrentDateTime();

            bool result = await _userModelBaseConnection.UpdateDataAsync(user);
            await GetAllUsersFromDBAsync();

            return result;
        }

        #endregion

        #region Material

        public static async Task GetAllMaterialsFromDBAsync()
        {
            List<MaterialModel> _materialModels = await _materialModelBaseConnection.GetAllDataEntriesAsync();

            _currentProcess.MaterialList = _materialModels;
        }

        private static bool IsUsedMaterial(int id)
        {
            TransactionModel transaction = _currentProcess.TransactionList.Find(model => model.MaterialId == id);

            return transaction != null;
        }

        public static async Task<bool> DeleteMaterialFromDBAsync(MaterialModel material)
        {
            if (!IsUsedMaterial(material.Id))
            {
                bool result = await _materialModelBaseConnection.DeleteDataAsync(material);
                await GetAllMaterialsFromDBAsync();

                return result;
            }

            return false;
        }
        
        public static async Task<int> SaveMaterialInDBAsync(MaterialModel material)
        {
            material.TimeStamp = CurrentDateTime();
            material.LastChanged = CurrentDateTime();

            int result = await _materialModelBaseConnection.SaveDataAsync(material);
            await GetAllMaterialsFromDBAsync();

            return result;
        }

        public static async Task<bool> UpdateMaterialInDBAsync(MaterialModel material)
        {
            material.LastChanged = CurrentDateTime();

            bool result = await _materialModelBaseConnection.UpdateDataAsync(material);
            await GetAllMaterialsFromDBAsync();

            return result;
        }
        #endregion

        #region Tare

        public static async Task GetAllTaresFromDBAsync()
        {
            List<TareModel> _tareModels = await _tareModelBaseConnection.GetAllDataEntriesAsync();

            _currentProcess.TareList = _tareModels;
        }

        public static async Task<bool> DeleteTareFromDBAsync(TareModel tare)
        {
            bool result = await _tareModelBaseConnection.DeleteDataAsync(tare);
            await GetAllTaresFromDBAsync();

            return result;
        }

        public static async Task<int> SaveTareInDBAsync(TareModel tare)
        {
            tare.TimeStamp = CurrentDateTime();
            tare.LastChanged = CurrentDateTime();

            int result = await _tareModelBaseConnection.SaveDataAsync(tare);
            await GetAllTaresFromDBAsync();

            return result;
        }

        public static async Task<bool> UpdateTareInDBAsync(TareModel tare)
        {
            tare.LastChanged = CurrentDateTime();

            bool result = await _tareModelBaseConnection.UpdateDataAsync(tare);
            await GetAllTaresFromDBAsync();

            return result;
        }
        #endregion

        #region Transaction

        public static async Task GetAllTransactionsFromDBAsync()
        {
            List<TransactionModel> _transactionModels = await _transactionModelBaseConnection.GetAllDataEntriesAsync();

            if (_transactionModels == null || _transactionModels.Count == 0)
            {
                return;
            }

            foreach (TransactionModel transaction in _transactionModels)
            {
                if (transaction.Material == null)
                {
                    MaterialModel material = _currentProcess.MaterialList.Find(model => model.Id == transaction.MaterialId);

                    if (material != null)
                    {
                        transaction.SetMaterial(material);
                    }
                }

                if (transaction.User == null)
                {
                    UserModel user = _currentProcess.UserList.Find(model => model.Id == transaction.UserId);

                    if (user != null)
                    {
                        transaction.SetUser(user);
                    }
                }
            }

            _currentProcess.TransactionList = _transactionModels;
        }

        public static async Task<bool> DeleteTransactionExported()
        {
            bool result = await _transactionModelBaseConnection.DeleteDataExported();
            await GetAllTransactionsFromDBAsync();

            return result;
        }

        public static async Task<int> SaveTransactionInDBAsync(TransactionModel transaction)
        {
            transaction.TimeStamp = CurrentDateTime();
            transaction.LastChanged = CurrentDateTime();
            transaction.IsExported = false;

            int result = await _transactionModelBaseConnection.SaveDataAsync(transaction);
            await GetAllTransactionsFromDBAsync();

            return result;
        }

        public static async Task<bool> UpdateTransactionInDBAsync(TransactionModel transaction)
        {
            transaction.LastChanged = CurrentDateTime();

            bool result = await _transactionModelBaseConnection.UpdateDataAsync(transaction);
            await GetAllTransactionsFromDBAsync();

            return result;
        }

        #endregion
    }
}
