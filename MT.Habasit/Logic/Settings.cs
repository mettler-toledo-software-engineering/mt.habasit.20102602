﻿using System;
using System.IO;
using System.Xml.Serialization;
using log4net;
using MT.Singularity.Logging;

#if IndPro
using MT.Singularity.Platform;
#endif

namespace MT.Habasit.Logic
{
    [Serializable]
    public class Settings
    {
#if IndPro
        public static string FileName =
            Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "Settings.xml");

        public static string FileName2 = @"C:\Temp\SettingsCopy.xml";
#else
        public static string FileName = Path.Combine(".\\Settings", "Settings.xml");
#endif
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Settings);
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Settings));
        private static Settings _current;
        private static string _currentFilename = "";

        #region Load

        private static Settings Load()
        {
            if (!File.Exists(FileName))
            {
                _log.InfoEx("Unable to access original file", _sourceClass);
                _currentFilename = File.Exists(FileName2) ? FileName2 : "";
            }
            else
                _currentFilename = FileName;

            if (_currentFilename != "")
            {
                try
                {
                    _log.InfoEx($"Reading settings: {_currentFilename}", _sourceClass);
                    using (FileStream fs = new FileStream(_currentFilename, FileMode.Open, FileAccess.Read,
                               FileShare.Read))
                    {
                        return (Settings)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    _log.InfoEx("Unable reading settings, new", _sourceClass);
                    return new Settings();
                }
            }
            else
            {
                _log.InfoEx("Unable to access copy file, creating new", _sourceClass);
                return new Settings();
            }
        }



        public static Settings Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public string FileLocation { get; set; }
        public int Interval { get; set; }
        public bool AutoCreation { get; set; }
        public string Index { get; set; }
        public string Delimiter { get; set; }
        public bool FileWritten { get; set; }
        public DateTime FileWrittenDate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            FileLocation = @"I:\data\reibri\Logistik\Abfallmengen";
            Interval = 10000; // hard coded, nicht änderbar
            AutoCreation = true;
            Index = "A";
            Delimiter = ";";
            FileWritten = false;
            FileWrittenDate = DateTime.Now;
        }

        #region Save
        public void Save()
        {
            if (FileName == null) return;
            if (!Directory.Exists(Path.GetDirectoryName(FileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));
            if (!Directory.Exists(Path.GetDirectoryName(FileName2)))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName2));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                _log.InfoEx("Write settings", _sourceClass);
                Serializer.Serialize(fs, this);
            }

            using (var fs = new FileStream(FileName2, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                _log.InfoEx("Write settings copy", _sourceClass);
                Serializer.Serialize(fs, this);
            }
        }
        #endregion
    }
}