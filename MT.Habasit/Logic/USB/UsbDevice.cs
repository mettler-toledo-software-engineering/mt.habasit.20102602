﻿using System;
using System.Management;
using log4net;
using MT.Singularity.Logging;

namespace MT.Habasit.Logic.USB
{
    public class USBDevice
    {
        private string _name;
        private const string _sourceClass = nameof(USBDevice);
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;

        public USBDevice(string name)
        {
            _name = name;
        }

        public string GetPortName()
        {
            if (_name.ToLowerInvariant().StartsWith("com"))
            {
                return _name;
            }

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity");

            foreach (var o in searcher.Get())
            {
                var queryObj = (ManagementObject)o;
                string devicename = (string)queryObj["Name"];


                if (string.IsNullOrEmpty(devicename) == false && devicename.Contains(_name))
                {
                    //bsp: "Zebra CDC Scanner (COM5)"
                    int portstart = devicename.IndexOf("(", StringComparison.Ordinal) + 1;
                    int portWordLength = devicename.Length - portstart - 1;

                    var portname = devicename.Substring(portstart, portWordLength);

                    return portname;
                }
            }

            _log.ErrorEx($"No Device with name {_name} found ", _sourceClass);

            return "";
        }
    }
}
