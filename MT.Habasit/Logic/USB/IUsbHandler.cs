﻿using System;

namespace MT.Habasit.Logic.USB
{
    public interface IUsbHandler
    {
        bool UsbDriveAvailable { get; }

        event EventHandler DeviceInsertedEvent;
        event EventHandler DeviceRemovedEvent;
    }
}
