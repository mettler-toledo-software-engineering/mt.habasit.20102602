﻿using MT.Habasit.Models;
using System.Collections.Generic;

namespace MT.Habasit.Logic
{
    public class ProcessFlow
    {
        #region Properties

        public TransactionModel Transaction { get; private set; }

        public List<TransactionModel> TransactionList { get; set; }
        public List<TareModel> TareList { get; set; }
        public List<MaterialModel> MaterialList { get; set; }
        public List<UserModel> UserList { get; set; }

        #endregion

        #region Initialization

        private static ProcessFlow _current;
        private static ProcessFlow Load()
        {
            return new ProcessFlow();
        }

        public static ProcessFlow Current
        {
            get
            {
                if (_current == null)
                {
                    _current = Load();
                }

                return _current;
            }
        }

        #endregion
        
        public ProcessFlow()
        {
            Transaction = new TransactionModel();

            TransactionList = new List<TransactionModel>();
            TareList = new List<TareModel>();
            MaterialList = new List<MaterialModel>();
            UserList = new List<UserModel>();
        }
        
        #region User
        
        public void Login(UserModel user)
        {
            Transaction.SetUser(user);
        }

        public void Logout()
        {
            Transaction = new TransactionModel();
        }

        public bool HasLoggedUser()
        {
            return Transaction.User != null || Transaction.User == new UserModel();
        }

        #endregion

        #region Material Selection

        public void SelectMaterial(MaterialModel material)
        {
            Transaction.SetMaterial(material);
        }
         
        #endregion

        #region Tare Selection

        public void SelectTare(TareModel tare)
        {
            Transaction.TareWeight = tare.Value;
        }

        #endregion
    }
}
