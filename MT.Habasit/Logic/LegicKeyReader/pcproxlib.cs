﻿using System;
using System.Runtime.InteropServices;

namespace MT.Habasit.Logic.LegicKeyReader
{
    public class pcproxlib
    {
        [DllImport("pcProxAPI.dll")]
        public static extern ushort usbConnect();
        [DllImport("pcProxAPI.dll")]
        public static extern ushort usbDisconnect();
        [DllImport("pcProxAPI.dll")]
        public static extern IntPtr getPartNumberString();
        [DllImport("pcProxAPI.dll")]
        public static extern int GetLUID();
        [DllImport("pcProxAPI.dll")]
        public static extern short GetDevCnt();
        [DllImport("pcProxAPI.dll")]
        public static extern IntPtr GetVidPidVendorName();
        [DllImport("pcProxAPI.dll")]
        public static extern ushort SetActDev(short iNdx);
        [DllImport("pcProxAPI.dll")]
        public static extern ushort GetLibVersion(ref short major, ref short minor, ref short ver);
        [DllImport("pcProxAPI.dll")]
        public static extern ushort GetActiveID32(IntPtr result1, short buffSize);
        [DllImport("pcProxAPI.dll")]
        public static extern ushort SetDevTypeSrch(short iSrchType);
    }
}
