﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;
using log4net;
using MT.Singularity.Logging;
using System.Threading;
using System.Threading.Tasks;
using MT.Singularity.Platform;
using MT.Singularity.Platform.Infrastructure;
using Timer = System.Timers.Timer;

namespace MT.Habasit.Logic.LegicKeyReader
{
    public class LegicKeyReader : IDisposable
    {
        public const short PRXDEVTYP_USB = 0;
        private static int Interval = 300;
        private static int SleepInterval = 250;

        private static short MaxLength = 32;
        private static int MaxBytes = 8;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(LegicKeyReader);

        private string _oldCardData = "";
        private bool _dataReceived;
        private Thread _legicReader;
        private ManualResetEvent _running;
       
        private bool quit = false;
        private object _parent;
        
        private LegicKeyReader()
        {
            
        }

        public static async Task<LegicKeyReader> CreateAsync()
        {
           var newInstance = new LegicKeyReader();
            newInstance.Start();
            return newInstance;
        }

        public void Start()
        {
           _legicReader = new Thread(Worker);
            _legicReader.Name = "LegicKeyReader";
            _legicReader.IsBackground = false;
            _running = new ManualResetEvent(false);
            
            _legicReader.Start();
            
        }

        public void Dispose()
        {
            quit = true;
        }

        private void Worker()
        {

            while (!_running.WaitOne(Interval, false) && quit != true)
            {
               // _log.InfoEx("check connection...", _sourceClass);
                pcproxlib.SetDevTypeSrch(PRXDEVTYP_USB);
                int rc = pcproxlib.usbConnect();
                Thread.Sleep(SleepInterval);
                if (rc == 1)
                {
                    string cardData = GetCardData();

                    cardData = cardData?.TrimStart('0');
                  //  _log.InfoEx($"Card ID Found with data '{cardData}'", _sourceClass);

                    OldCardData = cardData;
                }

            }
        }

        private bool HasValidContent(string content)
        {
            if (!String.IsNullOrEmpty(_oldCardData) && content == null)
            {
                _dataReceived = false;
            }

            string extractedContent = content == null ? "" : content;
            extractedContent = extractedContent.Replace('0', ' ');

            return !String.IsNullOrEmpty(extractedContent) && !String.IsNullOrWhiteSpace(extractedContent);
        }
        
        private string GetCardData()
        {
            string cardData = "";
            IntPtr result1 = Marshal.AllocHGlobal(MaxLength * sizeof(int));
            byte[] arr = new byte[MaxLength];
            int nBits = pcproxlib.GetActiveID32(result1, MaxLength);

            if (nBits == 0)
            {
                cardData = null;
            }
            else
            {
                int Bytes = (nBits + (MaxBytes - 1)) / MaxBytes;
                if (Bytes < MaxBytes)
                {
                    Bytes = MaxBytes;
                }

                Marshal.Copy(result1, arr, 0, MaxLength);
                
                for (int i = 0; i < Bytes; i++)
                {
                    string data = String.Format("{0:X2}", arr[i]);
                    cardData = data + cardData;
                }
            }

            Marshal.FreeHGlobal(result1);

            return cardData;
        }

        private string OldCardData
        {
            get { return _oldCardData; }
            set
            {
               // _log.InfoEx($"value = {value}, _oldCardData={_oldCardData}", _sourceClass);

                if (value != _oldCardData)
                {
                    _oldCardData = value;
                    _log.InfoEx($"_oldCardData={_oldCardData}", _sourceClass);

                    if (HasValidContent(_oldCardData) && !_dataReceived)
                    {
                        OnRfidReceived(new DataReceivedEventArgs(_oldCardData));
                        _dataReceived = true;
                        _log.InfoEx($"_dataReceived={_dataReceived}", _sourceClass);
                    }
                }
                else
                {
                    _dataReceived = false;
                   // _log.InfoEx($"_dataReceived={_dataReceived}", _sourceClass);
                }
            }
        }



        #region Event RfidReceived

        public event EventHandler<DataReceivedEventArgs> RfidReceived;

        protected virtual void OnRfidReceived(DataReceivedEventArgs e)
        {
            if (RfidReceived != null)
            {
                RfidReceived(this, e);
            }
        }

        #endregion
    }
}