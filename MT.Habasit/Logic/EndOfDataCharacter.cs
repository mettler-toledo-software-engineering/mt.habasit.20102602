﻿namespace MT.Habasit.Logic
{
    public enum EndOfDataCharacter
    {
        CR,
        LF,
        CRLF,
        ZPLCRLF
    }
}
