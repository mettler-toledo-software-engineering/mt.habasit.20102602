﻿using System;

namespace MT.Habasit.Logic
{
    public class DataReceivedEventArgs : EventArgs
    {
        public string Data { get; set; }

        public DataReceivedEventArgs(string data)
        {
            Data = data;
        }
    }
}