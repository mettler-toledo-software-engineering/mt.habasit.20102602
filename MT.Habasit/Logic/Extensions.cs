﻿using CsvHelper;
using log4net;
using MT.Habasit.Models;
using MT.Singularity.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Spire.Xls;

namespace MT.Habasit.Logic
{
    public static class Extensions
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Extensions);

        private static Settings _currentSettings = Settings.Current;
        private static string localExportPath = @"C:\Temp\HabaExport";
        
        public static bool WriteTransactionsToFileSystemAsCsv(List<TransactionModel> transactions)
        {
            int digits = 5;

            string index = _currentSettings.Index;
            string delimiter = _currentSettings.Delimiter;
            string finalPath = Settings.Current.FileLocation;
            string fileName = $"IND930{index}{DateTime.Now:yyMMdd_HHmmss}.txt";
            string localDestination = Path.Combine(localExportPath, fileName);
            string finalDestination = Path.Combine(finalPath, fileName);
            if (!Directory.Exists(localExportPath))
                Directory.CreateDirectory(localExportPath);

            using (var writer = new StreamWriter(localDestination))
            using (var csv = new CsvWriter(writer))
            {
                int number = 1;
                csv.Configuration.Delimiter = delimiter;
               
                try
                {
                    csv.WriteHeader(typeof(ExportModel));
                    csv.NextRecord();

                    foreach (TransactionModel transaction in transactions)
                    {
                        ExportModel model = new ExportModel
                        {
                            Index = index,
                            Number = number,
                            ACode = transaction.Material.Code,
                            UserKey = transaction.User.Key,
                            NetWeight = Math.Round(transaction.NetWeight, digits),
                            GrossWeight = Math.Round(transaction.GrossWeight, digits),
                            TareWeight = Math.Round(transaction.TareWeight, digits),
                            Date = $"{transaction.TimeStamp:dd.MM.yyyy}",
                            Time = $"{transaction.TimeStamp:HH.mm:ss}"
                        };

                        csv.WriteRecord(model);
                        csv.NextRecord();
                    }
                    _log.Success($"Transactions successfully local exported", _sourceClass);

                    //if (Directory.Exists(finalPath))
                    //    File.Copy(localDestination, finalDestination, true);
                    //_log.Success($"Transactions successfully copied to domain", _sourceClass);
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"Transactions couldn't exported", _sourceClass, ex);

                    return false;
                }
                _log.Success($"Transactions successfully exported", _sourceClass);
                return true;
            }
        }

        public static bool WriteTransactionsToFileSystemAsXlsx(List<TransactionModel> transactions)
        {
            int row = 2;
            string index = _currentSettings.Index;
            string fileName = $"IND930{index}{DateTime.Now:yyMMdd_HHmmss}.xlsx";
            string destination = Path.Combine(localExportPath, fileName);
            if (!Directory.Exists(localExportPath))
                Directory.CreateDirectory(localExportPath);

            using (var writer = new FileStream(destination, FileMode.Create))
            {
                Workbook wbToStream = new Workbook();
                wbToStream.Version = ExcelVersion.Version2016;
                wbToStream.Worksheets.Clear();
                Worksheet sheet = wbToStream.CreateEmptySheet(DateTime.Today.AddMonths(-1).ToString("MMMM yyyy"));

                sheet.InsertArray(new string[] { "Waage", "Nr.", "Code", "ZUKO-Nr.", "Netto", "Brutto", "Tara", "Datum", "Zeit", "Name" }, 1, 1, false);

                foreach (TransactionModel transaction in transactions)
                {
                    sheet.InsertRow(row);
                    sheet.Range[row, 1].Text = index;
                    sheet.Range[row, 2].Value = transaction.Id.ToString();
                    sheet.Range[row, 3].Value = transaction.Material.Code;
                    sheet.Range[row, 4].Text = transaction.User.Key;
                    sheet.Range[row, 5].Value = Math.Round(transaction.NetWeight, 1).ToString("F1");
                    sheet.Range[row, 6].Value = Math.Round(transaction.GrossWeight, 1).ToString("F1");
                    sheet.Range[row, 7].Value = Math.Round(transaction.TareWeight, 1).ToString("F1");
                    sheet.Range[row, 8].Text = $"{transaction.TimeStamp:dd.MM.yyyy}";
                    sheet.Range[row, 9].Text = $"{transaction.TimeStamp:HH.mm:ss}";
                    sheet.Range[row, 10].Text = transaction.User.Lastname;
                    row++;
                }
                sheet.AllocatedRange.Style.Font.FontName = "Calibri";
                sheet.AllocatedRange.Style.Font.Size = 11;
                sheet.Rows[0].Style.Font.Size = 15;
                sheet.AllocatedRange.AutoFitColumns();

                sheet.ListObjects.Create("Table", sheet.Range[1, 1, sheet.LastRow, sheet.LastColumn])
                    .BuiltInTableStyle = TableBuiltInStyles.TableStyleDark10;

                CellRange dataRange = sheet.AllocatedRange;
                PivotCache cache = wbToStream.PivotCaches.Add(dataRange);
                PivotTable pt = sheet.PivotTables.Add("Pivot Table", sheet.Range["L3"], cache);

                var r1 = pt.PivotFields["Code"];
                r1.Axis = AxisTypes.Row;
                pt.Options.RowHeaderCaption = "Code";
                pt.DataFields.Add(pt.PivotFields["Netto"], "Summe von Netto", SubtotalTypes.Sum);

                pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleMedium1;
                pt.CalculateData();

                sheet.AllocatedRange.AutoFitColumns();
                sheet.AllocatedRange.AutoFitRows();

                wbToStream.SaveToStream(writer);
                writer.Close();

                return true;
            }
        }
    }
}
