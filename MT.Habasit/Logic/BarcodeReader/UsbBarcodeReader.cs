﻿using MT.Singularity.Composition;
using System;

using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace MT.Habasit.Logic.BarcodeReader
{
    [Export(typeof(IBarcodeReader))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbBarcodeReader : BarcodeReader
    {
        
        public UsbBarcodeReader(SerialPort connection, EndOfDataCharacter character) : base(connection, character)
        {

        }
    }
}
