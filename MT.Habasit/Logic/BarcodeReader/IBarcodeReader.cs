﻿using System;
using System.IO.Ports;

namespace MT.Habasit.Logic.BarcodeReader
{
    public interface IBarcodeReader : IDisposable
    {
        event EventHandler<DataReceivedEventArgs> DataReceivedEvent;
        bool Initialized { get; }
        bool InitBarcodeReader(SerialPort connection);
    }
}
