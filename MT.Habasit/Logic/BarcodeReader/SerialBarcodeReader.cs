﻿using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Serialization;
using System;
using System.IO.Ports;
using SerialPort = System.IO.Ports.SerialPort;

namespace MT.Habasit.Logic.BarcodeReader
{

    public class SerialBarcodeReader : BarcodeReader
    {
        public SerialBarcodeReader(SerialPort connection, EndOfDataCharacter character) : base(connection, character)
        {
        }
    }
}
