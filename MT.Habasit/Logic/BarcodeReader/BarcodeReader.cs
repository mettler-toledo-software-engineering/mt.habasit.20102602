﻿using System;
using System.IO.Ports;
using log4net;
using MT.Singularity.Logging;

namespace MT.Habasit.Logic.BarcodeReader
{
    public abstract class BarcodeReader : IBarcodeReader
    {
        public event EventHandler<DataReceivedEventArgs> DataReceivedEvent;
        public bool Initialized { get; private set; }

        protected SerialPort _usedSerialPort;
        private string _dataline;
        private string _endOfDataCharakter;
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private const string _sourceClass = nameof(BarcodeReader);

        protected BarcodeReader(SerialPort connection, EndOfDataCharacter character)
        {
            _endOfDataCharakter = character.TranslateCharakterToString();
            Initialized = InitBarcodeReader(connection);
        }

        public bool InitBarcodeReader(SerialPort connection)
        {
            try
            {
                _usedSerialPort = connection;
                //_usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;

                if (_usedSerialPort != null)
                {
                    if (_usedSerialPort.IsOpen)
                    {
                        _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
                        _usedSerialPort.Close();
                    }
                    _usedSerialPort.Open();
                    _usedSerialPort.DataReceived += UsedSerialPortOnDataReceived;

                    return true;
                }

                _log.ErrorEx($"Barcode Reader coud not be initialized ", _sourceClass);

                return false;
            }
            catch(Exception ex)
            {
                _log.ErrorEx($"{ex.Message}", _sourceClass);
                return false;
            }
        }

        private void UsedSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _usedSerialPort.ReadExisting();
            _dataline += data;

            if (_dataline.EndsWith(_endOfDataCharakter))
            {
                _dataline = _dataline.Replace(_endOfDataCharakter, "");
                DataReceivedEvent?.Invoke(null, new DataReceivedEventArgs(_dataline));
                _dataline = "";
            }

        }

        public void Dispose()
        {
            if (_usedSerialPort != null)
            {
                _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
                _usedSerialPort.Close();
            }

            Initialized = false;
            _dataline = "";
        }

    }
}
