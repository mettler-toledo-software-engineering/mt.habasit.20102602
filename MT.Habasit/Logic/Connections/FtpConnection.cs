﻿using log4net;
using MT.Singularity.Logging;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace MT.Habasit.Logic.Connections
{
    public class FtpConnection
    {
        private string _server;
        private string _userName;
        private string _password;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(FtpConnection);

        public FtpConnection(string server, string userName, string password)
        {
            _server = server;
            _userName = userName;
            _password = password;
        }

        public void UploadFile(string file)
        {
            FtpWebRequest request = (FtpWebRequest) WebRequest.Create(_server);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            request.Credentials = new NetworkCredential(_userName, _password);
            
            byte[] fileContents;

            try
            {
                StreamReader sourceStream = new StreamReader(file);
                fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);

                FtpWebResponse response = (FtpWebResponse) request.GetResponse();

                _log.InfoEx($"upload file {file} to server {_server}", _sourceClass);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"upload file {file} to server {_server} failed", _sourceClass, ex);
            }
        }

        //public void UploadFile(string file)
        //{
        //    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(_server);
        //    request.Method = WebRequestMethods.Ftp.UploadFile;

        //    request.Credentials = new NetworkCredential(_userName, _password);

        //    byte[] fileContents;
        //    using (StreamReader sourceStream = new StreamReader(file))
        //    {
        //        fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
        //    }

        //    request.ContentLength = fileContents.Length;

        //    using (Stream requestStream = request.GetRequestStream())
        //    {
        //        requestStream.Write(fileContents, 0, fileContents.Length);
        //    }

        //    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
        //    {

        //    }
        //}
    }
}
