﻿using System.IO.Ports;

namespace MT.Habasit.Logic.Connections
{
    public interface IDeviceConnection
    {
        SerialPort GetDeviceConnection();
    }
}
