﻿using MT.Habasit.Logic.USB;
using System.IO.Ports;
using System.Linq;

namespace MT.Habasit.Logic.Connections
{
    public class UsbDeviceConnection : SerialDeviceConnection
    {
        private string _deviceName;

        public UsbDeviceConnection(string deviceName)
        {
            _deviceName = deviceName;
        }

        public override SerialPort GetDeviceConnection()
        {
            var device = new USBDevice(_deviceName);
            string portname = device.GetPortName();

            var availablePorts = SerialPort.GetPortNames();


            if (portname != "" && availablePorts.Any(availableport => availableport == portname))
            {
                SerialPort.PortName = portname;

                return SerialPort;
            }

            return null;
        }
    }
}
