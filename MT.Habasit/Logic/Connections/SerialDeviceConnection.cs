﻿using System.IO.Ports;

namespace MT.Habasit.Logic.Connections
{
    public class SerialDeviceConnection : IDeviceConnection
    {
        protected SerialPort SerialPort { get; } = new SerialPort();

        public SerialDeviceConnection(string port)
        {
            SerialPort.BaudRate = 9600;
            SerialPort.Parity = Parity.None;
            SerialPort.DataBits = 8;
            SerialPort.StopBits = StopBits.One;
            SerialPort.Handshake = Handshake.None;
            SerialPort.PortName = port;
        }

        public SerialDeviceConnection(int baudrate, Parity parity, int databits, StopBits stopBits, Handshake handshake, string port)
        {
            SerialPort.BaudRate = baudrate;
            SerialPort.Parity = parity;
            SerialPort.DataBits = databits;
            SerialPort.StopBits = stopBits;
            SerialPort.Handshake = handshake;
            SerialPort.PortName = port;
        }

        protected SerialDeviceConnection()
        {
            SerialPort.BaudRate = 9600;
            SerialPort.Parity = Parity.None;
            SerialPort.DataBits = 8;
            SerialPort.StopBits = StopBits.One;
            SerialPort.Handshake = Handshake.None;
        }

        protected SerialDeviceConnection(int baudrate, Parity parity, int databits, StopBits stopBits, Handshake handshake)
        {
            SerialPort.BaudRate = baudrate;
            SerialPort.Parity = parity;
            SerialPort.DataBits = databits;
            SerialPort.StopBits = stopBits;
            SerialPort.Handshake = handshake;
        }

        public virtual SerialPort GetDeviceConnection()
        {
            return SerialPort;
        }

    }
}
