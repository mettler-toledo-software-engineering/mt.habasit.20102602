﻿using System;
using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.Habasit.ViewModels
{
    public class TareButtonControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;

        public TareButtonControlViewModel(Visual parent, AnimatedContentControl homeNavigationFrame, TareModel tare)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Tare = tare;
        }

        #region Tare
        private TareModel _tare;

        public TareModel Tare
        {
            get { return _tare; }
            set
            {
                if (value != _tare)
                {
                    _tare = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
        
        #region Select

        public ICommand GoSelect
        {
            get { return new DelegateCommand(DoGoSelect); }
        }

        private void DoGoSelect()
        {
            OnTareButtonClicked(new TareButtonClickedEventArgs(Tare));
        }

        #endregion

        #region Event TareButtonClicked

        public event EventHandler<TareButtonClickedEventArgs> TareButtonClicked;

        protected virtual void OnTareButtonClicked(TareButtonClickedEventArgs e)
        {
            if (TareButtonClicked != null)
            {
                TareButtonClicked(this, e);
            }
        }

        public class TareButtonClickedEventArgs : EventArgs
        {
            public TareModel Tare { get; set; }

            public TareButtonClickedEventArgs(TareModel tare)
            {
                Tare = tare;
            }
        }

        #endregion
    }
}
