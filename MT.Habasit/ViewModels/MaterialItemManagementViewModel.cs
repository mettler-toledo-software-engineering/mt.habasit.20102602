﻿using System;
using MT.Habasit.Models;
using MT.Habasit.Views;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

using System.Collections.Generic;
using System.Linq;
using MT.Habasit.View.Controlls;
using MT.Habasit.Logic;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.ViewModels
{
    public class MaterialItemManagementViewModel : ItemManagementViewModel
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private ProcessFlow _currentProcess = ProcessFlow.Current;
        private Settings _currentSettings = Settings.Current;

        private MaterialManagement _materialManagement;
        private List<MaterialModel> _materialList;

        public MaterialItemManagementViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            
            LoadItems();
        }

        #region Item Overview
        public override void LoadItems()
        {
            ItemList = new List<object>();
            _materialList = _currentProcess.MaterialList.Where(d => d.TimeStamp > DateTime.Parse("1.7.2022")).ToList();

            if (_materialList == null)
            {
                return;
            } 

            foreach (MaterialModel material in _materialList)  
            {
                MaterialModelControl modelControl = new MaterialModelControl(_homeNavigationFrame, material);
                modelControl.ViewModel.MaterialItemChanged += ViewModel_MaterialItemChanged;
                ItemList.Add(modelControl);
            }

            UpdateTitle();
        }

        private void ViewModel_MaterialItemChanged(object sender, MaterialModelControlViewModel.MaterialItemChangedEventArgs e)
        {
            LoadItems();
        }

        public override void UpdateTitle()
        {
            Title = $"{Localization.Get(Localization.Key.Materiallist)} ({_materialList.Count})";
        }

        #endregion

        #region Properties

        private string _title;

        public override string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        
        private List<object> _itemList;

        public override List<object> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Key Commands

        public override ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }
        
        public override ICommand GoNew
        {
            get { return new DelegateCommand(DoGoNew); }
        }

        private void DoGoNew()
        {
            _materialManagement = new MaterialManagement(_homeNavigationFrame, new MaterialModel(), true);

            if (_materialManagement != null)
            {
                _materialManagement.Closed += _materialManagement_Closed;
                _materialManagement.Show(_parent);
            }
        }

        private void _materialManagement_Closed(object sender, System.EventArgs e)
        {
            LoadItems();
        }

        #endregion
    }
}
