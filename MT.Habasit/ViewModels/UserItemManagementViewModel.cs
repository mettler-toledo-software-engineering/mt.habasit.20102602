﻿using MT.Habasit.Logic;
using MT.Habasit.Models;
using MT.Habasit.Views;
using MT.Habasit.Views.Controlls;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;

using System.Collections.Generic;
using System.Linq;

namespace MT.Habasit.ViewModels
{
    public class UserItemManagementViewModel : ItemManagementViewModel
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private ProcessFlow _currentProcess = ProcessFlow.Current;   
        private List<UserModel> _userList;

        public UserItemManagementViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            LoadItems();
        }

        #region Item Overview
        
        public override void LoadItems()
        {
            ItemList = new List<object>();
            _userList = _currentProcess.UserList;

            foreach (UserModel user in _userList.OrderBy(d=> d.Lastname))
            {
                UserModelControl modelControl = new UserModelControl(_homeNavigationFrame, user);
                modelControl.ViewModel.UserItemChanged += ViewModel_UserItemChanged;
                ItemList.Add(modelControl);
            }

            UpdateTitle();
        }

        private void ViewModel_UserItemChanged(object sender, UserModelControlViewModel.UserItemChangedEventArgs e)
        {
            LoadItems();
        }

        public override void UpdateTitle()
        {
            Title = $"{Localization.Get(Localization.Key.Userlist)} ({ _userList.Count})";
        }

        #endregion

        #region Properties
        
        private string _title;

        public override string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private List<object> _itemList;

        public override List<object> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Key Commands

        public override ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }

        public override ICommand GoNew
        {
            get { return new DelegateCommand(DoGoNew); }
        }

        private void DoGoNew()
        {
            UserManagement userManagement = new UserManagement(_homeNavigationFrame, new UserModel(), true);
            
            if (userManagement != null)
            {
                userManagement.Closed += UserManagement_Closed;
                userManagement.Show(_parent);
            }
        }

        private void UserManagement_Closed(object sender, EventArgs e)
        {
            LoadItems();
        }

        #endregion
    }
}
