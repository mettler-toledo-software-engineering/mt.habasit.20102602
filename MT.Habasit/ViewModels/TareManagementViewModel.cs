﻿using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Data;
using MT.Habasit.Logic;
using MT.Habasit.Views;
using MT.Habasit.Logic.BarcodeReader;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Composition;

namespace MT.Habasit.ViewModels
{
    public class TareManagementViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(TareManagementViewModel);

        private IBarcodeReader _barcodeReader;
        private DelegateCommand _tareAddCommand = null;
        private DelegateCommand _tareSaveCommand = null;
        
        public TareManagementViewModel(Visual parent, AnimatedContentControl homeNavigationFrame, TareModel tare, bool isNew)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Title = isNew ? Localization.Get(Localization.Key.AddNewTare) : Localization.Get(Localization.Key.EditTare);
            SetButtonVisibility(!isNew);

            LoadFields(tare);

            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        private void LoadFields(TareModel tare)
        {
            Tare = tare;
            
            Description = Tare.Description;
            Code = Tare.Code;
            Value = Tare.Value;
            Unit = Tare.Unit;
        }

        #region RegisterEvents

        private void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        private void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
                return;
            }

            _log.ErrorEx($"barcode reader on viewmodel NULL", _sourceClass);
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            _log.InfoEx($"barcode reader on viewmodel data received {e.Data}", _sourceClass);

            Code = e.Data;
        }
        #endregion

        #region UnregisterEvents
        private void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        #endregion

        private void SetButtonVisibility(bool showSaveButton)
        {
            AddVisibility = showSaveButton ? Visibility.Collapsed : Visibility.Visible;
            SaveVisibility = showSaveButton ? Visibility.Visible : Visibility.Collapsed;
        }

        private void CloseWindow()
        {
            UnregisterEventsForViewModel();
            ((TareManagement)_parent).Close();
        }
        
        #region Title
        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
        
        #region Tare
        private TareModel _tare;

        public TareModel Tare
        {
            get
            {
                return _tare;
            }
            set
            {
                if (value != _tare)
                {
                    _tare = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Description
        private string _description;

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    NotifyPropertyChanged();
                    
                    _tareAddCommand?.NotifyCanExecuteChanged();
                    _tareSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion
        
        #region Code
        private string _code;

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    NotifyPropertyChanged();
                    
                    _tareAddCommand?.NotifyCanExecuteChanged();
                    _tareSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion
        
        #region Value
        private double _value;

        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (value != _value)
                {
                    _value = value;
                    NotifyPropertyChanged();
                    
                    _tareAddCommand?.NotifyCanExecuteChanged();
                    _tareSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Unit
        private string _unit;

        public string Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                if (value != _unit)
                {
                    _unit = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region AddVisibility
        private Visibility _addVisibility;

        public Visibility AddVisibility
        {
            get
            {
                return _addVisibility;
            }
            set
            {
                if (value != _addVisibility)
                {
                    _addVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region SaveVisibility
        private Visibility _saveVisibility;

        public Visibility SaveVisibility
        {
            get
            {
                return _saveVisibility;
            }
            set
            {
                if (value != _saveVisibility)
                {
                    _saveVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            CloseWindow();
        }
        #endregion

        #region Save/Add

        private bool CanExecuteAddSaveTareCommand()
        {
            bool result = (string.IsNullOrWhiteSpace(Description) || string.IsNullOrWhiteSpace(Code) ||
                           Value <= 0) == false;
            return result;
        }

        private void GetCurrentEntries()
        {
            Tare.Description = Description;
            Tare.Code = Code;
            Tare.Value = Value;
        }

        #region GoAdd
        public ICommand GoAdd =>
            _tareAddCommand = new DelegateCommand(AddTareCommandExecute, CanExecuteAddSaveTareCommand);

        private async void AddTareCommandExecute()
        {
            GetCurrentEntries();

            int result = await DatabaseExtension.SaveTareInDBAsync(Tare);

            if (result == (int)SqlResult.Failed)
            {
                string title = Localization.Get(Localization.Key.Tarelist);
                string message = Localization.Get(Localization.Key.AddTareFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #region GoSave
        public ICommand GoSave =>
            _tareSaveCommand = new DelegateCommand(SaveTareCommandExecute, CanExecuteAddSaveTareCommand);

        private async void SaveTareCommandExecute()
        {
            GetCurrentEntries();

            bool result = await DatabaseExtension.UpdateTareInDBAsync(Tare);

            if (!result)
            {
                string title = Localization.Get(Localization.Key.Tarelist);
                string message = Localization.Get(Localization.Key.UpdateTareFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #endregion
    }
}