﻿using MT.Habasit.Logic;
using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Threading.Tasks;
using MT.Singularity.Presentation;
using MT.Habasit.Views;
using MT.Habasit.View;

namespace MT.Habasit.ViewModels
{
    public class UserModelControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public UserModelControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, UserModel user)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            User = user;
        }

        #region User
        private UserModel _user;

        public UserModel User
        {
            get { return _user; }
            set
            {
                if (value != _user)
                {
                    _user = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Buttons
        
        #region ---- Edit

        public ICommand GoEdit
        {
            get { return new DelegateCommand(DoGoEdit); }
        }

        private void DoGoEdit()
        {
            UserManagement configWindow =new UserManagement(_homeNavigationFrame, User, false);
            configWindow.Closed += ConfigWindow_Closed;
            configWindow.Show(_parent);
        }

        private void ConfigWindow_Closed(object sender, EventArgs e)
        {
            OnUserItemChanged(new UserItemChangedEventArgs());
        }

        #endregion

        #region ---- Delete

        public ICommand GoDelete
        {
            get { return new DelegateCommand(DoGoDelete); }
        }

        private void DoGoDelete()
        {
            string title = Localization.Get(Localization.Key.DeleteUser);
            string message = String.Format(Localization.Get(Localization.Key.QuestionDeleteUserX), User.Name);
            
            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Show(_parent);
        }

        private void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            Task.Run(() =>
            {
                var result = DatabaseExtension.DeleteUserFromDBAsync(User);

                if (result.Result == true)
                {
                    OnUserItemChanged(new UserItemChangedEventArgs());
                }
                else
                {
                    string title = Localization.Get(Localization.Key.DeleteUser);
                    string message = Localization.Get(Localization.Key.DeleteUserFailed);

                    Message.ShowErrorMessage(_parent, title, message);
                }
            });
        }

        #endregion

        #endregion

        #region Event UserItemChangedEventArgs

        public event EventHandler<UserItemChangedEventArgs> UserItemChanged;

        protected virtual void OnUserItemChanged(UserItemChangedEventArgs e)
        {
            if (UserItemChanged != null)
                UserItemChanged(this, e);
        }

        public class UserItemChangedEventArgs : EventArgs
        {
            public UserItemChangedEventArgs() { }
        }

        #endregion

        #region Visibility

        #region ---- VisibilityEdit

        private Visibility _visibilityEdit = Visibility.Visible;
        public Visibility VisibilityEdit
        {
            get { return _visibilityEdit; }
            set
            {
                if (_visibilityEdit != value)
                {
                    _visibilityEdit = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- VisibilityDelete

        private Visibility _visibilityDelete = Visibility.Visible;
        public Visibility VisibilityDelete
        {
            get { return _visibilityDelete; }
            set
            {
                if (_visibilityDelete != value)
                {
                    _visibilityDelete = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion
    }
}
