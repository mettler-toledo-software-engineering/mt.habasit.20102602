﻿using log4net;
using MT.Habasit.Data;
using MT.Habasit.Logic;
using MT.Habasit.Logic.LegicKeyReader;
using MT.Habasit.Models;
using MT.Habasit.Views;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.Habasit.ViewModels
{
    public class UserManagementViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(UserManagementViewModel);

        private LegicKeyReader _legicKeyReader = ApplicationBootstrapperBase.CompositionContainer.Resolve<LegicKeyReader>();
        private DelegateCommand _userAddCommand = null;
        private DelegateCommand _userSaveCommand = null;

        public UserManagementViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, UserModel user, bool isNew)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Title = isNew ? Localization.Get(Localization.Key.AddNewUser) : Localization.Get(Localization.Key.EditUser);
            SetButtonVisibility(!isNew);

            LoadFields(user);
            RegisterLegicCardReaderEvent();
        }

        private void RegisterLegicCardReaderEvent()
        {
            _log.InfoEx("register card reader event...", _sourceClass);
            _legicKeyReader.RfidReceived += LegicKeyReader_RfidReceived;
        }

        public void UnregisterLegicCardReaderEvent()
        {
            _log.InfoEx("unregister card reader event...", _sourceClass);

            if (_legicKeyReader != null)
            {
                _legicKeyReader.RfidReceived -= LegicKeyReader_RfidReceived;
            }
        }

        private void LegicKeyReader_RfidReceived(object sender, DataReceivedEventArgs e)
        {
            _log.InfoEx($"legic key reader on viewmodel data received {e.Data}", _sourceClass);

            UID = e.Data;
        }

        private void LoadFields(UserModel user)
        {
            User = user;

            Firstname = User.Firstname;
            Lastname = User.Lastname;
            UID = User.UID;
            Key = User.Key;
        }

        private void SetButtonVisibility(bool showSaveButton)
        {
            AddVisibility = showSaveButton ? Visibility.Collapsed : Visibility.Visible;
            SaveVisibility = showSaveButton ? Visibility.Visible : Visibility.Collapsed;
        }

        private void CloseWindow()
        {
            UnregisterLegicCardReaderEvent();
            ((UserManagement)_parent).Close();
        }

        #region Title
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region User
        private UserModel _user;

        public UserModel User
        {
            get { return _user; }
            set
            {
                if (value != _user)
                {
                    _user = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Firstname
        private string _firstname;

        public string Firstname
        {
            get { return _firstname; }
            set
            {
                if (value != _firstname)
                {
                    _firstname = value;
                    NotifyPropertyChanged();

                    _userAddCommand?.NotifyCanExecuteChanged();
                    _userSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Lastname
        private string _lastname;

        public string Lastname
        {
            get { return _lastname; }
            set
            {
                if (value != _lastname)
                {
                    _lastname = value;
                    NotifyPropertyChanged();

                    _userAddCommand?.NotifyCanExecuteChanged();
                    _userSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region UID
        private string _uID;

        public string UID
        {
            get
            {
                return _uID;
            }
            set
            {
                if (value != _uID)
                {
                    _uID = value;
                    NotifyPropertyChanged();

                    _userAddCommand?.NotifyCanExecuteChanged();
                    _userSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Key
        private string _key;

        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                if (value != _key)
                {
                    _key = value;
                    NotifyPropertyChanged();

                    _userAddCommand?.NotifyCanExecuteChanged();
                    _userSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region AddVisibility
        private Visibility _addVisibility;

        public Visibility AddVisibility
        {
            get
            {
                return _addVisibility;
            }
            set
            {
                if (value != _addVisibility)
                {
                    _addVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region SaveVisibility
        private Visibility _saveVisibility;

        public Visibility SaveVisibility
        {
            get
            {
                return _saveVisibility;
            }
            set
            {
                if (value != _saveVisibility)
                {
                    _saveVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            CloseWindow();
        }
        #endregion

        #region Save/Add

        private bool CanExecuteAddSaveUserCommand()
        {
            bool result = (string.IsNullOrWhiteSpace(Firstname) || string.IsNullOrWhiteSpace(Lastname) ||
                           string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(UID)) == false;
            return result;
        }

        private void GetCurrentEntries()
        {
            User.Firstname = Firstname;
            User.Lastname = Lastname;
            User.UID = UID;
            User.Key = Key;
        }

        #region GoAdd
        public ICommand GoAdd =>
            _userAddCommand = new DelegateCommand(AddUserCommandExecute, CanExecuteAddSaveUserCommand);

        private async void AddUserCommandExecute()
        {
            GetCurrentEntries();

            int result = await DatabaseExtension.SaveUserInDBAsync(User);

            if (result == (int)SqlResult.Failed)
            {
                string title = Localization.Get(Localization.Key.Userlist);
                string message = Localization.Get(Localization.Key.AddUserFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #region GoSave
        public ICommand GoSave =>
            _userSaveCommand = new DelegateCommand(SaveUserCommandExecute, CanExecuteAddSaveUserCommand);

        private async void SaveUserCommandExecute()
        {
            GetCurrentEntries();

            bool result = await DatabaseExtension.UpdateUserInDBAsync(User);

            if (!result)
            {
                string title = Localization.Get(Localization.Key.Userlist);
                string message = Localization.Get(Localization.Key.UpdateUserFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #endregion
    }
}
