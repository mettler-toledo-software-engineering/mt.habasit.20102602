﻿using System;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Habasit.View;

namespace MT.Habasit.ViewModels
{
    public class QuestionBoxViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;

        public QuestionBoxViewModel(Visual parent, string title, string message)
        {
            _parent = parent;

            Title = title;
            Message = message;
        }

        private void CloseBox()
        {
            ((QuestionBox)_parent).Close();
        }

        #region Title
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Message
        private string _message;

        public string Message
        {
            get { return _message; }
            set
            {
                if (value != _message)
                {
                    _message = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoYes

        public ICommand GoYes
        {
            get { return new DelegateCommand(DoGoYes); }
        }

        private void DoGoYes()
        {
            OnYesButtonClicked(new YesButtonClickedEventArgs());
            CloseBox();
        }

        #endregion

        #region GoNo

        public ICommand GoNo
        {
            get { return new DelegateCommand(DoGoNo); }
        }

        private void DoGoNo()
        {
            OnNoButtonClicked(new NoButtonClickedEventArgs());
            CloseBox();
        }

        #endregion

        #region Event YesButtonClicked

        public event EventHandler<YesButtonClickedEventArgs> YesButtonClicked;

        protected virtual void OnYesButtonClicked(YesButtonClickedEventArgs e)
        {
            if (YesButtonClicked != null)
                YesButtonClicked(this, e);
        }

        public class YesButtonClickedEventArgs : EventArgs
        {
            public YesButtonClickedEventArgs()
            {
            }
        }

        #endregion

        #region Event NoButtonClicked

        public event EventHandler<NoButtonClickedEventArgs> NoButtonClicked;

        protected virtual void OnNoButtonClicked(NoButtonClickedEventArgs e)
        {
            if (NoButtonClicked != null)
                NoButtonClicked(this, e);
        }

        public class NoButtonClickedEventArgs : EventArgs
        {
            public NoButtonClickedEventArgs()
            {
            }
        }

        #endregion
    }
}
