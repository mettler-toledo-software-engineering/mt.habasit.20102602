﻿using System;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Views;
using MT.Habasit.Logic;

namespace MT.Habasit.ViewModels
{
    public class ManualTareViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public ManualTareViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Unit = Globals.GetCurrentScaleUnit();
        }

        private void CloseWindow()
        {
            ((ManualTare)_parent).Close();
        }
        
        #region Title
        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region TareValue
        private string _tareValue;

        public string TareValue
        {
            get
            {
                return _tareValue;
            }
            set
            {
                if (value != _tareValue)
                {
                    _tareValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Unit
        private string _unit;

        public string Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                if (value != _unit)
                {
                    _unit = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            CloseWindow();
        }
        #endregion

        #region GoAccept
        public ICommand GoAccept
        {
            get { return new DelegateCommand(DoGoAccept); }
        }

        private void DoGoAccept()
        {
            double weight;
            double.TryParse(TareValue, out weight);

            OnAcceptButtonClicked(new AcceptButtonClickedEventArgs(weight));
            CloseWindow();
        }
        #endregion

        #region Event AcceptButtonClicked

        public event EventHandler<AcceptButtonClickedEventArgs> AcceptButtonClicked;

        protected virtual void OnAcceptButtonClicked(AcceptButtonClickedEventArgs e)
        {
            if (AcceptButtonClicked != null)
                AcceptButtonClicked(this, e);
        }

        public class AcceptButtonClickedEventArgs : EventArgs
        {
            public double Weight { get; set; }

            public AcceptButtonClickedEventArgs(double weight)
            {
                Weight = weight;
            }
        }

        #endregion

        #region Event CancelButtonClicked

        public event EventHandler<CancelButtonClickedEventArgs> CancelButtonClicked;

        protected virtual void OnCancelButtonClicked(CancelButtonClickedEventArgs e)
        {
            if (CancelButtonClicked != null)
                CancelButtonClicked(this, e);
        }

        public class CancelButtonClickedEventArgs : EventArgs
        {
            public CancelButtonClickedEventArgs()
            {
            }
        }

        #endregion
    }
}
