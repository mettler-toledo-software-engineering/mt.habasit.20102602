﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Logic;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;
using MT.Habasit.View.Controlls;
using MT.Habasit.Views;
using MT.Habasit.View;
using log4net;
using MT.Singularity.Logging;
using MT.Habasit.Logic.BarcodeReader;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Composition;
using Timer = System.Timers.Timer;

namespace MT.Habasit.ViewModels
{
    public class TareSelectionViewModel: PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(TareSelectionViewModel);

        private Timer _timer;
        private ProcessFlow _currentProcess = ProcessFlow.Current;
        private Settings _currentSettings = Settings.Current;
        private IScale _scale;
        private IBarcodeReader _barcodeReader;
        private TransactionModel _transaction;

        private List<TareModel> _tareModels = new List<TareModel>();
        private List<List<TareModel>> _tareListPages = new List<List<TareModel>>();

        private DelegateCommand _pageBackwardsCommand = null;
        private DelegateCommand _pageForwardsCommand = null;
        private DelegateCommand _tareCommand = null;

        public TareSelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, TransactionModel transaction)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _transaction = transaction;
            _scale = HomeScreenViewModel.Instance.GetSelectedScale();

            UpdateOverviewItem();
            LoadPages();
            EnableBackButton();
        }

        private void LoadPages()
        {
            if (_currentProcess.TareList == null)
            {
                return;
            }

            int maxModels = Globals.MaxTaresPerPage;
            int lastModelIndex = _currentProcess.TareList.Count - 1;

            if (lastModelIndex >= 0)
            {
                TareModel lastModel = _currentProcess.TareList[lastModelIndex];

                List<TareModel> modelList = new List<TareModel>(maxModels);

                foreach (TareModel model in _currentProcess.TareList)
                {
                    modelList.Add(model);

                    if (modelList.Count == maxModels || model == lastModel)
                    {
                        _tareListPages.Add(modelList);
                        modelList = new List<TareModel>(maxModels);
                    }
                }

                LastPageIndex = _tareListPages.Count - 1;

                LoadCurrentPage();
            }
        }

        private void LoadCurrentPage()
        {
            TareButtons = new ObservableCollection<TareButtonControl>();

            List<TareModel> currentTares = _tareListPages[CurrentPageIndex];

            foreach (TareModel tare in currentTares)
            {
                TareButtonControl tareButton = new TareButtonControl(_homeNavigationFrame, tare);
                tareButton.ViewModel.TareButtonClicked += ViewModel_TareButtonClicked;
                TareButtons.Add(tareButton);
            }
        }

        #region RegisterEvents

        public void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
                return;
            }

            _log.ErrorEx($"barcode reader on viewmodel NULL", _sourceClass);
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            UnregisterEventsForViewModel();

            string searchCode = e.Data;

            _log.InfoEx($"barcode reader on viewmodel data received {searchCode}", _sourceClass);


            TareModel scannedTare = _currentProcess.TareList.Find(m => m.Code == searchCode);

            if (scannedTare == null)
            {
                ShowCodeNotFoundMessage();
                return;
            }

            ViewModel_TareButtonClicked(new object(), new TareButtonControlViewModel.TareButtonClickedEventArgs(scannedTare));
        }
        
        InfoBox _infoBox;

        private void ShowCodeNotFoundMessage()
        {
            string title = Localization.Get(Localization.Key.ScanTareCode);
            string message = Localization.Get(Localization.Key.CodeNotFound);
            int interval = Globals.MessageShowInterval;

            _infoBox = new InfoBox(title, message, MessageBoxIcon.Information);
            _infoBox.Show(_parent);

            KeepBoxOpen(interval);
        }

        private void KeepBoxOpen(int seconds)
        {
            _timer = new Timer(seconds);
            _timer.Elapsed += _timer_Elapsed;
            _timer.AutoReset = false;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _infoBox.Close();
            RegisterEventsForViewModel();
        }


        #endregion

        #region UnregisterEvents
        public void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        #endregion
        
        private void EnableBackButton(bool enable = true)
        {
            EnableBack = enable;
        }

        public ICommand GoGetTareList =>
            _tareCommand = new DelegateCommand(GetAllTaresFromDB, CanExecuteCommand);

        private bool CanExecuteCommand()
        {
            return true;
        }

        private void GetAllTaresFromDB()
        {
            _tareModels = _currentProcess.TareList;

            if (_tareModels.Count == 0)
            {
                string title = Localization.Get(Localization.Key.Tarelist);
                string message = Localization.Get(Localization.Key.NoTareFound);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                TareButtons = new ObservableCollection<TareButtonControl>();

                foreach (TareModel tare  in _tareModels)
                {
                    TareButtonControl tareButton = new TareButtonControl(_homeNavigationFrame, tare);
                    tareButton.ViewModel.TareButtonClicked += ViewModel_TareButtonClicked;
                    TareButtons.Add(tareButton);
                }
            }
        }

        void ViewModel_TareButtonClicked(object sender, TareButtonControlViewModel.TareButtonClickedEventArgs e)
        {
            _log.InfoEx($"tare '{e.Tare.ToString()}' selected", _sourceClass);
            TareWithWeight(e.Tare);
        }

        private void UpdateOverviewItem()
        {
            OverviewItem = new List<OverviewControl>();
            OverviewItem.Add(new OverviewControl(_homeNavigationFrame, _transaction));
        }

        private void ReturnToMainPage()
        {
            _log.InfoEx("Logout!", _sourceClass);
            ProcessFlow.Current.Logout();
            _homeNavigationFrame.Back();
            _homeNavigationFrame.Back();
        }

        #region EnableBack
        private bool _enableBack;

        public bool EnableBack
        {
            get
            {
                return _enableBack;
            }
            set
            {
                if (value != _enableBack)
                {
                    _enableBack = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Tare Weight
        private async void TareWithWeight(TareModel tare)
        {
            if(_scale != null)
            {
                WellknownWeightUnit unit = _scale.CurrentDisplayUnit;
                await _scale.PresetTareAsync(tare.Value.ToString(), unit);
                _scale.NewChangedWeightInDisplayUnit += _scale_NewChangedWeightInDisplayUnit;
                
                _transaction.Tare = tare;

                UpdateOverviewItem();
                EnableBackButton(false);
            }
        }

        private void _scale_NewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _transaction.TareWeight = weight.TareWeight;
            _transaction.NetWeight = weight.NetWeight;
            _transaction.GrossWeight = weight.GrossWeight;

            _transaction.Unit = weight.Unit.ToAbbreviation();
        }
        #endregion

        #region OverviewItem
        private List<OverviewControl> _overviewItem;

        public List<OverviewControl> OverviewItem
        {
            get { return _overviewItem; }
            set
            {
                if (value != _overviewItem)
                {
                    _overviewItem = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region TareButtons
        private ObservableCollection<TareButtonControl> _tareButtons;

        public ObservableCollection<TareButtonControl> TareButtons
        {
            get
            {
                return _tareButtons;
            }
            set
            {
                if (value != _tareButtons)
                {
                    _tareButtons = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region CurrentPageIndex
        private int _currentPageIndex;

        public int CurrentPageIndex
        {
            get
            {
                return _currentPageIndex;
            }
            set
            {
                if (value != _currentPageIndex)
                {
                    _currentPageIndex = value;
                    NotifyPropertyChanged();

                    _pageBackwardsCommand?.NotifyCanExecuteChanged();
                    _pageForwardsCommand?.NotifyCanExecuteChanged();

                    LoadCurrentPage();
                }
            }
        }
        #endregion

        #region LastPageIndex
        private int _lastPageIndex;

        public int LastPageIndex
        {
            get
            {
                return _lastPageIndex;
            }
            set
            {
                if (value != _lastPageIndex)
                {
                    _lastPageIndex = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Back
        /// <summary>
        /// GoBack
        /// </summary>
        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            string title = Localization.Get(Localization.Key.CancelProcess);
            string message = Localization.Get(Localization.Key.QuestionCancelProcess);

            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Show(_parent);
        }

        void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            ReturnToMainPage();
        }
        #endregion

        #region GoDone
        public ICommand GoDone =>
            _tareCommand = new DelegateCommand(SaveTransactionCommandExecute, CanExecuteCommand);

        private async void SaveTransactionCommandExecute()
        {
            int result = await DatabaseExtension.SaveTransactionInDBAsync(_transaction);
            string title = Localization.Get(Localization.Key.Quit);
            string message;

            if (result == 0)
            {
                message = Localization.Get(Localization.Key.AddTransactionFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                message = Localization.Get(Localization.Key.TransactionSaved);
                int interval = Globals.MessageShowIntervalShort;

                Message.ShowInfoMessage(_parent, title, message, interval);

                ReturnToMainPage();
            }
        }
        #endregion

        #region GoManualTare
        public ICommand GoManualTare
        {
            get { return new DelegateCommand(DoGoManualTare); }
        }

        private void DoGoManualTare()
        {
            ManualTare window = new ManualTare(_homeNavigationFrame);
            window.ViewModel.AcceptButtonClicked += ViewModel_AcceptButtonClicked;
            window.Show(_parent);
        }

        void ViewModel_AcceptButtonClicked(object sender, ManualTareViewModel.AcceptButtonClickedEventArgs e)
        {
            TareModel tare = new TareModel("manual tare", e.Weight, Globals.GetCurrentScaleUnit());
            TareWithWeight(tare);
        }
        #endregion

        #region GoPageBackwards
        public ICommand GoPageBackwards =>
            _pageBackwardsCommand = new DelegateCommand(PageBackwardsCommandExecute, CanExecutePageBackwardsCommand);

        private bool CanExecutePageBackwardsCommand()
        {
            return CurrentPageIndex != 0;
        }

        private void PageBackwardsCommandExecute()
        {
            CurrentPageIndex--;
        }
        #endregion

        #region GoPageForwards
        public ICommand GoPageForwards =>
            _pageForwardsCommand = new DelegateCommand(PageForwardsCommandExecute, CanExecutePageForwardsCommand);

        private bool CanExecutePageForwardsCommand()
        {
            return CurrentPageIndex < LastPageIndex;
        }

        private void PageForwardsCommandExecute()
        {
            CurrentPageIndex++;
        }
        #endregion
    }
}
