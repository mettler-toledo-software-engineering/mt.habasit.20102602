﻿using MT.Habasit.Logic;
using MT.Habasit.Logic.LegicKeyReader;
using MT.Habasit.Models;
using MT.Habasit.View;
using MT.Habasit.Views;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.IO;

namespace MT.Habasit.ViewModels
{
    public class SettingsPageViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private Settings _currentSettings;
      
        public SettingsPageViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            _currentSettings = Settings.Current;
        
            LoadFields();
        }

        private void LoadFields()
        {
            FileLocation = _currentSettings.FileLocation;
            Index = _currentSettings.Index;
            Delimiter = _currentSettings.Delimiter;
        }

        private void SaveFields()
        {
            _currentSettings.FileLocation = FileLocation;
            _currentSettings.Index = Index;
            _currentSettings.Delimiter = Delimiter;

            _currentSettings.Save();
        }

        private bool HasChanged()
        {
            return (FileLocation != _currentSettings.FileLocation) ||
                (Index != _currentSettings.Index) ||
                (Delimiter != _currentSettings.Delimiter);
        }

        #region FileLocation
        private string _fileLocation;

        public string FileLocation
        {
            get
            {
                return _fileLocation;
            }
            set
            {
                if (value != _fileLocation)
                {
                    _fileLocation = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Index
        private string _index;

        public string Index
        {
            get
            {
                return _index;
            }
            set
            {
                if (value != _index)
                {
                    _index = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Delimiter
        private string _delimiter;

        public string Delimiter
        {
            get
            {
                return _delimiter;
            }
            set
            {
                if (value != _delimiter)
                {
                    _delimiter = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoBack

        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            if (!HasChanged())
            {
                _homeNavigationFrame.Back();
                return;
            }

            if (!Directory.Exists(FileLocation))
            {
                ShowInvalidLocationMessage();
                return;
            }

            AskToSaveSettings();
        }

        private void AskToSaveSettings()
        {
            string title = Localization.Get(Localization.Key.SaveSettings);
            string message = Localization.Get(Localization.Key.QuestionSaveChanges);

            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Closed += Question_Closed;
            question.Show(_parent);
        }

        private void ShowInvalidLocationMessage()
        {
            string title = Localization.Get(Localization.Key.SaveSettings);
            string message = Localization.Get(Localization.Key.InvalidFileLocation);

            Message.ShowInfoMessage(_parent, title, message);
        }

        private void Question_Closed(object sender, EventArgs e)
        {
            _homeNavigationFrame.Back();
        }

        private void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            SaveFields();
        }
        #endregion

        #region GoUserManagement

        public ICommand GoUserManagement
        {
            get { return new DelegateCommand(DoGoUserManagement); }
        }

        private void DoGoUserManagement()
        {
            _homeNavigationFrame.NavigateTo(new ItemManagement(_homeNavigationFrame, new UserModel()));
        }
        #endregion

        #region GoMaterialManagement

        public ICommand GoMaterialManagement
        {
            get { return new DelegateCommand(DoGoMaterialManagement); }
        }

        private void DoGoMaterialManagement()
        {
            _homeNavigationFrame.NavigateTo(new ItemManagement(_homeNavigationFrame, new MaterialModel()));
        }
        #endregion

        #region GoTareManagement

        public ICommand GoTareManagement
        {
            get { return new DelegateCommand(DoGoTareManagement); }
        }

        private void DoGoTareManagement()
        {
            _homeNavigationFrame.NavigateTo(new ItemManagement(_homeNavigationFrame, new TareModel()));
        }
        #endregion
    }
}
