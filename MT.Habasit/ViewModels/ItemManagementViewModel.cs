﻿using System;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Model;
using System.Collections.Generic;

namespace MT.Habasit.ViewModels
{
    public class ItemManagementViewModel : PropertyChangedBase
    {
        public ItemManagementViewModel()
        {

        }

        #region Item Overview
        
        public virtual void LoadItems()
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateTitle()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Properties
        
        public virtual string Title
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public virtual List<object> ItemList
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region Key Commands

        public virtual ICommand GoBack
        {
            get { throw new NotImplementedException(); }
        }

        public virtual ICommand GoNew
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
