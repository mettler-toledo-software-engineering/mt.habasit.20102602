﻿using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;

namespace MT.Habasit.ViewModels
{
    public class OverviewControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;

        public OverviewControlViewModel(Visual parent, AnimatedContentControl homeNavigationFrame, TransactionModel transaction)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Transaction = transaction;
       } 

        #region Transaction
        private TransactionModel _transaction;

        public TransactionModel Transaction
        {
            get { return _transaction; }
            set
            {
                if (value != _transaction)
                {
                    _transaction = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
    }
}
