﻿using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Data;
using MT.Habasit.Logic;
using MT.Habasit.Views;
using log4net;
using MT.Singularity.Logging;
using MT.Habasit.Logic.BarcodeReader;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Composition;

namespace MT.Habasit.ViewModels
{
    public class MaterialManagementViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(MaterialManagementViewModel);

        private IBarcodeReader _barcodeReader;
        private DelegateCommand _materialAddCommand = null;
        private DelegateCommand _materialSaveCommand = null;

        public MaterialManagementViewModel(Visual parent, AnimatedContentControl homeNavigationFrame, MaterialModel material, bool isNew)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            
            Title = isNew ? Localization.Get(Localization.Key.AddNewMaterial) : Localization.Get(Localization.Key.EditMaterial);
            SetButtonVisibility(!isNew);

            LoadFields(material);

            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        private void LoadFields(MaterialModel material)
        {
            Material = material;
            
            Code = Material.Code;
            Type = Material.Type;
            System = Material.System;
        }

        #region RegisterEvents

        private void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        private void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
                return;
            }

            _log.Error($"barcode reader on viewmodel NULL");
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            _log.InfoEx($"barcode reader on viewmodel data received {e.Data}", _sourceClass);

            Code = e.Data;
        }
        #endregion

        #region UnregisterEvents
        private void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        #endregion

        private void SetButtonVisibility(bool showSaveButton)
        {
            AddVisibility = showSaveButton ? Visibility.Collapsed : Visibility.Visible;
            SaveVisibility = showSaveButton ? Visibility.Visible : Visibility.Collapsed;
        }

        private void CloseWindow()
        {
            UnregisterEventsForViewModel();
            ((MaterialManagement)_parent).Close();
        }

        #region Title
        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Material
        private MaterialModel _material;

        public MaterialModel Material
        {
            get
            {
                return _material;
            }
            set
            {
                if (value != _material)
                {
                    _material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Code
        private string _code;

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    NotifyPropertyChanged();

                    _materialAddCommand?.NotifyCanExecuteChanged();
                    _materialSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Type
        private string _type;

        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    NotifyPropertyChanged();

                    _materialAddCommand?.NotifyCanExecuteChanged();
                    _materialSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region System
        private string _system;

        public string System
        {
            get
            {
                return _system;
            }
            set
            {
                if (value != _system)
                {
                    _system = value;
                    NotifyPropertyChanged();

                    _materialAddCommand?.NotifyCanExecuteChanged();
                    _materialSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region AddVisibility
        private Visibility _addVisibility;

        public Visibility AddVisibility
        {
            get
            {
                return _addVisibility;
            }
            set
            {
                if (value != _addVisibility)
                {
                    _addVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region SaveVisibility
        private Visibility _saveVisibility;

        public Visibility SaveVisibility
        {
            get
            {
                return _saveVisibility;
            }
            set
            {
                if (value != _saveVisibility)
                {
                    _saveVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            CloseWindow();
        }
        #endregion

        #region Save/Add
        
        private bool CanExecuteAddSaveMaterialCommand()
        {
            bool result = (string.IsNullOrWhiteSpace(Code) || string.IsNullOrWhiteSpace(Type)) == false;
            return result;
        }
        
        private void GetCurrentEntries()
        {
            Material.Code = Code;
            Material.Type = Type;
            Material.System = System;
        }

        #region GoAdd
        public ICommand GoAdd =>
            _materialAddCommand = new DelegateCommand(AddMaterialCommandExecute, CanExecuteAddSaveMaterialCommand);

        private async void AddMaterialCommandExecute()
        {
            GetCurrentEntries();

            int result = await DatabaseExtension.SaveMaterialInDBAsync(Material);

            if (result == (int)SqlResult.Failed)
            {
                string title = Localization.Get(Localization.Key.AddNewMaterial);
                string message = Localization.Get(Localization.Key.AddMaterialFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #region GoSave
        public ICommand GoSave =>
            _materialSaveCommand = new DelegateCommand(SaveMaterialCommandExecute, CanExecuteAddSaveMaterialCommand);

        private async void SaveMaterialCommandExecute()
        {
            GetCurrentEntries();

            bool result = await DatabaseExtension.UpdateMaterialInDBAsync(Material);

            if (!result)
            {
                string title = Localization.Get(Localization.Key.EditMaterial);
                string message = Localization.Get(Localization.Key.UpdateMaterialFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #endregion
    }
}
