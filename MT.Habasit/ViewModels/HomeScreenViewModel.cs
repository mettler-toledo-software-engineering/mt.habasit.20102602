﻿using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.Devices;
using MT.Habasit.Logic;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Logging;

namespace MT.Habasit.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : PropertyChangedBase
    {
        private IInterfaceService _interfaces;
        public static HomeScreenViewModel Instance;
        private IScaleService _scaleService;
        
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(HomeScreenViewModel);

        private System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        /// 
        public HomeScreenViewModel()
        {
            Instance = this;

            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            InitializeViewModel().ContinueWith(OnInizializeViewModelCompleted);
        }

        private void OnInizializeViewModelCompleted(Task task)
        {
            if (task.Exception != null)
            {
                _log.ErrorEx("Initializing HomeViewModel failed!", _sourceClass);
                return;
            }

            _log.InfoEx("HomeViewModel initialized", _sourceClass);
        }

        public async Task InitializeViewModel()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            _interfaces = await platformEngine.GetInterfaceServiceAsync();

            _scaleService = await platformEngine.GetScaleServiceAsync();
            if (_scaleService == null)
                return;
            
            Globals.SetCurrentScale(_scaleService.SelectedScale);

            //await DatabaseExtension.LoadAllEntries();
        }

        public async void InitializeScales()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;

            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            await platformEngine.GetScaleServiceAsync();
        }

        public IScale GetSelectedScale()
        {
            return _scaleService.SelectedScale;
        }

        #region WeightDisplay

        private int _weightDisplayHeight = 200;

        public int WeightDisplayHeight
        {
            get { return _weightDisplayHeight; }
            set
            {
                if (_weightDisplayHeight != value)
                {
                    _weightDisplayHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Visibility
        private Visibility _statusBarVisibility = Visibility.Collapsed;

        public Visibility StatusBarVisibility
        {
            get { return _statusBarVisibility; }
            set
            {
                if (_statusBarVisibility != value)
                {
                    _statusBarVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _weightDisplayShow;

        public Visibility WeightDisplayShow
        {
            get { return _weightDisplayShow; }
            set
            {
                if (_weightDisplayShow != value)
                {
                    _weightDisplayShow = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
    }
}
