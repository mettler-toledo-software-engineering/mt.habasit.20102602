﻿using System;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Habasit.Models;
using MT.Singularity.Platform.Devices.Scale;
using MT.Habasit.Logic;
using System.Collections.Generic;
using System.Linq;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;
using log4net;
using MT.Singularity.Logging;
using MT.Habasit.Logic.USB;
using MT.Habasit.Views;

using Timer = System.Timers.Timer;
using MT.Habasit.Logic.LegicKeyReader;
using MT.Habasit.View;
using MT.Singularity;
using System.Threading.Tasks;
using System.Threading;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Infrastructure;

namespace MT.Habasit.ViewModels
{
    public class StartPageViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(StartPageViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private readonly UsbHandler _usbHandler = new UsbHandler(Globals.IND930USBDrive);

        private ProcessFlow _currentProcess;
        private Settings _currentSettings;
        private LegicKeyReader _legicKeyReader;

        private ISecurityService _securityService;
        private bool _hasPermission;
        private Timer _timer;
        private IScale _scale;
        private DelegateCommand _goToSettingsCommand;
        private DelegateCommand _goContinueCommand;
        private LoginBox _login;

        private List<TransactionModel> _transactionList = new List<TransactionModel>();
        private List<UserModel> _userList = new List<UserModel>();
        private List<MaterialModel> _materialList = new List<MaterialModel>();
        private List<TareModel> _tareList = new List<TareModel>();

        public StartPageViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, ISecurityService securityService)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _securityService = securityService;
            //  _currentSettings = Settings.Current;
            _currentProcess = ProcessFlow.Current;
            _legicKeyReader = ApplicationBootstrapperBase.CompositionContainer.Resolve<LegicKeyReader>();
            Information = "Initialize start page view model";

            RegisterLegicCardReaderEvent();

            UpdateInformation();
            LoadDatabaseEntries();

            InitializeCurrentScale();
            InitializeSecurityServiceEvents();

            SetPermissionState();
            StartTimerForTransactionReader();
        }

        #region Register / Unregister Events 

        public async void RegisterLegicCardReaderEvent()
        {

            _log.InfoEx("register legic card reader event...", _sourceClass);
            _legicKeyReader.RfidReceived += LegicKeyReader_RfidReceived;
        }

        public void UnregisterLegicCardReaderEvent()
        {
            _log.InfoEx("unregister legic card reader event...", _sourceClass);
            _legicKeyReader.RfidReceived -= LegicKeyReader_RfidReceived;
        }

        private void LegicKeyReader_RfidReceived(object sender, DataReceivedEventArgs e)
        {
            string cardData = e.Data;

            if (_login != null)
                _login.Close();

            UserModel user = _userList.Find(u => u.UID == cardData);

            if (user == null)
            {
                ShowUserNotFoundMessage();
                return;
            }

            if (!ProcessFlow.Current.HasLoggedUser())
            {
                _log.InfoEx($"automatically log in with user '{user.ToString()}'", _sourceClass);

                LoginWithUser(user);
            }
        }

        #endregion

        #region InfoBox

        private void ShowUserNotFoundMessage()
        {
            string title = Localization.Get(Localization.Key.Login);
            string message = Localization.Get(Localization.Key.UserNotFound);

            ShowMessageBox(title, message);
        }

        private void ShowMessageBox(string title, string message)
        {
            InfoBox infoBox = new InfoBox(title, message, MessageBoxIcon.Error);
            infoBox.Show(_parent);

            Thread.Sleep(Globals.MessageShowIntervalShort);

            infoBox.Close();
        }

        #endregion

        public void LoadDatabaseEntries()
        {
            DatabaseExtension.LoadAllEntries().ContinueWith(LoadLists);
        }

        private void LoadLists(Task obj)
        {
            if (obj.IsCompleted)
            {
                _transactionList = _currentProcess.TransactionList;
                _userList = _currentProcess.UserList;
                _materialList = _currentProcess.MaterialList;
                _tareList = _currentProcess.TareList;
            }
        }

        private void UpdateInformation()
        {
            Information = "Update information";

            ProjectNumber = Globals.ProjectNumber;
            Version = Globals.GetVersion();
        }

        #region Initialization

        private void InitializeSecurityServiceEvents()
        {
            if (_securityService != null)
            {
                _securityService.CurrentUserChanged -= _securityService_CurrentUserChanged;
                _securityService.CurrentUserChanged += _securityService_CurrentUserChanged;
            }
        }

        private void InitializeUsbEvents()
        {
            if (_usbHandler != null)
            {
                _usbHandler.DeviceInsertedEvent += _usbHandler_DeviceInsertedEvent;
                _usbHandler.DeviceRemovedEvent += _usbHandler_DeviceRemovedEvent;
            }
        }

        private void InitializeCurrentScale()
        {
            Information = "Initialize current scale";

            _scale = Globals.GetCurrentScale();
            _scale.NewChangedWeightInDisplayUnit += _scale_NewChangedWeightInDisplayUnit;
        }

        private void _scale_NewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            Information = $"Gross Weight changed: {weight.GrossWeightString} {weight.Unit.ToAbbreviation()}";
        }

        #endregion

        #region Permission

        private void SetPermissionState()
        {
            _hasPermission = Permissions.IsSupervisorOrHigher(_securityService.CurrentUser.Permission);
        }

        private void _securityService_CurrentUserChanged(User user)
        {
            SetPermissionState();
            _goToSettingsCommand.NotifyCanExecuteChanged();
        }

        #endregion

        #region TXT-File generation
        
        private void StartTimerForTransactionReader()
        {
            int interval = 10000; //_currentSettings.Interval;

            //  _log.InfoEx("Start timer for transaction reader", _sourceClass);
            _timer = new Timer(interval);
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StopTimerForTransactionReader();

            if (IsFirstDayOfMonth())
                GenerateExportFiles();
            else
                StartTimerForTransactionReader();
        }

        private bool IsFirstDayOfMonth()
        {
            DateTime today = DateTime.Now.Date;
            DateTime lastDate = Settings.Current.FileWrittenDate;

            if ((today.Day > 1) && (Settings.Current.FileWritten))
            {
                Settings.Current.FileWritten = false;
                Settings.Current.Save();
            }

            bool fileWritten = Settings.Current.FileWritten;
            bool result = (today > lastDate) && (today.Day == 1) && (!fileWritten);

            if (result)
                _log.InfoEx($"Actual export param  {today},{lastDate}", _sourceClass);
            return result;
            
        }

        private void StopTimerForTransactionReader()
        {
            // _log.InfoEx("Stop timer for transaction reader", _sourceClass);

            _timer.Stop();
            _timer = null;
        }

        private async void GenerateExportFiles()
        {
            List<TransactionModel> transactionList = _transactionList;

            if (transactionList == null)
            {
                _log.InfoEx($"Generating export files - no transactions found!", _sourceClass);

                StartTimerForTransactionReader();
                return;
            }

            transactionList.RemoveAll(model => model.IsExported == true);
            await DatabaseExtension.DeleteTransactionExported();

            if (transactionList.Count > 0)
            {
                bool succeed = Extensions.WriteTransactionsToFileSystemAsCsv(transactionList);
                succeed &= Extensions.WriteTransactionsToFileSystemAsXlsx(transactionList);

                if (succeed)
                {
                    transactionList.ForEach(async d =>
                    {
                        d.IsExported = true;
                        await DatabaseExtension.UpdateTransactionInDBAsync(d);
                    });
                    
                   _log.Success($"file generated successfully", _sourceClass);

                    Settings.Current.FileWritten = true;
                    Settings.Current.FileWrittenDate = DateTime.Now;
                    Settings.Current.Save();
                }
                else
                {
                    _log.ErrorEx($"file could not be generated", _sourceClass);
                }
            }

            StartTimerForTransactionReader();
        }

        #endregion

        #region USB Handler

        private void _usbHandler_DeviceRemovedEvent(object sender, System.EventArgs e)
        {
            _log.InfoEx("Hardware removed", _sourceClass);
        }

        private void _usbHandler_DeviceInsertedEvent(object sender, System.EventArgs e)
        {
            _log.InfoEx("Hardware inserted", _sourceClass);
        }

        #endregion

        #region KeyHandling

        #region ---- GoContinue

        public ICommand GoContinue => _goContinueCommand = new DelegateCommand(GoContinueCommandExecute);

        private void GoContinueCommandExecute()
        {
            _login = new LoginBox(_homeNavigationFrame);
            _login.ViewModel.LoginButtonClicked += ViewModel_LoginButtonClicked;
            _login.Show(_parent);
        }

        void ViewModel_LoginButtonClicked(object sender, LoginBoxViewModel.LoginButtonClickedEventArgs e)
        {
            UserModel user = e.User;

            _log.InfoEx($"manually log in with user '{user.ToString()}'", _sourceClass);

            LoginWithUser(user);
        }

        private void LoginWithUser(UserModel user)
        {
            _log.InfoEx($"Login with user {user}", _sourceClass);

            ProcessFlow.Current.Login(user);
            _homeNavigationFrame.NavigateTo(new MaterialSelection(_homeNavigationFrame, user), TransitionAnimation.LeftToRight);
        }

        #endregion

        #region ---- GoSettings

        private void GoToSettingsCommandExecute()
        {
            _homeNavigationFrame.NavigateTo(new SettingsPage(_homeNavigationFrame), TransitionAnimation.LeftToRight);
        }

        public ICommand GoSettings => _goToSettingsCommand = new DelegateCommand(GoToSettingsCommandExecute, CanExecuteGoToSettingsCommand);

        private bool CanExecuteGoToSettingsCommand()
        {
            return _hasPermission;
        }

        #endregion

        #region ---- Zero
        /// <summary>
        /// Zero
        /// </summary>
        public ICommand GoZero
        {
            get { return new DelegateCommand(DoGoZero); }
        }

        private void DoGoZero()
        {
            if (_scale != null)
            {
                _scale.ZeroAsync();
            }
        }
        #endregion

        #region ---- Tare
        /// <summary>
        /// Tare
        /// </summary>
        public ICommand GoTare
        {
            get { return new DelegateCommand(DoGoTare); }
        }

        private async void DoGoTare()
        {
            if (_scale != null)
            {
                WeightState state = await _scale.TareAsync();
                Console.WriteLine(state);
            }
        }
        #endregion

        #region ---- ClearTare
        /// <summary>
        /// ClearTare
        /// </summary>
        public ICommand GoClearTare
        {
            get { return new DelegateCommand(DoGoClearTare); }
        }

        private void DoGoClearTare()
        {
            if (_scale != null)
            {
                _scale.ClearTareAsync();
            }
        }
        #endregion

        #endregion

        #region Properties

        #region ---- Information
        private string _information = "";
        public string Information
        {
            get
            {
                return _information;
            }
            set
            {
                if (_information != value)
                {
                    _information = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- ProjectNumber
        private string _projectNumber = "";
        public string ProjectNumber
        {
            get
            {
                return _projectNumber;
            }
            set
            {
                if (_projectNumber != value)
                {
                    _projectNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- Version
        private string _version = "";
        public string Version
        {
            get
            {
                return _version;
            }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion
    }
}
