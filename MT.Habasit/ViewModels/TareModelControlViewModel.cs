﻿using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using MT.Habasit.Views;
using MT.Habasit.Logic;
using System.Threading.Tasks;
using MT.Habasit.View;

namespace MT.Habasit.ViewModels
{
    public class TareModelControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public TareModelControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, TareModel tare)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Tare = tare;
        }

        #region Tare
        private TareModel _tare;

        public TareModel Tare
        {
            get { return _tare; }
            set
            {
                if (value != _tare)
                {
                    _tare = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Description
        private string _description;

        public string Description
        {
            get { return $"{Tare.Description} ({Tare.ValueAsString})"; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Buttons

        #region ---- Edit

        public ICommand GoEdit
        {
            get { return new DelegateCommand(DoGoEdit); }
        }

        private void DoGoEdit()
        {
            TareManagement configWindow = new TareManagement(_homeNavigationFrame, Tare, false);
            configWindow.Closed += ConfigWindow_Closed;
            configWindow.Show(_parent);
        }

        private void ConfigWindow_Closed(object sender, EventArgs e)
        {
            OnTareItemChanged(new TareItemChangedEventArgs());
        }

        #endregion

        #region ---- Delete

        public ICommand GoDelete
        {
            get { return new DelegateCommand(DoGoDelete); }
        }

        private void DoGoDelete()
        {
            string title = Localization.Get(Localization.Key.DeleteTare);
            string message = String.Format(Localization.Get(Localization.Key.QuestionDeleteTareX), Tare.ToString());
            
            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Show(_parent);
        }

        private void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            Task.Run(() =>
            {
                var result = DatabaseExtension.DeleteTareFromDBAsync(Tare);

                if (result.Result)
                {
                    OnTareItemChanged(new TareItemChangedEventArgs());
                }
            });
        }

        #endregion

        #endregion

        #region Event TareItemChangedEventArgs

        public event EventHandler<TareItemChangedEventArgs> TareItemChanged;

        protected virtual void OnTareItemChanged(TareItemChangedEventArgs e)
        {
            if (TareItemChanged != null)
                TareItemChanged(this, e);
        }

        public class TareItemChangedEventArgs : EventArgs
        {
            public TareItemChangedEventArgs() { }
        }

        #endregion

        #region Visibility

        #region ---- VisibilityEdit

        private Visibility _visibilityEdit = Visibility.Visible;
        public Visibility VisibilityEdit
        {
            get { return _visibilityEdit; }
            set
            {
                if (_visibilityEdit != value)
                {
                    _visibilityEdit = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- VisibilityDelete

        private Visibility _visibilityDelete = Visibility.Visible;
        public Visibility VisibilityDelete
        {
            get { return _visibilityDelete; }
            set
            {
                if (_visibilityDelete != value)
                {
                    _visibilityDelete = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion
    }
}