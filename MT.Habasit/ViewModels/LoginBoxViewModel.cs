﻿using System;
using System.Runtime.Remoting.Contexts;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Logic;
using MT.Habasit.Models;
using MT.Habasit.Views;
using MT.Habasit.Logic.LegicKeyReader;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Infrastructure;



namespace MT.Habasit.ViewModels
{
    public class LoginBoxViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private DelegateCommand _userCommand;
        private ProcessFlow _currentProcess = ProcessFlow.Current;

        public LoginBoxViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
        }

        private void CloseWindow()
        {
            ((LoginBox)_parent).Close();
        }

        #region Key
        private string _key;

        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                if (value != _key)
                {
                    _key = value;
                    NotifyPropertyChanged();
                    _userCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            OnCancelButtonClicked(new CancelButtonClickedEventArgs());
            CloseWindow();
        }
        #endregion

        #region GoLogin

        public ICommand GoLogin =>
            _userCommand = new DelegateCommand(FindUserCommandExecute, CanExecuteAddSaveUserCommand);

        private bool CanExecuteAddSaveUserCommand()
        {
            bool result = !string.IsNullOrWhiteSpace(Key) == true;
            return result;
        }

        private void FindUserCommandExecute()
        {
            UserModel user = _currentProcess.UserList.Find(u => u.Key == Key);

            if (user == null)
            {
                ShowUserNotFoundMessage();
                return;
            }

            OnLoginButtonClicked(new LoginButtonClickedEventArgs(user));
            CloseWindow();
        }

        private void ShowUserNotFoundMessage()
        {
            string title = Localization.Get(Localization.Key.Login);
            string message = Localization.Get(Localization.Key.UserNotFound);

            Message.ShowErrorMessage(_parent, title, message);
        }

        #endregion

        #region Event LoginButtonClicked

        public event EventHandler<LoginButtonClickedEventArgs> LoginButtonClicked;

        protected virtual void OnLoginButtonClicked(LoginButtonClickedEventArgs e)
        {
            if (LoginButtonClicked != null)
                LoginButtonClicked(this, e);
        }

        public class LoginButtonClickedEventArgs : EventArgs
        {
            public UserModel User { get; set; }

            public LoginButtonClickedEventArgs(UserModel user)
            {
                User = user;
            }
        }

        #endregion

        #region Event CancelButtonClicked

        public event EventHandler<CancelButtonClickedEventArgs> CancelButtonClicked;

        protected virtual void OnCancelButtonClicked(CancelButtonClickedEventArgs e)
        {
            if (CancelButtonClicked != null)
                CancelButtonClicked(this, e);
        }

        public class CancelButtonClickedEventArgs : EventArgs
        {
            public CancelButtonClickedEventArgs()
            {
            }
        }

        #endregion
    }
}
