﻿using MT.Habasit.Logic;
using MT.Habasit.Models;
using MT.Habasit.View;
using MT.Habasit.Views;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Threading.Tasks;

namespace MT.Habasit.ViewModels
{
    public class MaterialModelControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public MaterialModelControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, MaterialModel material)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Material = material;
        }

        #region Material
        private MaterialModel _material;

        public MaterialModel Material
        {
            get { return _material; }
            set
            {
                if (value != _material)
                {
                    _material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
        
        #region Buttons

        #region ---- Edit

        public ICommand GoEdit
        {
            get { return new DelegateCommand(DoGoEdit); }
        }

        private void DoGoEdit()
        {
            MaterialManagement configWindow = new MaterialManagement(_homeNavigationFrame, Material, false);
            configWindow.Closed += ConfigWindow_Closed;
            configWindow.Show(_parent);
        }

        private void ConfigWindow_Closed(object sender, EventArgs e)
        {
            OnMaterialItemChanged(new MaterialItemChangedEventArgs());
        }

        #endregion

        #region ---- Delete

        public ICommand GoDelete
        {
            get { return new DelegateCommand(DoGoDelete); }
        }

        private void DoGoDelete()
        {
            string title = Localization.Get(Localization.Key.DeleteMaterial);
            string message = String.Format(Localization.Get(Localization.Key.QuestionDeleteMaterialX), Material.Type);
            
            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Show(_parent);
        }

        private void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            Task.Run(() =>
            {
                var result = DatabaseExtension.DeleteMaterialFromDBAsync(Material);

                if (result.Result == true)
                {
                    OnMaterialItemChanged(new MaterialItemChangedEventArgs());
                }
                else
                {
                    string title = Localization.Get(Localization.Key.DeleteMaterial);
                    string message = Localization.Get(Localization.Key.DeleteMaterialFailed);

                    Message.ShowErrorMessage(_parent, title, message);
                }
            });
        }

        #endregion

        #endregion

        #region Event MaterialItemChangedEventArgs

        public event EventHandler<MaterialItemChangedEventArgs> MaterialItemChanged;

        protected virtual void OnMaterialItemChanged(MaterialItemChangedEventArgs e)
        {
            if (MaterialItemChanged != null)
                MaterialItemChanged(this, e);
        }

        public class MaterialItemChangedEventArgs : EventArgs
        {
            public MaterialItemChangedEventArgs() { }
        }

        #endregion

        #region Visibility

        #region ---- VisibilityEdit

        private Visibility _visibilityEdit = Visibility.Visible;
        public Visibility VisibilityEdit
        {
            get { return _visibilityEdit; }
            set
            {
                if (_visibilityEdit != value)
                {
                    _visibilityEdit = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- VisibilityDelete

        private Visibility _visibilityDelete = Visibility.Visible;
        public Visibility VisibilityDelete
        {
            get { return _visibilityDelete; }
            set
            {
                if (_visibilityDelete != value)
                {
                    _visibilityDelete = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion
    }
}
