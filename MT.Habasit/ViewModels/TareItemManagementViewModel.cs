﻿using MT.Habasit.Logic;
using MT.Habasit.Models;
using MT.Habasit.View.Controlls;
using MT.Habasit.Views;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

using System.Collections.Generic;

namespace MT.Habasit.ViewModels
{
    public class TareItemManagementViewModel : ItemManagementViewModel
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private ProcessFlow _currentProcess = ProcessFlow.Current;
        private TareManagement _tareManagement;
        private List<TareModel> _tareList;

        public TareItemManagementViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            LoadItems();
        }

        #region Item Overview

        public override void LoadItems()
        {
            ItemList = new List<object>();
            _tareList = _currentProcess.TareList;

            if (_tareList == null)
            {
                return;
            }

            foreach (TareModel tare in _tareList)
            {
                TareModelControl modelControl = new TareModelControl(_homeNavigationFrame, tare);
                modelControl.ViewModel.TareItemChanged += ViewModel_TareItemChanged;
                ItemList.Add(modelControl);
            }

            UpdateTitle();
        }

        private void ViewModel_TareItemChanged(object sender, TareModelControlViewModel.TareItemChangedEventArgs e)
        {
            LoadItems();
        }

        public override void UpdateTitle()
        {
            Title = $"{Localization.Get(Localization.Key.Tarelist)} ({ _tareList.Count})";
        }

        #endregion

        #region Properties
        private string _title;

        public override string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private List<object> _itemList;

        public override List<object> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Key Commands

        public override ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }

        public override ICommand GoNew
        {
            get { return new DelegateCommand(DoGoNew); }
        }

        private void DoGoNew()
        {
            _tareManagement = new TareManagement(_homeNavigationFrame, new TareModel(), true);

            if (_tareManagement!= null)
            {
                _tareManagement.Closed += _tareManagement_Closed;
                _tareManagement.Show(_parent);
            }
        }

        private void _tareManagement_Closed(object sender, System.EventArgs e)
        {
            LoadItems();
        }

        #endregion
    }
}
