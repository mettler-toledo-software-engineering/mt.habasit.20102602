﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MT.Habasit.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.Habasit.Logic;
using MT.Habasit.View.Controlls;
using MT.Habasit.Views;
using MT.Habasit.View;
using MT.Habasit.Logic.BarcodeReader;
using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Composition;
using Timer = System.Timers.Timer;
using MT.Habasit.Logic.LegicKeyReader;

namespace MT.Habasit.ViewModels
{
    public class MaterialSelectionViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(MaterialSelectionViewModel);

        private Timer _timer;
        private ProcessFlow _currentProcess = ProcessFlow.Current;
        private IBarcodeReader _barcodeReader;
        private TransactionModel _transaction;
        private List<OverviewControl> _overviewItem;
        private List<MaterialModel> _materialModels = new List<MaterialModel>();
        private List<List<MaterialModel>> _materialListPages = new List<List<MaterialModel>>();

        private DelegateCommand _pageBackwardsCommand = null;
        private DelegateCommand _pageForwardsCommand = null;
        private DelegateCommand _materialCommand = null;

        public MaterialSelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, UserModel user)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            _transaction = new TransactionModel(user);

            OverviewItem = new List<OverviewControl>();
            OverviewItem.Add(new OverviewControl(homeNavigationFrame, _transaction));

            LoadPages();
        }


        private void LoadPages()
        {
            if (_currentProcess.MaterialList == null)
            {
                return;
            }

            int maxModels = Globals.MaxMaterialsPerPage;
            List<MaterialModel> tempList = _currentProcess.MaterialList.Where(d => d.TimeStamp > DateTime.Parse("1.7.2022")).ToList(); ;

            //   int lastModelIndex = _currentProcess.MaterialList.Count - 1;
            int lastModelIndex = tempList.Count - 1;

            if (lastModelIndex >= 0)
            {
                MaterialModel lastModel = tempList[lastModelIndex];
                List<MaterialModel> modelList = new List<MaterialModel>(maxModels);

                foreach (MaterialModel model in tempList)
                {
                    modelList.Add(model);

                    if (modelList.Count == maxModels || model == lastModel)
                    {
                        _materialListPages.Add(modelList);
                        modelList = new List<MaterialModel>(maxModels);
                    }
                }

                LastPageIndex = _materialListPages.Count - 1;

                LoadCurrentPage();
            }
        }

        private void LoadCurrentPage()
        {
            MaterialButtons = new ObservableCollection<MaterialButtonControl>();

            List<MaterialModel> currentMaterials = _materialListPages[CurrentPageIndex];

            foreach (MaterialModel material in currentMaterials)
            {
                MaterialButtonControl materialButton = new MaterialButtonControl(_homeNavigationFrame, material);
                materialButton.ViewModel.MaterialButtonClicked += ViewModel_MaterialButtonClicked;
                MaterialButtons.Add(materialButton);
            }
        }

        #region RegisterEvents

        public void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
            }

            _log.ErrorEx($"barcode reader on viewmodel NULL", _sourceClass);
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            UnregisterEventsForViewModel();

            _log.InfoEx($"barcode reader on viewmodel data received {e.Data}", _sourceClass);
            string searchCode = e.Data;

            MaterialModel scannedMaterial = _currentProcess.MaterialList.Find(m => m.Code == searchCode);

            if (scannedMaterial == null)
            {
                ShowCodeNotFoundMessage();
                return;
            }

            ViewModel_MaterialButtonClicked(new object(), new MaterialButtonControlViewModel.MaterialButtonClickedEventArgs(scannedMaterial));
        }

        InfoBox _infoBox;

        private void ShowCodeNotFoundMessage()
        {
            string title = Localization.Get(Localization.Key.ScanMaterialCode);
            string message = Localization.Get(Localization.Key.CodeNotFound);
            int interval = Globals.MessageShowIntervalShort;

            _infoBox = new InfoBox(title, message, MessageBoxIcon.Information);
            _infoBox.Show(_parent);

            KeepBoxOpen(interval);
        }

        private void KeepBoxOpen(int seconds)
        {
            _timer = new Timer(seconds);
            _timer.Elapsed += _timer_Elapsed;
            _timer.AutoReset = false;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _infoBox.Close();
            RegisterEventsForViewModel();
        }

        #endregion

        #region UnregisterEvents
        public void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        #endregion

        
        void ViewModel_MaterialButtonClicked(object sender, MaterialButtonControlViewModel.MaterialButtonClickedEventArgs e)
        {
            _log.InfoEx($"material '{e.Material.ToString()}' selected", _sourceClass);
            _transaction.SetMaterial(e.Material);
            _homeNavigationFrame.NavigateTo(new TareSelection(_homeNavigationFrame, _transaction));
        }

        private void ReturnToMainPage()
        {
            _homeNavigationFrame.Back();
        }

        #region OverviewItem
        public List<OverviewControl> OverviewItem
        {
            get { return _overviewItem; }
            set
            {
                if (value != _overviewItem)
                {
                    _overviewItem = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region MaterialButtons
        private ObservableCollection<MaterialButtonControl> _materialButtons;

        public ObservableCollection<MaterialButtonControl> MaterialButtons
        {
            get
            {
                return _materialButtons;
            }
            set
            {
                if (value != _materialButtons)
                {
                    _materialButtons = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region CurrentPageIndex
        private int _currentPageIndex;

        public int CurrentPageIndex
        {
            get
            {
                return _currentPageIndex;
            }
            set
            {
                if (value != _currentPageIndex)
                {
                    _currentPageIndex = value;
                    NotifyPropertyChanged();

                    _pageBackwardsCommand?.NotifyCanExecuteChanged();
                    _pageForwardsCommand?.NotifyCanExecuteChanged();

                    LoadCurrentPage();
                }
            }
        }
        #endregion

        #region LastPageIndex
        private int _lastPageIndex;

        public int LastPageIndex
        {
            get
            {
                return _lastPageIndex;
            }
            set
            {
                if (value != _lastPageIndex)
                {
                    _lastPageIndex = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Cancel
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            string title = Localization.Get(Localization.Key.CancelProcess);
            string message = Localization.Get(Localization.Key.QuestionCancelProcess);

            QuestionBox question = new QuestionBox(title, message);
            question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
            question.Show(_parent);
        }

        void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            _log.Info("Logout!");
            ProcessFlow.Current.Logout();
            ReturnToMainPage();
        }
        #endregion

        #region GoPageBackwards
        public ICommand GoPageBackwards =>
            _pageBackwardsCommand = new DelegateCommand(PageBackwardsCommandExecute, CanExecutePageBackwardsCommand);

        private bool CanExecutePageBackwardsCommand()
        {
            return CurrentPageIndex != 0;
        }

        private void PageBackwardsCommandExecute()
        {
            CurrentPageIndex--;
        }
        #endregion

        #region GoPageForwards
        public ICommand GoPageForwards =>
            _pageForwardsCommand = new DelegateCommand(PageForwardsCommandExecute, CanExecutePageForwardsCommand);

        private bool CanExecutePageForwardsCommand()
        {
            return CurrentPageIndex < LastPageIndex;
        }

        private void PageForwardsCommandExecute()
        {
            CurrentPageIndex++;
        }
        #endregion
    }
}
