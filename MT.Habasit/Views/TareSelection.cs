﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for TareSelection
    /// </summary>
    public partial class TareSelection
    {
        private readonly TareSelectionViewModel _viewModel;

        public TareSelection(AnimatedNavigationFrame homeNavigationFrame, TransactionModel transaction)
        {
            _viewModel = new TareSelectionViewModel(this, homeNavigationFrame, transaction);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _viewModel.RegisterBarcodeReader();
            _viewModel.RegisterEventsForViewModel();

            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);

            _viewModel.UnregisterEventsForViewModel();

            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);

            _viewModel.RegisterBarcodeReader();
            _viewModel.RegisterEventsForViewModel();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.UnregisterEventsForViewModel();

            return base.OnNavigatingBack(nextPage);
        }

        #endregion 
    }
}
