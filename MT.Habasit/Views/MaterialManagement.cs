﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for MaterialManagement
    /// </summary>
    public partial class MaterialManagement
    {
        private MaterialManagementViewModel _viewModel;
        
        public MaterialManagement(AnimatedNavigationFrame homeNavigationFrame, MaterialModel material, bool isNew)
        {
            _viewModel = new MaterialManagementViewModel(this, homeNavigationFrame, material, isNew);
            InitializeComponents();
        }
    }
}
