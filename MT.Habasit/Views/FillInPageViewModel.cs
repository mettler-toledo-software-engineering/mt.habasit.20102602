﻿using Marenco.ModelsAndController;
using MT.Balancer.Controls;
using MT.Balancer.Controls.TrimWeightControl;
using MT.Balancer.Logic;
using MT.Balancer.ModelsAndController;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MT.Balancer.Pages
{
    public partial class FillInPageViewModel : PropertyChangedBase
    {

        public int ThisStep = 160;
        private Visual Parent;
        private AnimatedNavigationFrame _homeNavigationFrame;
        public Spinner spinner = new Spinner();
        public bool enableContinue = false;

        public MiniScalePanel[] MiniScalePanels = new MiniScalePanel[3];

        public TrimWeightViewControl trimWeightViewControl;

        public CalcControl ResultControlSM;
        public CalcControl ResultControlCG;
        public CalcControl ResultControlWG;

        public FillInPageViewModel(Visual Parent, AnimatedNavigationFrame _homeNavigationFrame)
        {
            this.Parent = Parent;
            this._homeNavigationFrame = _homeNavigationFrame;

            InitCGControlView();
            InitSMControlView();
            InitWGControlView();


            SoftKeyContinueIndex = 11;
            NumberOfSoftkeys = 7;
            NumberOfSoftkeys = 8;
            MultiScalePanel.Instance.viewModel.WeightChanged += viewModel_WeightChanged;

            PreferredSelection();
        }


        void PreferredSelection()
        {
            switch ((PreferredTrims)Globals.Instance.calculationController.rbm.PreferredTrim)
            {
                case PreferredTrims.None:
                    ResultControlSM.viewModel.Selected = false;
                    ResultControlWG.viewModel.Selected = false;
                    ResultControlCG.viewModel.Selected = false;
                    break;
                case PreferredTrims.StaticMoment:
                    ResultControlSM.viewModel.Selected = true;
                    ResultControlWG.viewModel.Selected = false;
                    ResultControlCG.viewModel.Selected = false;
                    break;
                case PreferredTrims.CenterOfGravity:
                    ResultControlSM.viewModel.Selected = false;
                    ResultControlWG.viewModel.Selected = false;
                    ResultControlCG.viewModel.Selected = true;
                    break;
                case PreferredTrims.Weight:
                    ResultControlSM.viewModel.Selected = false;
                    ResultControlWG.viewModel.Selected = true;
                    ResultControlCG.viewModel.Selected = false;
                    break;
                default:
                    break;
            }
        }
        void viewModel_WeightChanged(object sender, MultiScalePanelViewModel.WeightChangedEventArgs e)
        {
            ResultControlWG.viewModel.DeltaTracValue = e.SumWeight + Globals.Instance.TrimResultModel.Weight1Coverplate + Globals.Instance.TrimResultModel.Weight2Coverplate + Globals.Instance.TrimResultModel.Weight3Coverplate;
                    
        }


        private void InitCGControlView()
        {
            ResultControlCG = new CalcControl();
            ResultControlCG.viewModel.Title = "Best CG";
            ResultControlCG.viewModel.PromptLine1 = "Target CG:";
            ResultControlCG.viewModel.PromptLine2 = "Current CG:";
            ResultControlCG.viewModel.PromptLine3 = "Deviation:";


            ResultControlCG.viewModel.ValueLine1 = Globals.Instance.calculationController.rbm.TargetCG.ToString("0.0000 m");

            ResultControlCG.viewModel.DeltaTracTargetPositionInPercent = 50;
            ResultControlCG.viewModel.DeltaTracTargetValue = Globals.Instance.calculationController.rbm.TargetCG;
            ResultControlCG.viewModel.ButtonVisibility = Visibility.Collapsed;
        }

        private void InitWGControlView()
        {
            ResultControlWG = new CalcControl();
            ResultControlWG.viewModel.Title = "Best weight";
            ResultControlWG.viewModel.PromptLine1 = "Target weight:";
            ResultControlWG.viewModel.PromptLine2 = "Current weight:";
            ResultControlWG.viewModel.PromptLine3 = "Deviation:";


            ResultControlWG.viewModel.ValueLine1 = Globals.Instance.calculationController.rbm.TargetWeight.ToString("0.0000 kg");

            ResultControlWG.viewModel.DeltaTracTargetPositionInPercent = 50;
            ResultControlWG.viewModel.DeltaTracTargetValue = Globals.Instance.calculationController.rbm.TargetWeight;
            ResultControlWG.viewModel.ButtonVisibility = Visibility.Collapsed;
        }

        private void InitSMControlView()
        {
            ResultControlSM = new CalcControl();
            ResultControlSM.viewModel.Title = "Best SM";
            ResultControlSM.viewModel.PromptLine1 = "Target SM:";
            ResultControlSM.viewModel.PromptLine2 = "Current SM:";
            ResultControlSM.viewModel.PromptLine3 = "Tolerance:";
            ResultControlSM.viewModel.PromptLine4 = "Deviation:";


            ResultControlSM.viewModel.DeltaTracTargetPositionInPercent = 50;

            ResultControlSM.viewModel.DeltaTracLowerToleranceAbs = Globals.Instance.calculationController.rbm.TargetSM * Globals.Instance.calculationController.rbm.Tolerance / 100;
            ResultControlSM.viewModel.DeltaTracUpperToleranceAbs = Globals.Instance.calculationController.rbm.TargetSM * Globals.Instance.calculationController.rbm.Tolerance / 100;
            ResultControlSM.viewModel.DeltaTracMinValue = Globals.Instance.calculationController.rbm.TargetSM - 1.2 * Globals.Instance.calculationController.rbm.TargetSM * Globals.Instance.calculationController.rbm.Tolerance / 100;
            ResultControlSM.viewModel.DeltaTracTargetValue = Globals.Instance.calculationController.rbm.TargetSM;
            ResultControlSM.viewModel.ValueLine1 = Globals.Instance.calculationController.rbm.TargetSM.ToString("0.0000 kgm");
            ResultControlSM.viewModel.ValueLine3 = "+/-" + Globals.Instance.calculationController.rbm.Tolerance.ToString("0.000000") + " %";

            ResultControlSM.viewModel.ButtonVisibility = Visibility.Collapsed;
        }

        public void SetupTrimViewControl()
        {

            trimWeightViewControl = new TrimWeightViewControl(Globals.Instance.calculationController.rbm.TrimPosController);
            Debug.WriteLine("SM after trim: " + Globals.Instance.TrimResultModel.TrimmedSM.ToString() + "Diff to Target: " + (Globals.Instance.TrimResultModel.TrimmedSM - Globals.Instance.calculationController.rbm.TargetSM).ToString("0.00000"));

            UpdateSMControlView();
            UpdateCGControlView();
            UpdateWGControlView();

            TrimWeightPanelVisibility = Visibility.Visible;
            SoftKeyContinueIndex = 7;
            NumberOfSoftkeys = 7;
            NumberOfSoftkeys = 8;
            OnNewTrimWeightViewControl(EventArgs.Empty);
        }

        private void UpdateSMControlView()
        {
            ResultControlSM.viewModel.ValueLine2 = Globals.Instance.TrimResultModel.TrimmedSM.ToString("0.0000 kgm");

            double diff = Globals.Instance.TrimResultModel.TrimmedSM - Globals.Instance.calculationController.rbm.TargetSM;
            ResultControlSM.viewModel.ValueLine4 = (diff / Globals.Instance.calculationController.rbm.TargetSM * 100).ToString("0.000000") + " %";
            ResultControlSM.viewModel.Show(Visibility.Visible);
            ResultControlSM.viewModel.DeltaTracValue = Globals.Instance.TrimResultModel.TrimmedSM;
        }
        private void UpdateWGControlView()
        {
            double diff = Globals.Instance.TrimResultModel.TrimmedWeight - Globals.Instance.calculationController.rbm.TargetWeight;
            ResultControlWG.viewModel.DeltaTracMinValue = Globals.Instance.calculationController.rbm.WeightBeforeTrim;
            ResultControlWG.viewModel.DeltaTracLowerToleranceAbs = 0;
            ResultControlWG.viewModel.DeltaTracUpperToleranceAbs = 0;
            ResultControlWG.viewModel.ValueLine1 = Globals.Instance.calculationController.rbm.TargetWeight.ToString("0.0000 kg");
            ResultControlWG.viewModel.ValueLine2 = Globals.Instance.TrimResultModel.TrimmedWeight.ToString("0.0000 kg");
            ResultControlWG.viewModel.ValueLine3 = diff.ToString("0.0000 kg");
            ResultControlWG.viewModel.Show(Visibility.Visible);
            ResultControlWG.viewModel.DeltaTracValue = Globals.Instance.TrimResultModel.TrimmedWeight;
        }
        private void UpdateCGControlView()
        {
            double diff = Globals.Instance.TrimResultModel.TrimmedCG_X - Globals.Instance.calculationController.rbm.TargetCG;
            ResultControlCG.viewModel.DeltaTracMinValue = Globals.Instance.calculationController.rbm.CGBeforeTrim_X;
            ResultControlCG.viewModel.DeltaTracLowerToleranceAbs = 0;
            ResultControlCG.viewModel.DeltaTracUpperToleranceAbs = 0;
            ResultControlCG.viewModel.ValueLine1 = Globals.Instance.calculationController.rbm.TargetCG.ToString("0.0000 m");
            ResultControlCG.viewModel.ValueLine2 = Globals.Instance.TrimResultModel.TrimmedCG_X.ToString("0.0000 m");
            ResultControlCG.viewModel.ValueLine3 = diff.ToString("0.0000 m");
            ResultControlCG.viewModel.Show(Visibility.Visible);
            ResultControlCG.viewModel.DeltaTracValue = Globals.Instance.TrimResultModel.TrimmedCG_X;
        }

        private PreferredTrims selectedItem;
        public PreferredTrims SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                if (selectedItem != value)
                    selectedItem = value;
            }
        }




        string GetPreferredText(PreferredTrims prefferdTrimVariant)
        {
            string s = "-";
            switch (prefferdTrimVariant)
            {
                case PreferredTrims.None:
                    s = "-";
                    break;
                case PreferredTrims.StaticMoment:
                    s = Localization.Get(Localization.Key.BestSM);
                    break;
                case PreferredTrims.CenterOfGravity:
                    s = Localization.Get(Localization.Key.BestSMToCG);
                    break;
                case PreferredTrims.Weight:
                    s = Localization.Get(Localization.Key.BestSMToWeight);
                    break;
                default:
                    break;
            }

            return s;
        }



        public void InitializeScales()
        {
            //MiniScalePanels[0] = new MiniScalePanel(1, HomeScreenViewModel.Instance.scales[Globals.Scale_1_Assign]);
            //MiniScalePanels[1] = new MiniScalePanel(2, HomeScreenViewModel.Instance.scales[Globals.Scale_2_Assign]);
            //MiniScalePanels[2] = new MiniScalePanel(3, HomeScreenViewModel.Instance.scales[Globals.Scale_3_Assign]);
            MiniScalePanels[0] = new MiniScalePanel(1, HomeScreenViewModel.Instance.scales[Globals.Scale_1_Assign], Globals.Instance.ScalePositionModelList[0].PosX, Globals.Instance.ScalePositionModelList[0].PosY);
            MiniScalePanels[1] = new MiniScalePanel(2, HomeScreenViewModel.Instance.scales[Globals.Scale_2_Assign], Globals.Instance.ScalePositionModelList[1].PosX, Globals.Instance.ScalePositionModelList[1].PosY);
            MiniScalePanels[2] = new MiniScalePanel(3, HomeScreenViewModel.Instance.scales[Globals.Scale_3_Assign], Globals.Instance.ScalePositionModelList[2].PosX, Globals.Instance.ScalePositionModelList[2].PosY);
        }



        public void KeyLeft()
        {
            _homeNavigationFrame.Back();
        }

        public void KeyContinue()
        {

            HintPage hintPage = new HintPage(_homeNavigationFrame, 150, 159);
            hintPage.viewModel.EndReached += viewModel_EndReached;
            _homeNavigationFrame.NavigateTo(hintPage);
        }

        void viewModel_EndReached(object sender, EventArgs e)
        {
            _homeNavigationFrame.NavigateTo(new RotorBladeGrossWeighingPage(_homeNavigationFrame));
        }

        public void Calculate()
        {
            SetupTrimViewControl();
        }


        #region Properties
        private int numberOfSoftkeys;

        public int NumberOfSoftkeys
        {
            get { return numberOfSoftkeys; }
            set
            {
                if (numberOfSoftkeys != value)
                {
                    numberOfSoftkeys = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int softKeyContinueIndex;

        public int SoftKeyContinueIndex
        {
            get { return softKeyContinueIndex; }
            set
            {
                if (softKeyContinueIndex != value)
                {
                    softKeyContinueIndex = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility spinnerVisibility;

        public Visibility SpinnerVisibility
        {
            get { return spinnerVisibility; }
            set
            {
                if (spinnerVisibility != value)
                {
                    spinnerVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility trimWeightPanelVisibility;

        public Visibility TrimWeightPanelVisibility
        {
            get { return trimWeightPanelVisibility; }
            set
            {
                if (trimWeightPanelVisibility != value)
                {
                    trimWeightPanelVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Object _imageSource;
        public Object ImageSource
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Color resultForgroundColor;

        public Color ResultForgroundColor
        {
            get { return resultForgroundColor; }
            set
            {
                if (resultForgroundColor != value)
                {
                    resultForgroundColor = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _hintText;

        public string HintText
        {
            get { return _hintText; }
            set
            {
                if (_hintText != value)
                {
                    _hintText = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string tareResultText;

        public string TareResultText
        {
            get { return tareResultText; }
            set
            {
                if (tareResultText != value)
                {
                    tareResultText = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ShowHelp
        public void ShowInfo()
        {
            PDFPopup pdfPopup = new PDFPopup(Globals.Instance.GetStep(ThisStep).PDF);
            pdfPopup.PopupResult += pdfPopup_PopupResult;
            pdfPopup.Show(Parent);
        }

        void pdfPopup_PopupResult(object sender, PDFPopup.PopupResultEventArgs e)
        {
            if (e.e.ReadResponse != RemoteFileController.ReadResponse.SuccessfullyDone)
            {
                MessageBox.Show(Parent, e.e.ResultText, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        /// <summary>
        /// Occures when new TrimWeightViewControl
        /// </summary>
        public event EventHandler NewTrimWeightViewControl;

        /// <summary>
        /// Raises the <see cref="NewTrimWeightViewControl"/> event
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnNewTrimWeightViewControl(EventArgs e)
        {
            if (NewTrimWeightViewControl != null)
                NewTrimWeightViewControl(this, e);
        }



    }
}
