﻿using MT.Habasit.Logic.LegicKeyReader;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for Settings
    /// </summary>
    public partial class SettingsPage
    {
        private readonly SettingsPageViewModel _viewModel;

        public SettingsPage(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new SettingsPageViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }

        #endregion 
    }
}
