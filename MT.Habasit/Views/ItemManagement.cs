﻿using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for ItemManagement
    /// </summary>
    public partial class ItemManagement
    {
        public readonly ItemManagementViewModel ViewModel;
        
        public ItemManagement(AnimatedNavigationFrame homeNavigationFrame, TareModel model)
        {
            ViewModel = new TareItemManagementViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        public ItemManagement(AnimatedNavigationFrame homeNavigationFrame, MaterialModel model)
        {
            ViewModel = new MaterialItemManagementViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        public ItemManagement(AnimatedNavigationFrame homeNavigationFrame, UserModel model)
        {
            ViewModel = new UserItemManagementViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
