﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using System;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for UserManagement
    /// </summary>
    public partial class UserManagement
    {
        public UserManagementViewModel ViewModel;

        public UserManagement(AnimatedNavigationFrame homeNavigationFrame, UserModel user, bool isNew)
        {
            ViewModel = new UserManagementViewModel(this, homeNavigationFrame, user, isNew);
            InitializeComponents();
        }
    }
}
