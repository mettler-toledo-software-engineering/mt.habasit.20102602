﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View.Controlls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class MaterialModelControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.Button internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.Button internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.FontSize = ((System.Nullable<System.Int32>)28);
            internal3.Width = 280;
            internal3.Text = "567678";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() => ViewModel.Material.Code,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal5.Width = 630;
            internal5.FontSize = ((System.Nullable<System.Int32>)28);
            internal5.Text = "Extruder Recycling Abfall TPU";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => ViewModel.Material.Type,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(40, 5, 0, 0);
            internal6.Width = 630;
            internal6.FontSize = ((System.Nullable<System.Int32>)24);
            internal6.Text = "P1200 / 2400 / 4000";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => ViewModel.Material.System,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal8.FontSize = ((System.Nullable<System.Int32>)18);
            internal8.Text = "Letzte Änderung:";
            internal8.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LastChanged);
            internal8.AddTranslationAction(() => {
                internal8.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LastChanged);
            });
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal9.FontSize = ((System.Nullable<System.Int32>)18);
            internal9.Text = "25.11.2020 15:14";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => ViewModel.Material.LastChanged,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8, internal9);
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6, internal7);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.Habasit/MT.Habasit.Images.Edit.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal10 = new MT.Singularity.Presentation.Controls.Button();
            internal10.Content = internal11;
            internal10.Height = 60;
            internal10.Width = 80;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(30, 0, 0, 0);
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Command,() => ViewModel.GoEdit,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Visibility,() => ViewModel.VisibilityEdit,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://MT.Habasit/MT.Habasit.Images.Delete.al8";
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12 = new MT.Singularity.Presentation.Controls.Button();
            internal12.Content = internal13;
            internal12.Height = 60;
            internal12.Width = 80;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Command,() => ViewModel.GoDelete,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Visibility,() => ViewModel.VisibilityDelete,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal10, internal12);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 10, 0);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293258224u));
            internal1.Width = 1200;
            internal1.Height = 100;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[8];
    }
}
