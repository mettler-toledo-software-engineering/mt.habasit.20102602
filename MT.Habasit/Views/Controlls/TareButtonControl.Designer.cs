﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View.Controlls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class TareButtonControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Button internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.FontSize = ((System.Nullable<System.Int32>)24);
            internal5.Text = "20.0 kg";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => ViewModel.Tare.ValueAsString,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 5);
            internal7.FontSize = ((System.Nullable<System.Int32>)16);
            internal7.Text = "Fix-Tara";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => ViewModel.Tare.Code,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.FontSize = ((System.Nullable<System.Int32>)16);
            internal8.Text = "654978";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Text,() => ViewModel.Tare.Description,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal8);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal6);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(internal3);
            internal1 = new MT.Singularity.Presentation.Controls.Button();
            internal1.Content = internal2;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal1.Width = 240;
            internal1.Height = 90;
            internal1.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal1.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Command,() => ViewModel.GoSelect,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.Content = internal1;
            this.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 5, 0);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
