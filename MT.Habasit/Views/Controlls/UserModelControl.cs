﻿using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views.Controlls
{
    /// <summary>
    /// Interaction logic for UserModelControl
    /// </summary>
    public partial class UserModelControl
    {
        public readonly UserModelControlViewModel ViewModel;

        public UserModelControl(AnimatedNavigationFrame homeNavigationFrame, UserModel user)
        {
            ViewModel = new UserModelControlViewModel(this, homeNavigationFrame, user);
            InitializeComponents();
        }
    }
}
