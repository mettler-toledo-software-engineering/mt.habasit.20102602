﻿using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.View.Controlls
{
    /// <summary>
    /// Interaction logic for MaterialModelControl
    /// </summary>
    public partial class MaterialModelControl
    {
        public readonly MaterialModelControlViewModel ViewModel;

        public MaterialModelControl(AnimatedNavigationFrame homeNavigationFrame, MaterialModel material)
        {
            ViewModel = new MaterialModelControlViewModel(this, homeNavigationFrame, material);
            InitializeComponents();
        }
    }
}
