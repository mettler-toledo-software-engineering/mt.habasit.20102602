﻿using MT.Singularity.Presentation.Controls;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.View.Controlls
{
    /// <summary>
    /// Interaction logic for MaterialButtonControl
    /// </summary>
    public partial class MaterialButtonControl
    {
        public readonly MaterialButtonControlViewModel ViewModel;

        public MaterialButtonControl(AnimatedContentControl homeNavigationFrame, MaterialModel material)
        {
            ViewModel = new MaterialButtonControlViewModel(this, homeNavigationFrame, material);
            InitializeComponents();
        }
    }
}
