﻿using MT.Singularity.Presentation.Controls;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.View.Controlls
{
    /// <summary>
    /// Interaction logic for TareButtonControl
    /// </summary>
    public partial class TareButtonControl
    {
        public readonly TareButtonControlViewModel ViewModel;

        public TareButtonControl(AnimatedContentControl homeNavigationFrame, TareModel tare)
        {
            ViewModel = new TareButtonControlViewModel(this, homeNavigationFrame, tare);
            InitializeComponents();
        }
    }
}
