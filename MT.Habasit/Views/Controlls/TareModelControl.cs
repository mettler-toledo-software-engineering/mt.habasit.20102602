﻿using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.View.Controlls
{
    /// <summary>
    /// Interaction logic for TareModelControl
    /// </summary>
    public partial class TareModelControl
    {
        public readonly TareModelControlViewModel ViewModel;

        public TareModelControl(AnimatedNavigationFrame homeNavigationFrame, TareModel tare)
        {
            ViewModel = new TareModelControlViewModel(this, homeNavigationFrame, tare);
            InitializeComponents();
        }
    }
}
