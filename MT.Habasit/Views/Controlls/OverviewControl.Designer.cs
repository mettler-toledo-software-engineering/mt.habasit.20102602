﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View.Controlls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class OverviewControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.FontSize = ((System.Nullable<System.Int32>)18);
            internal3.Text = "Benutzer:";
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal4.FontSize = ((System.Nullable<System.Int32>)18);
            internal4.Text = "merturi-1";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() => _viewModel.Transaction.User.Key,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.Width = 425;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(50, 6, 0, 6);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.FontSize = ((System.Nullable<System.Int32>)18);
            internal6.Text = "Abfall-Code:";
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal7.FontSize = ((System.Nullable<System.Int32>)18);
            internal7.Text = "123456789";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => _viewModel.Transaction.Material.Code,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Width = 425;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 6, 0, 6);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.FontSize = ((System.Nullable<System.Int32>)18);
            internal9.Text = "Tara:";
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal10.FontSize = ((System.Nullable<System.Int32>)18);
            internal10.Text = "23.87 kg";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() => _viewModel.Transaction.Tare.ValueAsString,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10);
            internal8.Width = 425;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 6, 0, 6);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal5, internal8);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4286553013u));
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.Content = internal1;
            this.Width = 1280;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
