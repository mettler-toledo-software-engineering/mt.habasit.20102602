﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View.Controlls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class MaterialButtonControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Button internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.FontSize = ((System.Nullable<System.Int32>)18);
            internal5.Text = "Extruder Recycling Abfall TPU";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => ViewModel.Material.Type,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.FontSize = ((System.Nullable<System.Int32>)18);
            internal7.Text = "P1200 / 2400 / 4000";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => ViewModel.Material.System,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.FontSize = ((System.Nullable<System.Int32>)14);
            internal9.Text = "6611";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => ViewModel.Material.Code,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9);
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal6, internal8);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(internal3);
            internal1 = new MT.Singularity.Presentation.Controls.Button();
            internal1.Content = internal2;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(6);
            internal1.Width = 240;
            internal1.Height = 85;
            internal1.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal1.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Command,() => ViewModel.GoSelect,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.Content = internal1;
            this.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 5, 0);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
