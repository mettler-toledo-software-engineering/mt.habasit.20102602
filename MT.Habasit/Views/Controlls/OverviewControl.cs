﻿using MT.Singularity.Presentation.Controls;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.View.Controlls
{
    /// <summary>
    /// Interaction logic for OverviewControl
    /// </summary>
    public partial class OverviewControl
    {
        public readonly OverviewControlViewModel _viewModel;

        public OverviewControl(AnimatedContentControl homeNavigationFrame, TransactionModel transaction)
        {
            _viewModel = new OverviewControlViewModel(this, homeNavigationFrame, transaction);
            InitializeComponents();
        }
    }
}
