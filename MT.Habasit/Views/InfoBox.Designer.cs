﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class InfoBox : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.Image internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.Button internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.TextBlock internal15;
            MT.Singularity.Presentation.Controls.Button internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            internal4 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Visibility,() =>  ViewModel.QuestionSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4.Source = "embedded://MT.Habasit/MT.Habasit.Images.question.png";
            internal4.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Visibility,() =>  ViewModel.WarnSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.Source = "embedded://MT.Habasit/MT.Habasit.Images.Warning.png";
            internal5.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Visibility,() =>  ViewModel.ErrorSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Source = "embedded://MT.Habasit/MT.Habasit.Images.Error.png";
            internal6.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Visibility,() =>  ViewModel.InfoSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7.Source = "embedded://MT.Habasit/MT.Habasit.Images.Info.png";
            internal7.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal5, internal6, internal7);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() =>  ViewModel.Title,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9.FontSize = ((System.Nullable<System.Int32>)28);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  ViewModel.Information,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.FontSize = ((System.Nullable<System.Int32>)22);
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Yes);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Yes);
            });
            internal13.FontSize = ((System.Nullable<System.Int32>)22);
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12 = new MT.Singularity.Presentation.Controls.Button();
            internal12.Content = internal13;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12.Width = 180;
            internal12.Height = 60;
            internal12.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Visibility,() =>  ViewModel.ButtonYesVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Command,() =>  ViewModel.GoYes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal15.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.No);
            internal15.AddTranslationAction(() => {
                internal15.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.No);
            });
            internal15.FontSize = ((System.Nullable<System.Int32>)22);
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Content = internal15;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14.Width = 180;
            internal14.Height = 60;
            internal14.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Visibility,() =>  ViewModel.ButtonNoVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() =>  ViewModel.GoNo,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Ok);
            internal17.AddTranslationAction(() => {
                internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Ok);
            });
            internal17.FontSize = ((System.Nullable<System.Int32>)22);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16 = new MT.Singularity.Presentation.Controls.Button();
            internal16.Content = internal17;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16.Width = 180;
            internal16.Height = 60;
            internal16.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Visibility,() =>  ViewModel.ButtonOkVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Command,() =>  ViewModel.GoOk,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal14, internal16);
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10, internal11);
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 40, 0);
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal8);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[12];
    }
}
