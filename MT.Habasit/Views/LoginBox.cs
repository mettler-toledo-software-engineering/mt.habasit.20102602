﻿using MT.Habasit.Logic.LegicKeyReader;
using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for LoginBox
    /// </summary>
    public partial class LoginBox
    {
        public LoginBoxViewModel ViewModel;

        public LoginBox(AnimatedNavigationFrame homeNavigationFrame)
        {
            ViewModel = new LoginBoxViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
