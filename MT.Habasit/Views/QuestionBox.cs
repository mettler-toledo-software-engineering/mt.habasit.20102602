﻿using MT.Habasit.ViewModels;

namespace MT.Habasit.View
{
    /// <summary>
    /// Interaction logic for UserManagement
    /// </summary>
    public partial class QuestionBox
    {
        public readonly QuestionBoxViewModel ViewModel;

        public QuestionBox(string title, string message)
        {
            ViewModel = new QuestionBoxViewModel(this,title, message);
            InitializeComponents();
        }
    }
}
