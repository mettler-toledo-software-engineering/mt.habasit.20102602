﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;
using MT.Habasit.Logic.LegicKeyReader;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for MaterialSelection
    /// </summary>
    public partial class MaterialSelection
    {
        private readonly MaterialSelectionViewModel _viewModel;

        public MaterialSelection(AnimatedNavigationFrame homeNavigationFrame, UserModel user)
        {
           _viewModel = new MaterialSelectionViewModel(this, homeNavigationFrame, user);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _viewModel.RegisterBarcodeReader();
            _viewModel.RegisterEventsForViewModel();

            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);

            _viewModel.UnregisterEventsForViewModel();

            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);

            _viewModel.RegisterBarcodeReader();
            _viewModel.RegisterEventsForViewModel();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.UnregisterEventsForViewModel();
         //   _legicKeyReader.StartPoll();

            return base.OnNavigatingBack(nextPage);
        }

        #endregion 
    }
}
