﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class QuestionBox : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.Button internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 0);
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5.FontSize = ((System.Nullable<System.Int32>)24);
            internal5.Text = "Benutzer löschen";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => ViewModel.Title,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Source = "embedded://MT.Habasit/MT.Habasit.Images.question.png";
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal6);
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 20, 0);
            internal7.FontSize = ((System.Nullable<System.Int32>)30);
            internal7.Text = "Möchten Sie wirklich den Benutzer 'merturi-1' \naus der Datenbank entfernen?";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => ViewModel.Message,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal7);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal9.Width = 180;
            internal9.Height = 80;
            internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.Habasit/MT.Habasit.Images.Cancel.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.No);
            internal12.AddTranslationAction(() => {
                internal12.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.No);
            });
            internal12.FontSize = ((System.Nullable<System.Int32>)20);
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal12);
            internal10.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9.Content = internal10;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Command,() => ViewModel.GoNo,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.Button();
            internal13.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal13.Width = 180;
            internal13.Height = 80;
            internal13.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal13.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://MT.Habasit/MT.Habasit.Images.ok.al8";
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Yes);
            internal16.AddTranslationAction(() => {
                internal16.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Yes);
            });
            internal16.FontSize = ((System.Nullable<System.Int32>)20);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal16);
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13.Content = internal14;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Command,() => ViewModel.GoYes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal13);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 60, 0, 20);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal8);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
