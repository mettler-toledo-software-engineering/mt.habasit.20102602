﻿using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls;

namespace MT.Habasit.View
{
    /// <summary>
    /// Interaction logic for InfoBox
    /// </summary>
    public partial class InfoBox
    {
        public readonly InfoBoxView ViewModel;

        public InfoBox(string title, string message)
        {
            ViewModel = new InfoBoxView(this, title, message);

            InitializeComponents();
        }

        public InfoBox(string title, string message, MessageBoxIcon icon)
        {
            ViewModel = new InfoBoxView(this, title, message, icon);

            InitializeComponents();
        }

        public InfoBox(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            ViewModel = new InfoBoxView(this, title, message, icon, buttons);

            InitializeComponents();
        }
    }
}
