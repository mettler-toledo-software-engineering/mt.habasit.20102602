﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
namespace MT.Habasit.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class ItemManagement : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.ListBox OverviewList;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.DockPanel internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.GroupPanel internal15;
            MT.Singularity.Presentation.Controls.Image internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal4.FontSize = ((System.Nullable<System.Int32>)40);
            internal4.Text = "Benutzerliste";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  ViewModel.Title,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            OverviewList = new MT.Singularity.Presentation.Controls.ListBox();
            OverviewList.Margin = new MT.Singularity.Presentation.Thickness(10);
            OverviewList.Width = 1200;
            OverviewList.Height = 500;
            OverviewList.BorderBrush = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            OverviewList.ScrollVertical = true;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => OverviewList.ItemsSource,() =>  ViewModel.ItemList,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(OverviewList);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3, internal5);
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9.Width = 180;
            internal9.Height = 90;
            internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.Habasit/MT.Habasit.Images.ArrowLeft.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Back);
            internal12.AddTranslationAction(() => {
                internal12.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Back);
            });
            internal12.FontSize = ((System.Nullable<System.Int32>)20);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal12);
            internal9.Content = internal10;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Command,() => ViewModel.GoBack,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14.Width = 180;
            internal14.Height = 90;
            internal14.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal14.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal16 = new MT.Singularity.Presentation.Controls.Image();
            internal16.Source = "embedded://MT.Habasit/MT.Habasit.Images.Add.al8";
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.New);
            internal17.AddTranslationAction(() => {
                internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.New);
            });
            internal17.FontSize = ((System.Nullable<System.Int32>)20);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal15 = new MT.Singularity.Presentation.Controls.GroupPanel(internal16, internal17);
            internal14.Content = internal15;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() => ViewModel.GoNew,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(internal14);
            internal13.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8, internal13);
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal7.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal6 = new MT.Singularity.Presentation.Controls.DockPanel(internal7);
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal6);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            this.Content = internal1;
            this.Width = 1280;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
