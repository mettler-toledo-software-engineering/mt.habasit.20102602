﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class ManualTare : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.TextBox internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.StackPanel internal15;
            MT.Singularity.Presentation.Controls.Button internal16;
            MT.Singularity.Presentation.Controls.GroupPanel internal17;
            MT.Singularity.Presentation.Controls.Image internal18;
            MT.Singularity.Presentation.Controls.TextBlock internal19;
            MT.Singularity.Presentation.Controls.Button internal20;
            MT.Singularity.Presentation.Controls.GroupPanel internal21;
            MT.Singularity.Presentation.Controls.Image internal22;
            MT.Singularity.Presentation.Controls.TextBlock internal23;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 0);
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5.FontSize = ((System.Nullable<System.Int32>)30);
            internal5.Text = "Manuelles Tara eingeben";
            internal5.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.InputManualTare);
            internal5.AddTranslationAction(() => {
                internal5.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.InputManualTare);
            });
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 20, 0);
            internal6.Source = "embedded://MT.Habasit/MT.Habasit.Images.Weight.al8";
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal6);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Width = 140;
            internal10.FontSize = ((System.Nullable<System.Int32>)24);
            internal10.Text = "Tara:";
            internal10.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Tare);
            internal10.AddTranslationAction(() => {
                internal10.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Tare);
            });
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10);
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal12 = new MT.Singularity.Presentation.Controls.TextBox();
            internal12.Width = 300;
            internal12.FontSize = ((System.Nullable<System.Int32>)28);
            internal12.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal12.Text = "1.240 g";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() => ViewModel.TareValue,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12);
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.Width = 60;
            internal14.FontSize = ((System.Nullable<System.Int32>)28);
            internal14.Text = "kg";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Text,() => ViewModel.Unit,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(internal14);
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal11, internal13);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8);
            internal7.Margin = new MT.Singularity.Presentation.Thickness(20, 60, 20, 0);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal7);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal16 = new MT.Singularity.Presentation.Controls.Button();
            internal16.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal16.Width = 180;
            internal16.Height = 80;
            internal16.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal16.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal18 = new MT.Singularity.Presentation.Controls.Image();
            internal18.Source = "embedded://MT.Habasit/MT.Habasit.Images.Cancel.al8";
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal19 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal19.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal19.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            internal19.AddTranslationAction(() => {
                internal19.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            });
            internal19.FontSize = ((System.Nullable<System.Int32>)20);
            internal17 = new MT.Singularity.Presentation.Controls.GroupPanel(internal18, internal19);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16.Content = internal17;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Command,() => ViewModel.GoCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal20 = new MT.Singularity.Presentation.Controls.Button();
            internal20.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal20.Width = 180;
            internal20.Height = 80;
            internal20.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal20.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal22 = new MT.Singularity.Presentation.Controls.Image();
            internal22.Source = "embedded://MT.Habasit/MT.Habasit.Images.ok.al8";
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal22.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal23 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal23.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Save);
            internal23.AddTranslationAction(() => {
                internal23.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Save);
            });
            internal23.FontSize = ((System.Nullable<System.Int32>)20);
            internal21 = new MT.Singularity.Presentation.Controls.GroupPanel(internal22, internal23);
            internal21.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal20.Content = internal21;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.Command,() => ViewModel.GoAccept,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.StackPanel(internal16, internal20);
            internal15.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(0, 60, 0, 20);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal15);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
