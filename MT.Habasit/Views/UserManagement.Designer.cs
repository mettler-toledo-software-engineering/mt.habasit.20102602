﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class UserManagement : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBox internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.StackPanel internal14;
            MT.Singularity.Presentation.Controls.TextBox internal15;
            MT.Singularity.Presentation.Controls.GroupPanel internal16;
            MT.Singularity.Presentation.Controls.StackPanel internal17;
            MT.Singularity.Presentation.Controls.StackPanel internal18;
            MT.Singularity.Presentation.Controls.TextBlock internal19;
            MT.Singularity.Presentation.Controls.StackPanel internal20;
            MT.Singularity.Presentation.Controls.TextBox internal21;
            MT.Singularity.Presentation.Controls.GroupPanel internal22;
            MT.Singularity.Presentation.Controls.StackPanel internal23;
            MT.Singularity.Presentation.Controls.StackPanel internal24;
            MT.Singularity.Presentation.Controls.TextBlock internal25;
            MT.Singularity.Presentation.Controls.StackPanel internal26;
            MT.Singularity.Presentation.Controls.TextBox internal27;
            MT.Singularity.Presentation.Controls.StackPanel internal28;
            MT.Singularity.Presentation.Controls.Button internal29;
            MT.Singularity.Presentation.Controls.GroupPanel internal30;
            MT.Singularity.Presentation.Controls.Image internal31;
            MT.Singularity.Presentation.Controls.TextBlock internal32;
            MT.Singularity.Presentation.Controls.Button internal33;
            MT.Singularity.Presentation.Controls.GroupPanel internal34;
            MT.Singularity.Presentation.Controls.Image internal35;
            MT.Singularity.Presentation.Controls.TextBlock internal36;
            MT.Singularity.Presentation.Controls.Button internal37;
            MT.Singularity.Presentation.Controls.GroupPanel internal38;
            MT.Singularity.Presentation.Controls.Image internal39;
            MT.Singularity.Presentation.Controls.TextBlock internal40;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 0);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.FontSize = ((System.Nullable<System.Int32>)30);
            internal3.Text = "Neuen Benutzer hinzufügen";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() => ViewModel.Title,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Width = 140;
            internal7.FontSize = ((System.Nullable<System.Int32>)24);
            internal7.Text = "Vorname:";
            internal7.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Firstname);
            internal7.AddTranslationAction(() => {
                internal7.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Firstname);
            });
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9 = new MT.Singularity.Presentation.Controls.TextBox();
            internal9.Width = 300;
            internal9.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => ViewModel.Firstname,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9);
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal8);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5);
            internal4.Margin = new MT.Singularity.Presentation.Thickness(20, 60, 20, 0);
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.Width = 140;
            internal13.FontSize = ((System.Nullable<System.Int32>)24);
            internal13.Text = "Nachname:";
            internal13.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Lastname);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Lastname);
            });
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13);
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal15 = new MT.Singularity.Presentation.Controls.TextBox();
            internal15.Width = 300;
            internal15.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Text,() => ViewModel.Lastname,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal14 = new MT.Singularity.Presentation.Controls.StackPanel(internal15);
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal14);
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11);
            internal10.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 20, 0);
            internal19 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal19.Width = 140;
            internal19.FontSize = ((System.Nullable<System.Int32>)24);
            internal19.Text = "Legic-Key:";
            internal19.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LegicKey);
            internal19.AddTranslationAction(() => {
                internal19.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LegicKey);
            });
            internal18 = new MT.Singularity.Presentation.Controls.StackPanel(internal19);
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal21 = new MT.Singularity.Presentation.Controls.TextBox();
            internal21.Width = 300;
            internal21.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal21.Text,() => ViewModel.Key,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal20 = new MT.Singularity.Presentation.Controls.StackPanel(internal21);
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal20.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal17 = new MT.Singularity.Presentation.Controls.StackPanel(internal18, internal20);
            internal17.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal16 = new MT.Singularity.Presentation.Controls.GroupPanel(internal17);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 20, 0);
            internal25 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal25.Width = 140;
            internal25.FontSize = ((System.Nullable<System.Int32>)24);
            internal25.Text = "UID:";
            internal24 = new MT.Singularity.Presentation.Controls.StackPanel(internal25);
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal27 = new MT.Singularity.Presentation.Controls.TextBox();
            internal27.Width = 300;
            internal27.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal27.Text,() => ViewModel.UID,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal26 = new MT.Singularity.Presentation.Controls.StackPanel(internal27);
            internal26.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal26.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal23 = new MT.Singularity.Presentation.Controls.StackPanel(internal24, internal26);
            internal23.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal22 = new MT.Singularity.Presentation.Controls.GroupPanel(internal23);
            internal22.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 20, 0);
            internal29 = new MT.Singularity.Presentation.Controls.Button();
            internal29.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal29.Width = 180;
            internal29.Height = 80;
            internal29.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal29.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal31 = new MT.Singularity.Presentation.Controls.Image();
            internal31.Source = "embedded://MT.Habasit/MT.Habasit.Images.Cancel.al8";
            internal31.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal32 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal32.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal32.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal32.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            internal32.AddTranslationAction(() => {
                internal32.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            });
            internal32.FontSize = ((System.Nullable<System.Int32>)20);
            internal30 = new MT.Singularity.Presentation.Controls.GroupPanel(internal31, internal32);
            internal30.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal29.Content = internal30;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal29.Command,() => ViewModel.GoCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal33 = new MT.Singularity.Presentation.Controls.Button();
            internal33.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal33.Width = 180;
            internal33.Height = 80;
            internal33.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal33.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal35 = new MT.Singularity.Presentation.Controls.Image();
            internal35.Source = "embedded://MT.Habasit/MT.Habasit.Images.ok.al8";
            internal35.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal35.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal36 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal36.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal36.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal36.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Add);
            internal36.AddTranslationAction(() => {
                internal36.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Add);
            });
            internal36.FontSize = ((System.Nullable<System.Int32>)20);
            internal34 = new MT.Singularity.Presentation.Controls.GroupPanel(internal35, internal36);
            internal34.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal33.Content = internal34;
            internal33.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal33.Visibility,() => ViewModel.AddVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal33.Command,() => ViewModel.GoAdd,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal37 = new MT.Singularity.Presentation.Controls.Button();
            internal37.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal37.Width = 180;
            internal37.Height = 80;
            internal37.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal37.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal39 = new MT.Singularity.Presentation.Controls.Image();
            internal39.Source = "embedded://MT.Habasit/MT.Habasit.Images.ok.al8";
            internal39.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal39.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal40 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal40.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal40.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal40.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Save);
            internal40.AddTranslationAction(() => {
                internal40.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Save);
            });
            internal40.FontSize = ((System.Nullable<System.Int32>)20);
            internal38 = new MT.Singularity.Presentation.Controls.GroupPanel(internal39, internal40);
            internal38.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal37.Content = internal38;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal37.Visibility,() => ViewModel.SaveVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal37.Command,() => ViewModel.GoSave,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal28 = new MT.Singularity.Presentation.Controls.StackPanel(internal29, internal33, internal37);
            internal28.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal28.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal28.Margin = new MT.Singularity.Presentation.Thickness(0, 60, 0, 20);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal10, internal16, internal22, internal28);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[10];
    }
}
