﻿using MT.Habasit.ViewModels;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for StartPage
    /// </summary>
    public partial class StartPage
    {
        private readonly StartPageViewModel _viewModel;

        public StartPage(AnimatedNavigationFrame homeNavigationFrame, ISecurityService securityService)
        {
            _viewModel = new StartPageViewModel(this, homeNavigationFrame, securityService);
            InitializeComponents();
        }

        #region Overrides

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            _viewModel.LoadDatabaseEntries();
            _viewModel.GoClearTare.Execute(null);
            _viewModel.RegisterLegicCardReaderEvent();  
            base.OnNavigationReturning(previousPage);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            return base.OnNavigatingBack(nextPage);
        }

        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            _viewModel.UnregisterLegicCardReaderEvent();
            return base.OnNavigatingAway(nextPage);
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #endregion
    }
}