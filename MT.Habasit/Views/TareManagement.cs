﻿using MT.Singularity.Presentation.Controls;
using MT.Habasit.Models;
using MT.Habasit.ViewModels;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for TareManagement
    /// </summary>
    public partial class TareManagement
    {
        private TareManagementViewModel _viewModel;
        
        public TareManagement(AnimatedContentControl homeNavigationFrame, TareModel tare, bool isNew)
        {
            _viewModel = new TareManagementViewModel(this, homeNavigationFrame, tare, isNew);
            InitializeComponents();
        }
    }
}
