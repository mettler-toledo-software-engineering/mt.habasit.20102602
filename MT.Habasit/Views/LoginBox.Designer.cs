﻿using MT.Singularity.Presentation.Controls;
namespace MT.Habasit.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class LoginBox : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.TextBox internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.GroupPanel internal15;
            MT.Singularity.Presentation.Controls.Image internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            MT.Singularity.Presentation.Controls.Button internal18;
            MT.Singularity.Presentation.Controls.GroupPanel internal19;
            MT.Singularity.Presentation.Controls.Image internal20;
            MT.Singularity.Presentation.Controls.TextBlock internal21;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 0);
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5.FontSize = ((System.Nullable<System.Int32>)30);
            internal5.Text = "Anmelden";
            internal5.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Login);
            internal5.AddTranslationAction(() => {
                internal5.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Login);
            });
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 20, 0);
            internal6.Source = "embedded://MT.Habasit/MT.Habasit.Images.LoginKey.al8";
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal6);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Width = 200;
            internal10.FontSize = ((System.Nullable<System.Int32>)24);
            internal10.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LegicKey);
            internal10.AddTranslationAction(() => {
                internal10.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.LegicKey);
            });
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10);
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal12 = new MT.Singularity.Presentation.Controls.TextBox();
            internal12.Width = 300;
            internal12.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() => ViewModel.Key,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12);
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal11);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8);
            internal7.Margin = new MT.Singularity.Presentation.Thickness(20, 60, 20, 0);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal7);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal14.Width = 180;
            internal14.Height = 80;
            internal14.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal14.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal16 = new MT.Singularity.Presentation.Controls.Image();
            internal16.Source = "embedded://MT.Habasit/MT.Habasit.Images.Cancel.al8";
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            internal17.AddTranslationAction(() => {
                internal17.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Cancel);
            });
            internal17.FontSize = ((System.Nullable<System.Int32>)20);
            internal15 = new MT.Singularity.Presentation.Controls.GroupPanel(internal16, internal17);
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14.Content = internal15;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() => ViewModel.GoCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal18 = new MT.Singularity.Presentation.Controls.Button();
            internal18.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal18.Width = 180;
            internal18.Height = 80;
            internal18.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal18.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal20 = new MT.Singularity.Presentation.Controls.Image();
            internal20.Source = "embedded://MT.Habasit/MT.Habasit.Images.ok.al8";
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal21 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Login);
            internal21.AddTranslationAction(() => {
                internal21.Text = MT.Habasit.Localization.Get(MT.Habasit.Localization.Key.Login);
            });
            internal21.FontSize = ((System.Nullable<System.Int32>)20);
            internal19 = new MT.Singularity.Presentation.Controls.GroupPanel(internal20, internal21);
            internal19.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal18.Content = internal19;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Command,() => ViewModel.GoLogin,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(internal14, internal18);
            internal13.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(0, 60, 0, 20);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal13);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
