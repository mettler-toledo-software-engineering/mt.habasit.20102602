﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using Marenco.ModelsAndController;
using MT.Balancer.Controls;
using MT.Balancer.Controls.TrimWeightControl;

namespace MT.Balancer.Pages
{
    /// <summary>
    /// Interaction logic for FillInPage
    /// </summary>
    public partial class FillInPage
    {
        private FillInPageViewModel viewModel;
        private AnimatedNavigationFrame _homeNavigationFrame;



        public FillInPage(AnimatedNavigationFrame _homeNavigationFrame)
        {
            string s = "";
            viewModel = new FillInPageViewModel(this, _homeNavigationFrame);
            InitializeComponents();
            try
            {
                s = Globals.Instance.GetStep(viewModel.ThisStep).Hint;
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Missing Info for Step " + viewModel.ThisStep.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            viewModel.HintText = s;
            viewModel.NewTrimWeightViewControl += viewModel_NewTrimWeightViewControl;

            this._homeNavigationFrame = _homeNavigationFrame;

            PanelResultSM.Add(viewModel.ResultControlSM);
            PanelResultCG.Add(viewModel.ResultControlCG);
            PanelResultWG.Add(viewModel.ResultControlWG);

        }

        void viewModel_NewTrimWeightViewControl(object sender, EventArgs e)
        {
            TrimWeightPanel.Clear();
            TrimWeightPanel.Add(viewModel.trimWeightViewControl);
        }


        /// <summary>
        /// TareKey all
        /// </summary>
        public ICommand Calculate
        {
            get { return new DelegateCommand(DoCalculate); }
        }

        private void DoCalculate()
        {
            viewModel.Calculate();
        }


        /// <summary>
        /// Information key
        /// </summary>
        public ICommand Information
        {
            get { return new DelegateCommand(DoInformation); }
        }

        private void DoInformation()
        {
            viewModel.ShowInfo();
        }


        /// <summary>
        /// Backkey
        /// </summary>
        public ICommand GoLeft
        {
            get { return new DelegateCommand(DoGoLeft); }
        }

        private void DoGoLeft()
        {
            viewModel.KeyLeft();
        }

        /// <summary>
        /// Continue
        /// </summary>
        public ICommand GoContinue
        {
            get { return new DelegateCommand(DoGoContinue); }
        }

        private void DoGoContinue()
        {
            viewModel.KeyContinue();
        }

        public void KeyContinue()
        {

            //HintPage hintPage = new HintPage(_homeNavigationFrame, 150, 159);
            //hintPage.viewModel.EndReached += viewModel_EndReached;
            //_homeNavigationFrame.NavigateTo(hintPage);
        }

        void viewModel_EndReached(object sender, EventArgs e)
        {
            _homeNavigationFrame.NavigateTo(new CalculationPage(_homeNavigationFrame));
        }

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            HomeScreenViewModel.Instance.Step = viewModel.ThisStep.ToString();

            viewModel.InitializeScales();
            ScalePanel1.Add(viewModel.MiniScalePanels[0]);
            ScalePanel2.Add(viewModel.MiniScalePanels[1]);
            ScalePanel3.Add(viewModel.MiniScalePanels[2]);
            viewModel.SetupTrimViewControl();
            base.OnFirstNavigation();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            return base.OnNavigatingBack(nextPage);
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreenViewModel.Instance.Step = viewModel.ThisStep.ToString();
            viewModel.enableContinue = false;
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnNavigationReturning(previousPage);
        }
    }
}
