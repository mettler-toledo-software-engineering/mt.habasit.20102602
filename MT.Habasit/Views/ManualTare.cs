﻿using MT.Habasit.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.Habasit.Views
{
    /// <summary>
    /// Interaction logic for ManualTare
    /// </summary>
    public partial class ManualTare
    {
        public ManualTareViewModel ViewModel;

        public ManualTare(AnimatedNavigationFrame homeNavigationFrame)
        {
            ViewModel = new ManualTareViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
