﻿using System;
using MT.Habasit.Infrastructure;

namespace MT.Habasit
{
    /// <summary>
    /// Main Program
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        [MTAThread]
        private static int Main(params string[] args)
        {
            var result = new Bootstrapper().Run(args);
            return result ? 0 : 1;
        }
    }
}
