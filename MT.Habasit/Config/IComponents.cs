﻿using MT.Singularity.Platform.Configuration;

namespace MT.Habasit.Config
{
    internal interface IComponents : IConfigurable<Configuration>
    {
    }
}