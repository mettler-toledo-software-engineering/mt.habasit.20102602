﻿using log4net;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using System;
using System.Threading.Tasks;

namespace MT.Habasit.Config
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class SetupNode : CustomerGroupSetupMenuItem
    {
        private IPlatformEngine _platformEngine;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(SetupNode);

        public SetupNode(SetupMenuContext context, IPlatformEngine platformEngine) : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.MainSetupNode))
        {
            _platformEngine = platformEngine;
        }

        private static readonly string SourceClass = nameof(SetupNode);

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override async Task ShowChildrenAsync()
        {
            try
            {
                var customerComponent = _context.CompositionContainer.Resolve<IComponents>();
                Configuration customerConfiguration = await customerComponent.GetConfigurationToChangeAsync();
                //  hier werden die 1-n subnodes für einen main setup node erstellt.

                var settingssubnode = new SettingsSubNode(_context, customerComponent, customerConfiguration, _platformEngine);
                Children = Indexable.ImmutableValues<SetupMenuItem>(settingssubnode);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"setup node crashed {ex.Message}", _sourceClass);
            }
        }
    }
}
