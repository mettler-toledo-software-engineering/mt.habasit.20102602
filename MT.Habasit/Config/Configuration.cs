﻿using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;
using System.ComponentModel;

namespace MT.Habasit.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {
        public Configuration()
        {
        }
        
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
           get { return (string)GetLocal(); }
           set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("Cino FuzzyScan")]
        public virtual string BarcodeScannerPort
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }
    }
}