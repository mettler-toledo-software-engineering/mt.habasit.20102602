﻿using log4net;
using MT.Habasit.Logic;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MT.Habasit.Models;

namespace MT.Habasit.Config
{
    class SettingsSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;
        private readonly IComponents _customerComponent;
        private readonly IPlatformEngine _platformEngine;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(SettingsSubNode);

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public SettingsSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration, IPlatformEngine platformEngine)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Setup), configuration, customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
            _platformEngine = platformEngine;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                _configuration.UsbUpdateResult = "";
                // Titles for the my Setup parameters
                var scannerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BarcodeScannerPort);
                var scannerPortTarget = new TextSetupMenuItem(_context, scannerPortTitle, _configuration, "BarcodeScannerPort");

                var usbUpdateResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.UpdateStatus);
                var usbUpdateResultTarget = new TextBlockSetupMenuItem(_context, usbUpdateResultTitle, _configuration, "UsbUpdateResult");

                //buttons können nicht in gruppen gelegt werden sondern müssen direkt an das child
                var btnExport = new ImageButtonSetupMenuItem(_context, "embedded://MT.Habasit/MT.Habasit.Images.Export.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Export, 20, DoExport);
                var btnSystemUpdate = new ImageButtonSetupMenuItem(_context, "embedded://MT.Habasit/MT.Habasit.Images.FlashDisk.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, DoSystemUpdate);
                var btnReboot = new ImageButtonSetupMenuItem(_context, "embedded://MT.Habasit/MT.Habasit.Images.restart.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Reboot, 20, DoReboot);

                var rangeGroup = new GroupedSetupMenuItems(_context, usbUpdateResultTarget, scannerPortTarget);
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup, btnExport,btnSystemUpdate, btnReboot);

                //Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, button, rangeGroup3);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"Unexpected Exception: {ex.Message}", _sourceClass);
            }

            return TaskEx.CompletedTask;
        }

        private void DoSystemUpdate()
        {
            bool success;
            success = SystemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.UsbUpdateResult = "Update copied.";
            }
            else
            {
                _configuration.UsbUpdateResult = "Update failed!";
            }
        }

        private void DoExport()
        {
            List<TransactionModel> transactionList = ProcessFlow.Current.TransactionList;
            Extensions.WriteTransactionsToFileSystemAsCsv(transactionList);
            Extensions.WriteTransactionsToFileSystemAsXlsx(transactionList);
        }

        private void DoReboot()
        {
            _log.InfoEx($"Reboot device", _sourceClass);
            _platformEngine.RebootAsync();
            _log.InfoEx($"Reboot device done", _sourceClass);
        }
    }
}
